package com.clockme.interfaces;
import org.json.JSONObject;

public interface IWebservice {
    public void onComplete(JSONObject WSResponse, String WSMessage, int WSCode);
    public void onError(String error);
    public void onAccessDenied(String WSMessage);
}
