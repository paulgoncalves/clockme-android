package com.clockme.interfaces;

import android.location.Location;

public interface IGetLocation {
	void onLocationListener(Location location);
}
