package com.clockme.interfaces;

public interface IChangePassword {
	
	public void onComplete(boolean isSuccessful, String message);

	public void onError(String error);
	
	public void onChangePassword(String currentPassword, String newPassword);
}
