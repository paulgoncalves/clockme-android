package com.clockme.interfaces;

public interface IRefresh {
	public void onRefresh();
}