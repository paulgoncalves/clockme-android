package com.clockme.interfaces;

public interface IUploadImage {
	public void onComplete(int WSResponse);
	public void onError(String error);
	
}
