package com.clockme.interfaces;

import org.json.JSONArray;

public interface IGetAddress {
	public void onComplete(JSONArray WSResponse);
	public void onError(String error);
}
