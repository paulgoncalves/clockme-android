package com.clockme.interfaces;

import android.graphics.Bitmap;

/**
 * Created by tannguyen on 5/7/15.
 */
public interface IImageLoader {

    void onSuccess(Bitmap _bitmap);
    void onFailed(String error);

}
