package com.clockme;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.clockme.activities.LoginActivity;
import com.clockme.models.CKCompanyModel;
import com.clockme.models.CKPlaceModel;
import com.clockme.models.CKPersonModel;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class SBApplication extends Application {

	public static Context appContext;
	public static LruCache<String, Bitmap> imageCache;
	private static Activity mainActivity = null;
    private static Activity loginActivity = null;

	private static CKPersonModel mCKPersonModel;
	private static CKCompanyModel mCKCompanyModel;
	private static CKPlaceModel mCKPlaceModel;


	@Override
	public void onCreate() {
		super.onCreate();

		// todo:
		// turn crash analytics back on when
		// going live
		//Fabric.with(this, new Crashlytics());

		// turn crash analytics on only when not in debug mode
		if(!BuildConfig.DEBUG) {
			Fabric.with(this, new Crashlytics());
		}

        setMainActivity(null);
        setLoginActivity(null);
		appContext = SBApplication.this.getApplicationContext();
		imageCache = new LruCache<>(20 * 1024 * 1024);

		mCKPersonModel = new CKPersonModel();
	}

	public static void gotoLoginPage() {
		Intent i = new Intent(appContext, LoginActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		appContext.startActivity(i);
	}
	
	public static void setMainActivity(Activity activity) {
		mainActivity = activity;
	}

    public static Activity getMainActivity() {
        return mainActivity;
    }

    public static void setLoginActivity(Activity activity) {
        loginActivity = activity;
    }

    public static Activity getLoginActivity() {
        return loginActivity;
    }

	public static void destroy() {
		if (mainActivity != null) {
			mainActivity.finish();
		}
	}

	// User model
	public static void setUserModel(CKPersonModel model) {
		mCKPersonModel = model;
	}
	public static CKPersonModel getUserModel() {
		return mCKPersonModel;
	}

	//Entity Model
	public static void setEntityModel(CKCompanyModel model) { mCKCompanyModel = model; }
	public static CKCompanyModel getEntityModel() {
		return mCKCompanyModel;
	}

	//Site model
	public static void setSiteModel(CKPlaceModel model) {
		mCKPlaceModel = model;
	}
	public static CKPlaceModel getSiteModel() {
		return mCKPlaceModel;
	}
}
