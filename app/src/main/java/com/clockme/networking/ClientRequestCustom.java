package com.clockme.networking;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.ResponseHandlerInterface;

import cz.msebera.android.httpclient.HttpEntity;

/**
 * Created by tannguyen on 7/15/15.
 */
public class ClientRequestCustom extends AsyncHttpClient {

    @Override
    public RequestHandle post(Context context, String url, HttpEntity entity, String contentType, ResponseHandlerInterface responseHandler) {
        return super.post(context, url, entity, contentType, responseHandler);
    }
}
