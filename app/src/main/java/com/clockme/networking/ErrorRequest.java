package com.clockme.networking;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.clockme.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class ErrorRequest {

	public interface ExceptionType {
		String UNKNOWN_HOST_EXCEPTION = "UnknownHostException";
		String TIMEOUT_EXCEPTION = "TimeoutException";
		String SOCKET_TIMEOUT_EXCEPTION = "SocketTimeoutException";
		String MALFORMED_URL_EXCEPTION = "MalformedURLException";
	}

	public static String[] getErrorMessage(Context context, byte[] response,
			Throwable ex) {
		String errorTitle = null;
		String errorMsg = null;
		String errorTemp = null;
		errorTitle = context.getResources().getString(R.string.warning);
		if (response != null) {
			try {
				errorTemp = new String(response, "UTF-8");
				JSONObject jsonObj = new JSONObject(errorTemp);
				if (jsonObj.has("error")) {
					errorMsg = jsonObj.optString("error");
				} else if (jsonObj.has("message")) {
					errorMsg = jsonObj.optString("message");
				} else {
					errorMsg = errorTemp;
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			errorTemp = ex.getMessage();
			if (TextUtils.isEmpty(errorTemp)) {
				errorMsg = context.getResources().getString(
						R.string.msg_timeout);
			} else {
				Log.e("ERROR","=> "+errorTemp);
				if (errorTemp.contains(ExceptionType.UNKNOWN_HOST_EXCEPTION)) {
					errorMsg = context.getResources().getString(
							R.string.msg_lost_connection);
				} else if (errorTemp.contains(ExceptionType.TIMEOUT_EXCEPTION)) {
					errorMsg = context.getResources().getString(
							R.string.msg_timeout);
				} else if (errorTemp
						.contains(ExceptionType.SOCKET_TIMEOUT_EXCEPTION)) {
					errorMsg = context.getResources().getString(
							R.string.msg_timeout);
				} else if (errorTemp
						.contains(ExceptionType.MALFORMED_URL_EXCEPTION)) {
					errorMsg = context.getResources().getString(
							R.string.msg_lost_connection);
				} else {
					errorMsg = context.getResources().getString(
							R.string.msg_lost_connection);
				}
			}
		}
		return new String[] { errorTitle, errorMsg };
	}

}
