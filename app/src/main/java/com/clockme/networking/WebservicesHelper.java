package com.clockme.networking;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.interfaces.IGetAddress;
import com.clockme.interfaces.ILoading;
import com.clockme.interfaces.IUploadImage;
import com.clockme.interfaces.IWebservice;
import com.clockme.utilities.Constants;
import com.clockme.utilities.Utilities;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class WebservicesHelper{

    private static final String TAG = WebservicesHelper.class.getSimpleName();

    public static ClientRequestCustom clientRequest;

    final static String CONTENT_TYPE = "application/x-www-form-urlencoded";
    final static String CONTENT_TYPE_JSON = "application/json";

    public final static String BASE_URL = "http://10.0.2.2:80/Api/v1_android";
    //public final static String BASE_URL = "http://10.0.2.2:80/api.android/v1_android";
    // todo: remember to change to correct url
    //public final static String BASE_URL = "http://api.clockme.com/v1_android";

    //////////////////////////////////////
    //////////      ROTE   ///////////////
    //////////////////////////////////////

    ////////// LOGIN ///////////////
    public final static String ROUTE_LOGIN = BASE_URL + "/login";
    public final static String ROUTE_LOGIN_FORGOT_PASSWORD = BASE_URL + "/forgotPassword";
    public final static String ROUTE_LOGIN_REGISTRATION = BASE_URL + "/registerUser";

    ////////// USER ///////////////
    public final static String ROUTE_USER_UPDATE = BASE_URL + "/updateUser";
    public final static String ROUTE_USER_UPDATE_PASSWORD = BASE_URL + "/updateUserPassword";
    public final static String ROUTE_USER_UPDATE_DEVICE = BASE_URL + "/updateDevice";

    ////////// ENTITY ///////////////
    public final static String ROUTE_ENTITY_ALL_ENTITIES = BASE_URL + "/entity";
    public final static String ROUTE_ENTITY_BY_ID = BASE_URL + "/companyId";
    public final static String ROUTE_ENTITY_USER_ENTITIES = BASE_URL + "/entityUser";
    public final static String ROUTE_ENTITY_JOIN = BASE_URL + "/joinEntity";
    public final static String ROUTE_ENTITY_DELETE = BASE_URL + "/deleteUserEntity";

    ////////// SITE ///////////////
    public final static String ROUTE_SITE_BY_ENTITY = BASE_URL + "/entitySite";
    public final static String ROUTE_SITE_JOIN = BASE_URL + "/joinSite";
    public final static String ROUTE_SITE_CREATE = BASE_URL + "/createSite";
    public final static String ROUTE_SITE_UPDATE = BASE_URL + "/updateSite";
    public final static String ROUTE_SITE_DELETE = BASE_URL + "/deleteSite";
    public final static String ROUTE_SITE_LOG = BASE_URL + "/siteLog";
    public final static String ROUTE_SITE_PROJECT = BASE_URL + "/siteProject";
    public final static String ROUTE_SITE_EMERGENCY = BASE_URL + "/emergency";
    public final static String ROUTE_SITE_NOTE = BASE_URL + "/checkinNote";
    public final static String ROUTE_SITE_QUESTION = BASE_URL + "/question";
    public final static String ROUTE_SITE_ANSWER = BASE_URL + "/answer";

    ////////// ACTION ///////////////
    public final static String ROUTE_ACTION_CHECKIN = BASE_URL + "/checkin";
    public final static String ROUTE_ACTION_REPORTIN = BASE_URL + "/reportin";
    public final static String ROUTE_ACTION_CHECKOUT = BASE_URL + "/checkout";
    public final static String ROUTE_ACTION_PULSE = BASE_URL + "/pulse";
    public final static String ROUTE_ACTION_USERSAFE = BASE_URL + "/userSafe";

    ////////// MY TEAM ///////////////
    public final static String ROUTE_MY_TEAM = BASE_URL + "/myTeam";
    public final static String ROUTE_MY_TEAM_LEADERS = BASE_URL + "/myTeamLeader";
    public final static String ROUTE_MY_TEAM_TEAM_LEADERS_BY_ENTITY = BASE_URL + "/teamLeader";
    public final static String ROUTE_MY_TEAM_ADD_TEAM_LEADER = BASE_URL + "/addTeamLeader";
    public final static String ROUTE_MY_TEAM_REMOVE_TEAM_LEADER = BASE_URL + "/removeTeamLeader";

    ////////// USER ///////////////
    public final static String ROUTE_USER_LOG = BASE_URL + "/userLog";
    public final static String ROUTE_USER_LOG_BY_ENTITY = BASE_URL + "/userLog";
    public final static String ROUTE_USER_ENTITY = BASE_URL + "/userEntity";
    public final static String ROUTE_USER_STATUS = BASE_URL + "/userStatus";

    ////////// CONNECTION ///////////////
    public final static String ROUTE_CONNECTION_USER = BASE_URL + "/userConnection";
    public final static String ROUTE_CONNECTION_ADD = BASE_URL + "/addConnection";
    public final static String ROUTE_CONNECTION_REMOVE = BASE_URL + "/removeConnection";
    public final static String ROUTE_CONNECTION_ACCEPT = BASE_URL + "/acceptConnection";
    public final static String ROUTE_CONNECTION_REJECT = BASE_URL + "/rejectConnection";
    public final static String ROUTE_CONNECTION_USER_SINGLE = BASE_URL + "/userSingleConnection";

    ////////// NOTIFICATION ///////////////
    public final static String ROUTE_NOTIFICATION_USER = BASE_URL + "/notification";
    public final static String ROUTE_NOTIFICATION_CLEAR_ALL = BASE_URL + "/clearAllNotification";
    public final static String ROUTE_NOTIFICATION_CLEAR = BASE_URL + "/clearNotification";

    ////////// HAZARD ///////////////
    public final static String ROUTE_HAZARD = BASE_URL + "/hazard";
    public final static String ROUTE_HAZARD_CLOSE = BASE_URL + "/hazardClose";
    public final static String ROUTE_HAZARD_HAZARD_SHARE = BASE_URL + "/hazardShare";
    public final static String ROUTE_HAZARD_CREATE = BASE_URL + "/hazardCreate";
    public final static String ROUTE_HAZARD_UPDATE = BASE_URL + "/hazardUpdate";
    public final static String ROUTE_HAZARD_ACTION_CREATE = BASE_URL + "/hazardActionCreate";
    public final static String ROUTE_HAZARD_ACTION_CLOSE = BASE_URL + "/hazardActionClose";

    //////////// LEAVE ////////////
    public final static String ROUTE_LEAVE = BASE_URL + "/leave";
    public final static String ROUTE_LEAVE_TYPE = BASE_URL + "/leaveType";
    public final static String ROUTE_LEAVE_SAVE = BASE_URL + "/leaveSave";
    public final static String ROUTE_LEAVE_UPDATE= BASE_URL + "/leaveUpdate";
    public final static String ROUTE_LEAVE_DELETE = BASE_URL + "/leaveDelete";


    ////////// IMAGE ///////////////
    public final static String ROUTE_IMAGE_UPLOAD = BASE_URL + "/imgUpload";
    public final static String ROUTE_FOLDER_USER = "userImage";
    public final static String ROUTE_FOLDER_HAZARD = "hazard";
    public final static String ROUTE_FOLDER_HAZARD_ACTION = "hazardAction";

    // todo: remove after testing - use code above
//    public final static String ROUTE_FOLDER_HAZARD = "hazard_test";
//    public final static String ROUTE_FOLDER_HAZARD_ACTION = "hazardAction_test";

    //////////////////////////////////////
    ////////// PARAMETERS ///////////////
    //////////////////////////////////////

    /////////// LOGIN ///////////
    public final static String PARAMS_LOGIN = "email=%s&password=%s";
    public final static String PARAMS_LOGIN_FORGOT_PASSWORD = "email=%s";

    /////////// REGISTRATION ///////////
    public final static String PARAMS_REGISTRATION = "companyName=%s&lastName=%s&email=%s&password=%s&mobile=%s";
    public final static String PARAMS_REGISTRATION_UPDATE_DEVICE = "personId=%s&token=%s&deviceId=%s&deviceType=Android&dateModified=%s&timezone=%s";

    /////////// USER ///////////
    public final static String PARAMS_USER_UPDATE_USER = "personId=%s&token=%s&companyName=%s&lastName=%s&email=%s&mobile=%s&emergencyNumber=%s";
    public final static String PARAMS_USER_UPDATE_USER_PASSWORD = "personId=%s&token=%s&currentPassword=%s&password=%s";
    public final static String PARAMS_USER_STATUS = "personId=%s&token=%s";
    public final static String PARAMS_USER_LOG = "personId=%s&token=%s";
    public final static String PARAMS_USER_LOG_BY_ENTITY = "personId=%s&token=%s&companyId=%s&userLog=%s";
    //final static String USER_STATUS_PARAM = "personId=%s&token=%s&hazardId=%s&companyId=%s&time=%s&timezone=%s";

    /////////// MY TEAM ///////////
    public final static String PARAMS_MY_TEAM = "personId=%s&token=%s&companyId=%s&accessLevel=%s";
    public final static String PARAMS_MY_TEAM_LEADERS = "personId=%s&token=%s";
    public final static String PARAMS_MY_TEAM_TEAM_LEADERS_BY_ENTITY = "personId=%s&token=%s&companyId=%s";
    public final static String PARAMS_MY_TEAM_ADD_TEAM_LEADER = "personId=%s&token=%s&companyId=%s&leaderId=%s";
    public final static String PARAMS_MY_TEAM_REMOVE_TEAM_LEADER = "personId=%s&token=%s&companyId=%s&leaderId=%s";

    /////////// ENTITY ///////////
    public final static String PARAMS_ENTITY_ALL = "personId=%s&token=%s";
    public final static String PARAMS_ENTITY_USER_ENTITIES = "personId=%s&token=%s";
    public final static String PARAMS_ENTITY_JOIN_ENTITY = "personId=%s&token=%s&companyId=%s&groupId=%s&ts=%s";
    public final static String PARAMS_ENTITY_DELETE_ENTITY = "personId=%s&token=%s&companyId=%s&ts=%s";
    public final static String PARAMS_ENTITY_BY_ID = "personId=%s&token=%s&companyId=%s";

    /////////// SITE ///////////
    public final static String PARAM_SITE_BY_ENTITY = "personId=%s&token=%s&companyId=%s&time=%s&lat=%s&lng=%s";
    public final static String PARAMS_SITE_CREATE = "personId=%s&token=%s&companyId=%s&companyName=%s&lat=%s&lng=%s&radius=%s&stNumber=%s&stName=%s&suburb=%s&state=%s&safeZone=%s&blackspot=%s&postcode=%s&country=%s&projectId=%s";
    public final static String PARAMS_SITE_UPDATE = "personId=%s&token=%s&placeId=%s&companyName=%s&projectId=%s&lat=%s&lng=%s&radius=%s&stNumber=%s&stName=%s&suburb=%s&state=%s&safeZone=%s&blackspot=%s";
    public final static String PARAMS_SITE_DELETE = "placeId=%s&personId=%s&token=%s";
    public final static String PARAMS_SITE_LOG = "personId=%s&token=%s&placeId=%s";
    public final static String PARAMS_SITE_PROJECT = "personId=%S&token=%s";
    public final static String PARAMS_SITE_JOIN = "personId=%s&token=%s&companyId=%s&code=%s&time=%s";
    //public final static String PARAMS_SITE_PROJECT = "personId=%s&token=%s&companyId=%s&placeId=%s";
    public final static String PARAMS_SITE_EMERGENCY = "companyId=%s&placeId=%s&personId=%s&token=%s";
    public final static String PARAMS_SITE_QUESTION = "personId=%s&token=%s&placeId=%s&companyId=%s";
    public final static String PARAMS_SITE_ANSWER = "personId=%s&token=%s&placeId=%s&checkinId=%s&answers=%s";
    public final static String PARAMS_SITE_NOTE = "personId=%s&token=%s&placeId=%s";

    /////////// ACTION ///////////
    public final static String PARAMS_ACTION_CHECK_IN = "personId=%s&token=%s&checkinCategoryId=%s&placeId=%s&ts=%s&timezone=%s&lat=%s&lng=%s&userEntityId=%s&batteryLevel=%s&success=%s";
    public final static String PARAMS_ACTION_REPORTIN = "personId=%s&token=%s&placeId=%s&checkinId=%s&ts=%s&timezone=%s&lat=%s&lng=%s&userEntityId=%s&batteryLevel=%s";
    public final static String PARAMS_ACTION_CHECKOUT = "personId=%s&token=%s&placeId=%s&ts=%s&timezone=%s&lat=%s&lng=%s&userEntityId=%s&batteryLevel=%s&done=%s&checkinId=%s&onRadius=1";
    public final static String PARAMS_ACTION_PULSE = "personId=%s&token=%s&ts=%s&timezone=%s&lat=%s&lng=%s&checkinTypeId=%s&userEntityId=%s&batteryLevel=%s";
    public final static String PARAMS_ACTION_USER_SAFE = "personId=%s&token=%s&userActionId=%s&ts=%s&timezone=%s&lat=%s&lng=%sdescription=%s&action=%s";

    /////////// CONNECTION ///////////
    public final static String PARAMS_CONNECTION_USERS = "personId=%s&token=%s&companyId=%s&connectionFor=%s";
    public final static String PARAMS_CONNECTION_ACCEPT = "personId=%s&token=%s&reportToId=%s&timezone=%s";
    public final static String PARAMS_CONNECTION_ADD = "personId=%s&token=%s&companyId=%s&userConnection=%s&permission=%s&timezone=%s";
    public final static String PARAMS_CONNECTION_REMOVE = "personId=%s&token=%s&userConnection=%s&reportToId=%s";
    public final static String PARAMS_CONNECTION_REJECT = "personId=%s&token=%s&reportToId=%s&tz=%s";
    public final static String PARAMS_CONNECTION_USERS_SINGLE = "personId=%s&token=%s&companyId=%s&connectedTo=%s&accessLevel=%s";

    /////////// NOTIFICATION ///////////
    public final static String PARAMS_NOTIFICATION_USER = "personId=%s&token=%s&companyId=%s&notificationId=%s";
    public final static String PARAMS_NOTI_CLEAR_ALL = "personId=%s&token=%s";
    public final static String PARAMS_NOTI_CLEAR = "personId=%s&token=%s&notiAlertId=%s";

    /////////// HAZARD ///////////
    public final static String PARAMS_HAZARD_CREATE = "personId=%s&token=%s&selfManaged=%s&placeId=%s&companyId=%s&time=%s&timezone=%s&title=%s&description=%s&precaution=%s&preciseLocation=%s&risk=%s";
    public final static String PARAMS_HAZARD_GET_ALL_BY_SITE_ID = "personId=%s&token=%s&placeId=%s";
    public final static String PARAMS_HAZARD_UPDATE = "personId=%s&token=%s&hazardId=%s&title=%s&description=%s&precaution=%s&preciseLocation=%s&risk=%s";

    public final static String PARAMS_HAZARD_CLOSE = "personId=%s&token=%s&hazardId=%s&companyId=%s&time=%s&timezone=%s";
    public final static String PARAMS_HAZARD_ACTION_COMPLETE = "personId=%s&token=%s&hazardActionId=%s&companyId=%s&note=%s&time=%s&timezone=%s";

    /////////// LEAVE ///////////
    public final static String PARAMS_LEAVE = "personId=%s&token=%s";
    public final static String PARAMS_LEAVE_TYPE = "personId=%s&token=%s&companyId=%s";
    public final static String PARAMS_LEAVE_SAVE = "personId=%s&token=%s&companyId=%s&description=%s&leaveType=%s&timeFrom=%s&timeTo=%s&tz=%s";
    public final static String PARAMS_LEAVE_UPDATE = "personId=%s&token=%s&leaveId=%s&description=%s&leaveType=%s&timeFrom=%s&timeTo=%s";
    public final static String PARAMS_LEAVE_DELETE = "personId=%s&token=%s&leaveId=%s";

    /////////// OTHERS ///////////
    public final static String GET_ADDRESS = "http://maps.googleapis.com/maps/api/geocode/json?sensor=true&latlng=%s,%s";
    public final static String LOAD_USER_IMAGE_URL = "http://www.clockme.com/assets/user/%s/%s.jpg";
    public final static String LOAD_ENTITY_IMAGE_URL = "http://www.clockme.com/assets/entity/%s/%s.jpg";
    //public final static String LOAD_HAZARD_IMAGE_URL = "http://www.clockme" +
    //        ".com/assets/hazard/%s/%s.jpg";
    //public final static String LOAD_HAZARD_ACTION_IMAGE_URL = "http://www.clockme" +
    //        ".com/assets/hazardAction/%s/%s.jpg";

    public final static String LOAD_HAZARD_IMAGE_URL = "http://www.clockme" +
            ".com/assets/hazard_test/%s/%s.jpg";
    public final static String LOAD_HAZARD_ACTION_IMAGE_URL = "http://www.clockme" +
            ".com/assets/hazardAction_test/%s/%s.jpg";


    //////////////////////////////////////
    //////// WEBSERVICE CODE /////////////
    //////////////////////////////////////

    public final static int WSCODE_LOGIN_ACCESS_DENIED = 500;
    public final static int WSCODE_PDO_ERROR = 501;
    //LOGIN
    public final static int WSCODE_LOGIN_SUCCESS = 100;
    public final static int WSCODE_LOGIN_FAIL = 101;
    public final static int WSCODE_LOGIN_NOT_ACTIVE = 102;
    public final static int WSCODE_LOGIN_FORGOT_PASSWORD = 103;
    public final static int WSCODE_LOGIN_INVALID_EMAIL = 104;

    //USER
    public final static int WSCODE_REGISTER_SUCCESS = 110;
    public final static int WSCODE_REGISTER_FAIL = 111;
    public final static int WSCODE_REGISTER_EXISTENT = 112;
    public final static int WSCODE_USER_UPDATE_SUCCESS = 113;
    public final static int WSCODE_USER_UPDATE_FAIL = 114;
    public final static int WSCODE_USER_UPDATE_PASSWORD_SUCCESS = 115;
    public final static int WSCODE_USER_UPDATE_PASSWORD_FAIL = 114;
    public final static int WSCODE_USER_STATUS_SUCCESS = 114;
    public final static int WSCODE_USER_STATUS_FAIL = 115;
    public final static int WSCODE_USER_TOKEN_UPDATE_SUCCESS = 116;
    public final static int WSCODE_USER_TOKEN_UPDATE_FAIL = 117;
    public final static int WSCODE_USER_VERSION_UPDATE_SUCCESS = 118;
    public final static int WSCODE_USER_VERSION_UPDATE_FAIL= 119;

    //ENTITY
    public final static int WSCODE_ENTITY_FETCH_SUCCESS = 120;
    public final static int WSCODE_ENTITY_FETCH_FAIL = 121;
    public final static int WSCODE_ENTITY_USER_FETCH_SUCCESS = 122;
    public final static int WSCODE_ENTITY_USER_FETCH_FAIL = 123;
    public final static int WSCODE_ENTITY_JOIN_SUCCESS = 124;
    public final static int WSCODE_ENTITY_JOIN_FAIL = 125;
    public final static int WSCODE_ENTITY_JOIN_FAIL_UPGRADE = 126;
    public final static int WSCODE_ENTITY_DELETE_SUCCESS = 127;
    public final static int WSCODE_ENTITY_DELETE_FAIL = 128;

    //SITE
    public final static int WSCODE_SITE_FETCH_SUCCESS = 130;
    public final static int WSCODE_SITE_FETCH_FAIL = 131;
    public final static int WSCODE_SITE_CREATE_SUCCESS = 132;
    public final static int WSCODE_SITE_CREATE_FAIL = 133;
    public final static int WSCODE_SITE_CREATE_FRAIL_UPGRADE = 134;
    public final static int WSCODE_SITE_UPDATE_SUCCESS = 135;
    public final static int WSCODE_SITE_UPDATE_FAIL= 136;
    public final static int WSCODE_SITE_DELETE_SUCCESS = 137;
    public final static int WSCODE_SITE_DELETE_FAIL = 138;
    public final static int WSCODE_SITE_EMERGENCY_FETCH_SUCCESS = 139;
    public final static int WSCODE_SITE_EMERGENCY_FETCH_FAIL = 140;
    public final static int WSCODE_SITE_JOIN_SUCCESS = 141;
    public final static int WSCODE_SITE_JOIN_SAME_ENTITY = 142;
    public final static int WSCODE_SITE_JOIN_NO_MATCH = 143;
    public final static int WSCODE_SITE_JOIN_EXPIRED = 144;
    public final static int WSCODE_SITE_JOIN_FAIL = 145;
    public final static int WSCODE_SITE_PROJECT_STATUS_FETCH_SUCCESS = 146;
    public final static int WSCODE_SITE_PROJECT_STATUS_FETCH_FAIL = 146;
    public final static int WSCODE_SITE_PROJECT_FETCH_SUCCESS = 147;
    public final static int WSCODE_SITE_PROJECT_FETCH_FAIL = 148;
    public final static int WSCODE_SITE_LOG_FETCH_SUCCESS = 149;
    public final static int WSCODE_SITE_LOG_FETCH_NO_LOG = 150;
    public final static int WSCODE_SITE_LOG_FETCH_FAIL = 151;

    public final static int WSCODE_SITE_NOTE_SAVE_SUCCESS = 152;
    public final static int WSCODE_SITE_NOTE_SAVE_FAIL = 153;
    public final static int WSCODE_SITE_NOTE_SUCCESS = 154;
    public final static int WSCODE_SITE_NOTE_FAIL = 155;
    public final static int WSCODE_SITE_NOTE_NO_NOTE = 156;

    //ACTION PROCESS
    public final static int WSCODE_ACTION_CHECKIN_SUCCESS = 160;
    public final static int WSCODE_ACTION_CHECKIN_FAIL= 161;
    public final static int WSCODE_ACTION_REPORTIN_SUCCESS = 163;
    public final static int WSCODE_ACTION_REPORTIN_FAIL = 164;
    public final static int WSCODE_ACTION_CHECKOUT_SUCCESS = 165;
    public final static int WSCODE_ACTION_CHECKOUT_FAIL = 166;
    public final static int WSCODE_ACTION_PULSE_SUCCESS = 167;
    public final static int WSCODE_ACTION_PULSE_FAIL = 167;
    public final static int WSCODE_ACTION_USER_SAFE_SUCCESS = 168;
    public final static int WSCODE_ACTION_USER_SAFE_FAIL = 169;
    public final static int WSCODE_ACTION_USER_IS_SAFE = 170;

    //CONNECTIONS
    public final static int WSCODE_CONNECTION_USER_FETCH_SUCCESS = 180;
    public final static int WSCODE_CONNECTION_USER_FETCH_FAIL = 181;
    public final static int WSCODE_CONNECTION_USER_NO_EXIST = 182;
    public final static int WSCODE_CONNECTION_FETCH_SUCCESS = 183;
    public final static int WSCODE_CONNECTION_FETCH_FAIL = 184;
    public final static int WSCODE_CONNECTION_FETCH_NO_EXIST = 185;
    public final static int WSCODE_CONNECTION_ADD_SUCCESS = 186;
    public final static int WSCODE_CONNECTION_ADD_FAIL = 187;
    public final static int WSCODE_CONNECTION_REMOVE_SUCCESS = 188;
    public final static int WSCODE_CONNECTION_REMOVE_FAIL = 189;
    public final static int WSCODE_CONNECTION_ACCEPT_SUCCESS = 190;
    public final static int WSCODE_CONNECTION_ACCEPT_FAIL = 191;
    public final static int WSCODE_CONNECTION_REJECT_SUCCESS = 192;
    public final static int WSCODE_CONNECTION_REJECT_FAIL = 193;
    public final static int WSCODE_CONNECTION_PREVIOUS_ACCEPTED = 194;
    public final static int WSCODE_CONNECTION_NO_EXIST = 195;
    public final static int WSCODE_CONNECTION_USER_SINGLE_FETCH_SUCCESS = 196;

    //MY TEAM
    public final static int WSCODE_MYTEAM_SUCCESS = 200;
    public final static int WSCODE_MYTEAM_FAIL = 201;
    public final static int WSCODE_MYTEAM_NO_TEAM = 202;
    public final static int WSCODE_MYTEAM_TEAM_LEADER_FETCH_SUCCESS = 203;
    public final static int WSCODE_MYTEAM_NO_LEADER = 204;
    public final static int WSCODE_MYTEAM_LEADER_FETCH_SUCCESS = 205;
    public final static int WSCODE_MYTEAM_LEADER_NO_LEADER = 206;

    //GET USER LOG
    public final static int WSCODE_LOG_USER_SUCCESS = 210;
    public final static int WSCODE_LOG_USER_FAIL = 211;
    public final static int WSCODE_LOG_NO_LOG = 212;

    //QUESTIONS
    public final static int WSCODE_QUESTION_SUCCESS = 220;
    public final static int WSCODE_QUESTION_ANSWERED = 221;
    public final static int WSCODE_QUESTION_NO_QUESTION = 222;
    public final static int WSCODE_QUESTION_FAIL = 223;
    public final static int WSCODE_QUESTION_ANSWER_SUCCESS = 224;
    public final static int WSCODE_QUESTION_ANSWER_FAIL = 225;

    //NOTIFICATION
    public final static int WSCODE_NOTIFICATION_SUCCESS = 230;
    public final static int WSCODE_NOTIFICATION_FAIL = 231;
    public final static int WSCODE_NOTIFICATION_CLEAR_SUCCESS = 232;
    public final static int WSCODE_NOTIFICATION_CLEAR_FAIL = 233;
    public final static int WSCODE_NOTIFICATION_CLEAR_ALL_SUCCESS = 234;
    public final static int WSCODE_NOTIFICATION_CLEAR_ALL_FAIL = 235;

    //MAN DOWN
    public final static int WSCODE_MANDOWN_SUCCESS = 240;
    public final static int WSCODE_MANDOWN_FAIL = 241;
    public final static int WSCODE_MANDOWN_DEACTIVATE_SUCCESS = 242;
    public final static int WSCODE_MANDOWN_DEACTIVATE_FAIL = 243;
    public final static int WSCODE_MANDOWN_DEACTIVATE_WRONG_PASSWORD = 244;

    //UPLOAD
    public final static int WSCODE_IMAGE_SUCCESS = 250;
    public final static int WSCODE_IMAGE_FAIL = 251;
    public final static int WSCODE_IMAGE_INVALID = 252;

    //LOG MESSAGES
    public final static int WSCODE_LOG_SUCCESS = 260;
    public final static int WSCODE_LOG_FAIL = 261;
    public final static int WSCODE_LOG_ENTITY_UPDATE_USER = 262;
    public final static int WSCODE_LOG_ENTITY_UPDATE_SITE = 264;

    //ANALYTICS
    public final static int WSCODE_ANALYTICS_USER_LAUCH_SUCCESS = 270;
    public final static int WSCODE_ANALYTICS_USER_LAUCH_FAIL = 271;
    public final static int WSCODE_ANALYTICS_USER_CLOSE_SUCCESS = 272;
    public final static int WSCODE_ANALYTICS_USER_CLOSE_FAIL = 273;

    //LEAVE
    public final static int WSCODE_LEAVE_FETCH_SUSCCESS = 280;
    public final static int WSCODE_LEAVE_FETCH_FAIL = 281;
    public final static int WSCODE_LEAVE_FETCH_NO_LEAVE = 282;
    public final static int WSCODE_LEAVE_SAVE_SUCCESS = 283;
    public final static int WSCODE_LEAVE_SAVE_FAIL = 284;
    public final static int WSCODE_LEAVE_UPDATE_SUCCESS = 285;
    public final static int WSCODE_LEAVE_UPDATE_FAIL = 286;
    public final static int WSCODE_LEAVE_DELETE_SUCCESS = 287;
    public final static int WSCODE_LEAVE_DELETE_FAIL = 288;
    public final static int WSCODE_LEAVE_TYPE_FETCH_SUCCESS = 289;
    public final static int WSCODE_LEAVE_TYPE_FETCH_FAIL = 290;
    public final static int WSCODE_LEAVE_TYPE_FETCH_NO_LEAVE_TYPE = 291;

    //HAZARD
    public final static int WSCODE_HAZARD_SUCCESS = 300;
    public final static int WSCODE_HAZARD_FAIL = 301;
    public final static int WSCODE_HAZARD_NO_HAZARD = 302;
    public final static int WSCODE_HAZARD_SAVE_SUCCESS = 303;
    public final static int WSCODE_HAZARD_SAVE_FAIL = 304;
    public final static int WSCODE_HAZARD_ACTION_SAVE_SUCCESS = 305;
    public final static int WSCODE_HAZARD_ACTION_SAVE_FAIL = 306;
    public final static int WSCODE_HAZARD_UPDATE_SUCCESS  = 307;
    public final static int WSCODE_HAZARD_UPDATE_FAIL = 308;
    public final static int WSCODE_HAZARD_ACTION_UPDATE_SUCCESS = 309;
    public final static int WSCODE_HAZARD_ACTION_UPDATE_FAIL = 310;
    public final static int WSCODE_HAZARD_CLOSE_SUCCESS = 311;
    public final static int WSCODE_HAZARD_CLOSE_FAIL = 312;
    public final static int WSCODE_HAZARD_ACTION_CLOSE_SUCCESS = 313;
    public final static int WSCODE_HAZARD_ACTION_CLOSE_FAIL = 314;
    public final static int WSCODE_HAZARD_SHARE_SUCCESS = 315;
    public final static int WSCODE_HAZARD_SHARE_FAIL = 316;
    public final static int WSCODE_HAZARD_DETAIL_SUCCESS = 317;
    public final static int WSCODE_HAZARD_DETAIL_FAIL = 318;

    //LOCATION BACKGROUND
    public final static int WSCODE_LOCATION_SAVE_SUCCESS = 320;
    public final static int WSCODE_LOCATION_SAVE_FAIL = 321;

    //MESSAGE
    public final static int WSCODE_MESSAGE_USER_SUCCESS = 330;
    public final static int WSCODE_MESSAGE_USER_FAIL = 331;
    public final static int WSCODE_MESSAGE_SUCCESS = 332;
    public final static int WSCODE_MESSAGE_FAIL = 333;
    public final static int WSCODE_MESSAGE_SEND_SUCCESS = 334;
    public final static int WSCODE_MESSAGE_SEND_FAIL = 335;
    public final static int WSCODE_MESSAGE_SAVE_SUCCESS = 336;
    public final static int WSCODE_MESSAGE_SAVE_FAIL = 337;
    public final static int WSCODE_MESSAGE_LEAVE_CHAT_SUCCESS = 338;
    public final static int WSCODE_MESSAGE_LEAVE_CHAT_FAIL = 339;

    //DOCUMENTS
    public final static int WSCODE_DOCUMENT_FETCH_SUCCESS = 340;
    public final static int WSCODE_DOCUMENT_FETCH_FAIL = 341;

    protected static WebservicesHelper instance;

    public WebservicesHelper() {
        clientRequest = new ClientRequestCustom();
        clientRequest.setTimeout(Constants.SECONDS_IN_AMINUTE * 3 * 1000);
    }

    public static WebservicesHelper getInstance() {
        if (instance == null) {
            instance = new WebservicesHelper();
        }
        return instance;
    }

    /**
     * Main method to connect to webservice that will handle all the api connection
     * @param context default context
     * @param iLoading interface for beating hear animated
     * @param webservice the callback handler
     * @param url route address to the api call
     * @param body the parameters required for the api call
     */
    public void webserviceRequest(final Context context, final ILoading iLoading,final IWebservice webservice, final String url, final String body) {

        Log.e("WS url request", "=> " + url);
        Log.e("WS post request", "=> " + body);

        try {

            if (Utilities.isNetworkOnline(context)) {

                StringEntity bodyString = new StringEntity(body, "UTF-8");
                clientRequest.addHeader("Content-Type", CONTENT_TYPE);
                clientRequest.post(context, url, bodyString, CONTENT_TYPE,new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode,Header[] headers, byte[] responseBody) {

                        try {
                            String WSResponse = new String(responseBody,"UTF-8");
                            JSONObject WSJsonObj = new JSONObject(WSResponse);

                            if (WSJsonObj != null) {

                                String WSMessage = WSJsonObj.getString("message");
                                int WSStatusCode = Integer.parseInt(WSJsonObj.getString("status"));
                                int WSCode = Integer.parseInt(WSJsonObj.getString("WSResponseCode"));
                                Log.e("WSCode:", WSCode + " ::: WSMessage => " + WSMessage);

                                if (WSStatusCode == 1) {
                                    webservice.onComplete(WSJsonObj, WSMessage, WSCode);
                                } else {

                                    if (WSCode == WSCODE_LOGIN_ACCESS_DENIED) {
                                        webservice.onAccessDenied(WSMessage);
                                        return;
                                    }

                                    webservice.onComplete(WSJsonObj, WSMessage, WSCode);
                                }
                            }

                        } catch (JSONException e) {
                            webservice.onError(Constants.MSG_GENERAL_ERROR);

                        } catch (Exception ex){
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode,Header[] headers, byte[] responseBody,Throwable error) {

                        if(responseBody != null) {
                            Log.e("WS error:"," => " + new String(responseBody));
                            webservice.onError(Constants.MSG_GENERAL_ERROR);
                        }


                    }

                    @Override
                    public void onStart() {
                        super.onStart();
                        iLoading.showLoading();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                    }

                });
            } else {
                webservice.onError(context.getResources().getString(R.string.msg_lost_connection));
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    /**
     * Main method to connect to webservice that will handle all the api connection for json request
     * @param context default context
     * @param iLoading interface for beating hear animated
     * @param webservice the callback handler
     * @param url route address to the api call
     * @param body the parameters required for the api call with json format
     */
    public void webserviceRequestWithJson(final Context context, final ILoading iLoading,final IWebservice webservice, final String url, final String body) {

        Log.e("WS url request", "=> " + url);
        Log.e("WS post request", "=> " + body);

        try {

            if (Utilities.isNetworkOnline(context)) {

                StringEntity bodyString = new StringEntity(body, "UTF-8");
                clientRequest.addHeader("Content-Type", CONTENT_TYPE);
                clientRequest.post(context, url, bodyString, CONTENT_TYPE,new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode,Header[] headers, byte[] responseBody) {

                        try {
                            try {

                                String WSResponse = new String(responseBody,"UTF-8");
                                JSONObject WSJsonObj = new JSONObject(WSResponse);

                                if (WSJsonObj != null) {

                                    String WSMessage = WSJsonObj.getString("message");
                                    Log.e("WSMessage", "=> " + WSMessage);
                                    int WSStatusCode = Integer.parseInt(WSJsonObj.getString("status"));
                                    int WSCode = Integer.parseInt(WSJsonObj.getString("WSResponseCode"));

                                    if (WSStatusCode == 1) {
                                        webservice.onComplete(WSJsonObj, WSMessage, WSCode);
                                    } else {

                                        if (WSCode == WSCODE_LOGIN_ACCESS_DENIED) {
                                            webservice.onAccessDenied(WSMessage);
                                            return;
                                        }

                                        webservice.onError(WSMessage);
                                    }
                                }

                            } catch (JSONException e) {
                                webservice.onError(Constants.MSG_GENERAL_ERROR);
                                e.printStackTrace();
                            }

                        } catch (UnsupportedEncodingException e) {
                            webservice.onError(Constants.MSG_GENERAL_ERROR);
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode,Header[] headers, byte[] responseBody,Throwable error) {
                        Log.e("WS error:"," => " + new String(responseBody));
                        webservice.onError(Constants.MSG_GENERAL_ERROR);
                    }

                    @Override
                    public void onStart() {
                        super.onStart();
                        iLoading.showLoading();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                    }

                });
            } else {
                webservice.onError(context.getResources().getString(R.string.msg_lost_connection));
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }


    public void webserviceUploadImage(final IUploadImage webservice, final String folder, final int refId) {

        String sourceFileUri = Environment.getExternalStorageDirectory().getAbsolutePath().toString() + "/" + Constants.TEMP_PHOTO_FILE_NAME;

        // DEBUGGING
        Log.d(TAG, "webserviceUploadImage() - imageUri = " + sourceFileUri);
        String filenames;

        //If hazard or hazard action parse another parameter to be used as reference id
        if (folder.equals(WebservicesHelper.ROUTE_FOLDER_USER)){
            filenames = String.format("%s&%s&%s", SBApplication.getUserModel().personId, SBApplication.getUserModel().token,folder);
        } else {
            filenames = String.format("%s&%s&%s&%s", SBApplication.getUserModel().personId, SBApplication.getUserModel().token,folder,refId);
        }

        int serverResponseCode = 0;
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "---------------------------14737809831466499882746641449";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1024 * 1024;
        File sourceFile = new File(sourceFileUri);

        try {
            FileInputStream fileInputStream = new FileInputStream(sourceFile);
            URL url = new URL(ROUTE_IMAGE_UPLOAD);
            // Open a HTTP connection to the URL
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type","multipart/form-data;boundary=" + boundary);

            dos = new DataOutputStream(conn.getOutputStream());
            dos.writeBytes(lineEnd + twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; companyName=\"filenames\"" + lineEnd + lineEnd);
            dos.writeBytes(filenames);
            dos.writeBytes(lineEnd + twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; companyName=\"userfile\"; filename=\".jpg\"" + lineEnd);
            dos.writeBytes("Content-Type: application/octet-stream" + lineEnd + lineEnd);

            // create a buffer of maximum size
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // read file and write it into form...
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {

                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            }

            // send multipart form data necessary after file data...
            dos.writeBytes(lineEnd + twoHyphens + boundary + twoHyphens + lineEnd);
            // close the streams //
            fileInputStream.close();
            dos.flush();
            dos.close();
            // Get Response
            // Responses from the server (code and message)
            serverResponseCode = conn.getResponseCode();

            InputStream is = conn.getInputStream();
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }

            rd.close();

            Log.e("WSResponse " , " => " + response.toString());
            if (serverResponseCode == 200) {
                webservice.onComplete(serverResponseCode);
            } else {
                webservice.onError(response.toString());
            }

        } catch (MalformedURLException ex) {
            ex.printStackTrace();
            webservice.onError("MalformedURLException");
        } catch (IOException ex) {
            ex.printStackTrace();
            webservice.onError(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            webservice.onError(ex.getMessage());
        }

    }

    public void webserviceLoadImage(ImageView viewAvatar, int imgRoute, final int refId){

        viewAvatar.setImageBitmap(null);

        //Load user image
        final String avatarUrl;
        if (imgRoute == Constants.IMG_TYPE_USER){
            avatarUrl = String.format(LOAD_USER_IMAGE_URL, refId,refId);
            viewAvatar.setImageResource(R.mipmap.icon_small_avatar);
        } else if ( imgRoute == Constants.IMG_TYPE_ENTITY ) {
            avatarUrl = String.format(LOAD_ENTITY_IMAGE_URL, refId,refId);
            viewAvatar.setImageResource(R.mipmap.icon_entity);
        }else if ( imgRoute == Constants.IMG_TYPE_HAZARD ) {
            avatarUrl = String.format(LOAD_HAZARD_IMAGE_URL, refId,refId);
            viewAvatar.setImageResource(R.mipmap.ic_hazard_grey);
        }else if ( imgRoute == Constants.IMG_TYPE_HAZARD_ACTION ) {
            avatarUrl = String.format(LOAD_HAZARD_ACTION_IMAGE_URL, refId,refId);
            viewAvatar.setImageResource(R.mipmap.ic_hazard_grey);
        } else {
            return;
        }

        final Bitmap bmp = Utilities.loadCachedImage(avatarUrl);
        if (bmp != null) {
            viewAvatar.setImageBitmap(bmp);
        } else {

            if (!TextUtils.isEmpty(avatarUrl) && !avatarUrl.equalsIgnoreCase("null")) {
                ImageLoader.getInstance().displayImage(avatarUrl,
                        viewAvatar, new ImageLoadingListener() {

                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                if (loadedImage != null) {
                                    Utilities.cacheAvatarImage(loadedImage, avatarUrl);
                                }
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {
                            }
                        });
            }
        }
    }

    /**
     * Main method to connect to webservice that will handle all the api connection
     * @param context default contex
     * @param iGetAddress the callback handler
     * @param lat latitude for the desire address
     * @param lng longitude for the desire address
     */
    public void webserviceGetAddress(final Context context,final IGetAddress iGetAddress, final double lat, final double lng) {

        if (Utilities.isNetworkOnline(context)) {

            String url = String.format(Locale.ENGLISH, GET_ADDRESS, lat, lng);
            clientRequest.get(context, url, new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode,Header[] headers, byte[] responseBody) {

                    try {

                        String str = new String(responseBody, "UTF-8");
                        JSONObject WSResponse = new JSONObject(str);

                        if (WSResponse.getString("status").equals("OK")) {
                            JSONArray localJSONArray = WSResponse.getJSONArray("results");
                            if (localJSONArray.length() > 0) {
                                if (localJSONArray.getJSONObject(0).has("address_components")) {
                                    iGetAddress.onComplete(localJSONArray.getJSONObject(0).getJSONArray("address_components"));
                                }else{
                                    iGetAddress.onError(Constants.MSG_GENERAL_ERROR);
                                }
                            }else{
                                iGetAddress.onError(Constants.MSG_GENERAL_ERROR);
                            }
                        }else{
                            iGetAddress.onError(Constants.MSG_GENERAL_ERROR);
                        }

                    } catch (JSONException e) {
                        iGetAddress.onError(Constants.MSG_GENERAL_ERROR);

                    } catch (Exception ex){
                        ex.printStackTrace();
                        iGetAddress.onError(Constants.MSG_GENERAL_ERROR);
                    }

                }

                @Override
                public void onFailure(int statusCode,Header[] headers, byte[] responseBody,Throwable error) {

                    if(responseBody != null) {
                        Log.e("WS error:"," => " + new String(responseBody));
                        iGetAddress.onError(Constants.MSG_GENERAL_ERROR);
                    }

                }

                @Override
                public void onStart() {
                    super.onStart();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                }

            });
        } else {
            iGetAddress.onError(context.getResources().getString(R.string.msg_lost_connection));
        }

    }

}

