package com.clockme.utilities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.clockme.R;
import com.clockme.networking.WebservicesHelper;

public class LoadingBeat {
	Context ctx;
	ImageView img;
	Animation animHeartBeat; // declared as public
	Dialog dialog;
	private Boolean isShowing = false;

	public LoadingBeat(Context mContext) {
		ctx = mContext;
		animHeartBeat = AnimationUtils.loadAnimation(mContext, R.anim.animation_heartbeat);

		// custom dialog
		//dialog = new Dialog((Activity)mContext);
		dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.loading_beat);
		dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		img = (ImageView) dialog.findViewById(R.id.imgLogo);
		img.setVisibility(View.VISIBLE);
	}

	public void showLoading() {

		if (!isShowing){

			if(dialog != null && !dialog.isShowing() && !((Activity)ctx).isFinishing()) {
				img.startAnimation(animHeartBeat);
				dialog.show();
			}

			isShowing = true;
		}
	}

	public void cancelLoading() {

		if(WebservicesHelper.clientRequest != null) {
			WebservicesHelper.clientRequest.cancelAllRequests(true);
		}

		animHeartBeat.cancel();
		if ((dialog != null) && dialog.isShowing()) {
			dialog.dismiss();
			isShowing = false;
		}
	}

	public void destroyLoading() {
		if(WebservicesHelper.clientRequest != null) {
			WebservicesHelper.clientRequest.cancelAllRequests(true);
		}
		animHeartBeat.cancel();
		if (dialog != null) {
			dialog.dismiss();
			isShowing = false;
		}
	}
}
