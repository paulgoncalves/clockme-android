package com.clockme.utilities;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.clockme.R;


/**
 * Created by tannguyen on 5/6/15.
 */
public class RequestConnectionDialog extends Dialog implements View.OnClickListener{

    private Context mContext;
    private RelativeLayout rlDialogRootView;
    private TextView tvTitle;
    private TextView tvMessage;
    private Button btnYES;
    private Button btnCANCEL;
    private Button btnMember;
    private Button btnFollow;
    private Button btnLeader;
    public int intPermission = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_request_connection_dialog);
        getWindow().setLayout(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
        initUI();
    }


    public RequestConnectionDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    public RequestConnectionDialog(Context context, int theme) {
        super(context, theme);
        this.mContext = context;
    }

    protected RequestConnectionDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.mContext = context;
    }


    private void initUI() {
        rlDialogRootView = (RelativeLayout) findViewById(R.id.rlDialogRootView);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvMessage = (TextView) findViewById(R.id.tvMessage);
        btnYES = (Button) findViewById(R.id.btnYES);
        btnCANCEL = (Button) findViewById(R.id.btnCANCEL);

        btnMember = (Button) findViewById(R.id.btnMember);
        btnFollow = (Button) findViewById(R.id.btnFollow);
        btnLeader = (Button) findViewById(R.id.btnLeader);

        btnMember.setOnClickListener(this);
        btnFollow.setOnClickListener(this);
        btnLeader.setOnClickListener(this);

        btnMember.setBackgroundResource(R.drawable.border_left);
        btnFollow.setBackgroundColor(Color.GRAY);
        btnLeader.setBackgroundResource(R.drawable.border_right_gray);
        intPermission = 1;

        btnFollow.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View v) {

        if(v == btnMember){

            if(mMode == Constants.CONNECTION_TYPE_FOLLOWER){
                btnMember.setBackgroundResource(R.drawable.border_left);
                btnFollow.setBackgroundResource(R.drawable.border_right_gray);
            }
            else{
                btnMember.setBackgroundResource(R.drawable.border_left);
                btnFollow.setBackgroundColor(Color.GRAY);
                btnLeader.setBackgroundResource(R.drawable.border_right_gray);
            }

            intPermission = Constants.CONNECTION_TYPE_TEAM_MEMBER;
        }
        else if (v == btnFollow){

            if(mMode == Constants.CONNECTION_TYPE_FOLLOWER){
                btnMember.setBackgroundResource(R.drawable.border_left_gray);
                btnFollow.setBackgroundResource(R.drawable.border_right);
            }
            else{
                btnMember.setBackgroundResource(R.drawable.border_left_gray);
                btnFollow.setBackgroundColor(Color.YELLOW);
                btnLeader.setBackgroundResource(R.drawable.border_right_gray);
            }

            intPermission = Constants.CONNECTION_TYPE_FOLLOWER;
        }
        else if (v == btnLeader){

            btnMember.setBackgroundResource(R.drawable.border_left_gray);
            btnFollow.setBackgroundColor(Color.GRAY);
            btnLeader.setBackgroundResource(R.drawable.border_right);

            intPermission = Constants.CONNECTION_TYPE_TEAM_LEADER;
        }

    }

    public void setContent(String title, String message, int mode, String btnText1, String btnText2,
                           IRequestConnectionDialog iRequestConnectionDialog) {
        tvTitle.setText(title);
        tvMessage.setText(message);

        if (!TextUtils.isEmpty(btnText1)) {
            btnYES.setText(btnText1);
        } else {
            btnYES.setText("YES");
        }

        if (!TextUtils.isEmpty(btnText2)) {
            btnCANCEL.setText(btnText2);
        } else {
            btnCANCEL.setText("CANCEL");
        }

        setButtonMode(mode);
        setButtonListener(iRequestConnectionDialog);
    }

    int mMode = 0;
    private void setButtonMode(int mode) {

        mMode = mode;
        if (mode == Constants.CONNECTION_TYPE_TEAM_LEADER) {
            btnFollow.setVisibility(View.GONE);
            btnMember.setVisibility(View.VISIBLE);
            btnLeader.setVisibility(View.VISIBLE);

            Log.i("CONNECTION", "connType = " + mode + "   CONNECTION_TYPE_MEMBER & CONNECTION_TYPE_LEADER");
        }
        else if (mode == Constants.CONNECTION_TYPE_FOLLOWER){
            btnFollow.setVisibility(View.VISIBLE);
            btnFollow.setBackgroundResource(R.drawable.border_right_gray);
            btnMember.setVisibility(View.VISIBLE);
            btnLeader.setVisibility(View.GONE);

            Log.i("CONNECTION", "connType = " + mode + "   CONNECTION_TYPE_MEMBER & FOLLOWER");

        }
        else if (mode == Constants.CONNECTION_TYPE_LEADER_FOLLOWER) {
            btnFollow.setVisibility(View.VISIBLE);
            btnFollow.setBackgroundColor(Color.GRAY);
            btnMember.setVisibility(View.VISIBLE);
            btnLeader.setVisibility(View.VISIBLE);

            Log.i("CONNECTION", "connType = " + mode + "   CONNECTION_TYPE_MEMBER & FOLLOWER & CONNECTION_TYPE_LEADER");
        }
    }

    private void setButtonListener(final
            IRequestConnectionDialog iRequestConnectionDialog) {

        btnYES.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iRequestConnectionDialog.onOk(intPermission);
                dismiss();
            }
        });

        btnCANCEL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iRequestConnectionDialog.onCancel();
                dismiss();
            }
        });

    }



}
