package com.clockme.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import eu.janmuller.android.simplecropimage.CropImage;

/**
 * Utility class to handle creating temp file
 * for image
 * Created by AppDev on 15/10/15.
 */
public class ImageUtil {

    private static final String TAG = ImageUtil.class.getSimpleName();
    private static File mFileTemp;
    //private static Bitmap mBitMap;
    private static Context mContext;
    //private static InputStream mInputStream;


//    public ImageUtil(Context context, InputStream inputStream) {
//        mContext = context;
//        //mInputStream = inputStream;
//    }

    public static void createTempFile(Context context, InputStream inputStream) {

        mContext = context;

        // TODO: 29/09/15 Handle when user select image from external directory
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(),Constants.TEMP_PHOTO_FILE_NAME);
        } else {
            mFileTemp = new File(mContext.getFilesDir(), Constants.TEMP_PHOTO_FILE_NAME);
        }


        try {

            FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
            copyStream(inputStream, fileOutputStream);
            fileOutputStream.close();
            inputStream.close();

            //startCropImage();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error => " + e.getMessage());
            Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }


    public static void startCropImage() {

        Activity activity;

        if(mContext instanceof WizardActivity) {
            activity = ((WizardActivity)mContext);
        } else {
            activity = ((HazardDetailActivity)mContext);
        }

        Intent intent = new Intent(mContext, CropImage.class);
        if (mFileTemp != null) {
            intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
            intent.putExtra(CropImage.SCALE, true);
            intent.putExtra(CropImage.ASPECT_X, 2);
            intent.putExtra(CropImage.ASPECT_Y, 2);

            activity.startActivityForResult(intent, Constants.REQUEST_CODE_CROP_IMAGE);

        }
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }
}
