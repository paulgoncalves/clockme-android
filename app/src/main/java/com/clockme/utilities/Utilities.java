package com.clockme.utilities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.BatteryManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.clockme.SBApplication;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utilities {

	public static boolean checkEmailFormat(String email) {
		boolean result = false;

		// old regex not allowing a.horogh@auroraaustralis.co.uk
		/*
		if (!email.matches("[a-zA-Z0-9_.%+]+@[a-zA-Z0-9]+\\.[a-zA-Z]{2,3}")
				&& !email
						.matches("[a-zA-Z0-9_.%+]+@[a-zA-Z0-9]+\\.[a-zA-Z]{3}+\\.[a-zA-Z]{2,3}")) {
			result = false;
		} else {
			if (email.contains(".web")) {
				result = false;
			} else
				result = true;
		}
		*/

		String emailExpression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(emailExpression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches())
		{
			result = true;
		}

		return result;
	}

	public static void showKeyboard(Context context, View view) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
	}

	public static void hideKeyboard(Context context, View view) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	public static boolean isNetworkOnline(Context mContext) {
		if (mContext != null) {
			ConnectivityManager cm = (ConnectivityManager) mContext
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			return (cm.getActiveNetworkInfo() != null
					&& cm.getActiveNetworkInfo().isAvailable() && cm
					.getActiveNetworkInfo().isConnected());
		}
		else{
			return false;
		}
	}

	public static String convertBytesArrayToString(byte[] responseBody) {
		try {
			return new String(responseBody, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

	public static String getMD5String(String str) {
		if (str == null || str.length() == 0) {
			return "";
		}
		StringBuffer hexString = new StringBuffer();

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes(), 0, str.getBytes().length);
			byte[] temp = new byte[str.getBytes().length];
			int lenResult = md.digest(temp, 0, temp.length);

			byte[] hash = new byte[lenResult];
			for (int i = 0; i < lenResult; i++) {
				hash[i] = temp[i];
			}

			for (int i = 0; i < hash.length; i++) {
				if ((0xff & hash[i]) < 0x10) {
					hexString.append("0"
							+ Integer.toHexString((0xFF & hash[i])));
				} else {
					hexString.append(Integer.toHexString(0xFF & hash[i]));
				}
			}
		} catch (NoSuchAlgorithmException e) {
			return "";
		} catch (DigestException e) {
			return "";
		}
		return hexString.toString();
	}

	/**
	 * Calculator distance between 2 points
	 * */
	private static final int earthRadius = 6371;

	public static double calculateDistance(LatLng siteLocation, LatLng userLocation) {

		try {

			double dLat = Math.toRadians(userLocation.latitude - siteLocation.latitude);
			double dLon = Math.toRadians(userLocation.longitude - siteLocation.longitude);
			double a = (Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math
					.cos(Math.toRadians(siteLocation.latitude))
					* Math.cos(Math.toRadians(userLocation.latitude))
					* Math.sin(dLon / 2) * Math.sin(dLon / 2));
			double c = (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
			double d = (earthRadius * c);
			return round(d, 1);

		}catch (Exception ex){
			ex.printStackTrace();
			return 0;
		}
	}

	/**
	 * round
	 * */
	public static double round(double value, int places) {

		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	/**
	 * last report time + entity reportTime ( ex: 2 hours )
	 * */
	public static String getTimeReport() {

		Calendar reportTime = Calendar.getInstance();
		reportTime.setTimeZone(TimeZone.getTimeZone(SBApplication.getUserModel().timezone));
		reportTime.setTimeInMillis((long)SBApplication.getSiteModel().usedTime * 1000);
		reportTime.add(Calendar.HOUR_OF_DAY, SBApplication.getEntityModel().reportTime);

		int hour = reportTime.get(Calendar.HOUR_OF_DAY);
		int minute = reportTime.get(Calendar.MINUTE);
		String h = "", m = "";
		if (hour < 10) {
			h = "0";
		}

		if (minute < 10) {
			m = "0";
		}
		return h + hour + ":" + m + minute;
	}

	/**
	 * 
	 * get battery level
	 * */
	public static float getBatteryLevel(Context context) {

		Intent batteryIntent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
		int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

		// Error checking that probably isn't needed but I added just in case.
		if (level == -1 || scale == -1) {
			return 50.0f;
		}

		return (int)( (float) level / (float) scale * 100.0f);
	}

	public static Bitmap loadCachedImage(String key) {

		Bitmap bmp = SBApplication.imageCache.get(key);
		if (bmp != null) {
			return bmp;
		} else {
			File imageFile = new File(SBApplication.storageDir, key);
			if (imageFile.exists()) {

				// try and first get from local storage
				try {
					byte[] imageBytes = convertFileToByteArray(imageFile);
					bmp = BitmapFactory.decodeByteArray(imageBytes, 0,
							imageBytes.length);
					SBApplication.imageCache.put(key, bmp);
					return bmp;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return bmp;
	}

	public static byte[] convertFileToByteArray(File f) {
		byte[] byteArray = null;
		try {
			InputStream inputStream = new FileInputStream(f);
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] b = new byte[1024 * 10];
			int bytesRead = 0;

			while ((bytesRead = inputStream.read(b)) != -1) {
				bos.write(b, 0, bytesRead);
			}

			byteArray = bos.toByteArray();
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return byteArray;
	}

	public static boolean cacheAvatarImage(Bitmap bmp, String key) {

		File imageFile = new File(SBApplication.storageDir, key);
		SBApplication.imageCache.put(key, bmp);

		FileOutputStream out = null;
		try {
			out = new FileOutputStream(imageFile);
			bmp.compress(Bitmap.CompressFormat.PNG, 25, out);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.flush();
					out.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	public static boolean clearCacheImage(String key) {
		SBApplication.imageCache.remove(key);
		File imageFile = new File(SBApplication.storageDir, key);
		if (imageFile.exists()) {
			// try and first get from local storage
			try {

				return imageFile.delete();

			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	public static void makeAPhoneCall(Context context, String number) {
		try {
			Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
					+ number));
			context.startActivity(intent);
			return;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static void sendEmail(Context context, String email) {
		final Intent emailIntent = new Intent(
				android.content.Intent.ACTION_SEND);
		emailIntent.setType("plain/text");
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
				new String[] { email });
		context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
	}

	public static boolean isLongNumber(String str)
	{
		try
		{
			long d = Long.parseLong(str);
		}
		catch(NumberFormatException nfe)
		{
			return false;
		}
		return true;
	}

	/**
	 * Method to get String from JsonObject
	 * */
	public static String getStringValue(JSONObject value, String key){
		try {
			return value.getString(key);
		}catch (JSONException ex){
			ex.printStackTrace();
			return "";
		}
	}

	/**
	 * Method to get int from JsonObject
	 * */
	public static int getIntValue(JSONObject value, String key){
		try {
			return value.getInt(key);
		}catch (JSONException ex){
			ex.printStackTrace();
			return 0;
		}
	}

	/**
	 * Method to get Double from JsonObject
	 * */
	public static Double getDoubleValue(JSONObject value, String key){
		try {
			return value.getDouble(key);
		}catch (JSONException ex){
			ex.printStackTrace();
			return 0.0;
		}
	}

	/**
	 * Method to get Boolean from JsonObject
	 * */
	public static Boolean getBooleanValue(JSONObject value, String key){
		try {
			if (value.getString(key).equals("1")){
				return true;
			}else{
				return false;
			}
		}catch (JSONException ex){
			ex.printStackTrace();
			return false;
		}
	}

	/**
	 * Method to get String from boolean
	 * */
	public static String getBooleanString(boolean value){
		if (value){
			return "1";
		}else{
			return "0";
		}
	}

	public static String validateString(String value){
		if (value != null){
			return value;
		}else{
			return "";
		}
	}

	public static int validateInt(int value){
		if (value != 0){
			return value;
		}else{
			return 0;
		}
	}

	public static double validateDouble(double value){
		if (value != 0){
			return value;
		}else{
			return 0;
		}
	}

	public static String encodeString(String uncoded){
		try {
			return URLDecoder.decode(uncoded, "UTF-8");
		}catch (Exception ex){
			ex.printStackTrace();
			return "";
		}
	}

}
