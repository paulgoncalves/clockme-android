package com.clockme.utilities;

public class Constants {

	public static final String WEBSITE = "http://clockme.co";
    public static final String PROJECT_NUMBER = "23657452796";

	public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
	public static final int REQUEST_CODE_GALLERY = 0x1;
	public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
	public static final int REQUEST_CODE_CROP_IMAGE = 0x3;

	// ACTIVITIES & FRAGMENTS
	//MAIN
	public static final int FRAGMENT_HOME = 0;
	public static final int FRAGMENT_LOGIN = 1;
	public static final int FRAGMENT_MENU = 2;

	//PERSON
	public static final int FRAGMENT_PROFILE = 10;

	//SCHEDULE
	public static final int FRAGMENT_SCHEDULE = 20;

	//NOTE
	public static final int FRAGMENT_ACTION_NOTE_ADD = 31;

	//TEAM
	public static final int FRAGMENT_TEAM_STATUS = 40;


	public final static double DEFAULT_LAT = -37.851729;
	public final static double DEFAULT_LNG = 145.103997;
	
	public final static double DELTA_LAT = 0.0025;
	public final static double DELTA_LNG = 0.0016;

	public final static double DISTANCE_FROM_ME = 0.8;

	public final static int SECONDS_IN_AMINUTE = 60;
	public final static int SECONDS_IN_AHOUR = 3600;
	public final static int SECONDS_IN_ADAY = 86400;
	public final static int MINUTES_IN_AHOUR = 60;

	public final static String JSON="json";
	public final static String KEY_REFRESH = "1";
	public static final int THEME = android.R.style.Theme_Translucent_NoTitleBar;

	//Image types
	public static final int IMG_TYPE_USER = 1;
	public static final int IMG_TYPE_ENTITY = 2;

	public static final String MSG_GENERAL_ERROR = "Error occur, please try again";

	public static final int SECONDS_IN_A_MINUTE = 60;
	public static final int SECONDS_IN_A_HOUR = 3600;
	public static final int SECONDS_IN_A_DAY = 86400;
	public static final int MINUTES_IN_A_HOUR = 60;

	/// CUSTOM FRAGMENT ANIMATION
	public static final int ANIMATION_VIEW_TYPE_ADD = 1;
	public static final int ANIMATION_VIEW_TYPE_SLIDE = 2;
	public static final int ANIMATION_VIEW_TYPE_FADE = 3;
	public static final int ANIMATION_VIEW_TYPE_DISMISS = 4;
	public static final int ANIMATION_VIEW_TYPE_PRESENT = 5;

	//Access level type
	public static final int USER_ACCESS_LEVEL_MANAGER = 1;
	public static final int USER_ACCESS_LEVEL_NORMAL = 2;

}
