package com.clockme.utilities;

public class MyMarker {
	private String mLabel;
	private double mLatitude;
	private double mLongitude;
	private String address;
	int count = -1;
	private long id;
	/**
	 * 0 : group
	 * 1 : beach 
	 * 2 : favorite
	 * 3 : top and favorite
	 */
	private int mode = 0;
	public MyMarker(String label, double latitude, double longitude,
			String address, int count,long id, int mode) {
		this.mLabel = label;
		this.mLatitude = latitude;
		this.mLongitude = longitude;
		this.address = address;
		this.count = count;
		this.id=id;
		this.mode = mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}
	
	public int getMode() {
		return mode;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getmLabel() {
		return mLabel;
	}

	public void setmLabel(String mLabel) {
		this.mLabel = mLabel;
	}

	public double getmLatitude() {
		return mLatitude;
	}

	public void setmLatitude(double mLatitude) {
		this.mLatitude = mLatitude;
	}

	public double getmLongitude() {
		return mLongitude;
	}

	public void setmLongitude(double mLongitude) {
		this.mLongitude = mLongitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String toString()
	{
		return "companyName="+mLabel+"; address="+address;
	}
}
