package com.clockme.utilities;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePref {

	public static final String SHARE_PREFERENCES_NAME = "clockme_SHARED_PREFERENCES";
	private static final String USER_INFO = "USER_INFO";
	private static final String SITE_INFO = "SITE_INFO";
	private static final String ENTITY_INFO = "ENTITY_INFO";

	private static final String SCREEN_WIDTH = "SCREEN_WIDTH";
    private static final String TOKEN_PUSH = "TOKEN_PUSH";
    private static final String UPDATED_TOKEN_PUSH = "UPDATED_TOKEN_PUSH";
    private static final String TOTAL_PUSH = "TOTAL_PUSH";
    private static final String HAS_PUSH = "HAS_PUSH";
	private static final String CATEGORY = "CATEGORY";
	private static final String ALL_PROJECT = "ROUTE_SITE_PROJECT";
	private static final String SHOW_GAUSE = "SHOW_GAUSE";
	private static final String ANSWER_RESULT = "ANSWER_RESULT";
	private static final String SITE_JSON_TEMP = "SITE_JSON_TEMP";
	private static final String CATEGORY_ID_TEMP = "CATEGORY_ID_TEMP";


	static final String USER_ID = "USER_ID";
	static final String USERNAME = "USERNAME";
	static final String PASSWORD = "PASSWORD";
	static final String TOKEN = "TOKEN";
	static final String LATITUDE = "LATITUDE";
	static final String LONGITUDE = "LONGITUDE";
	private static final String SITE_NOT_CHECKOUT = "SITE_NOT_CHECKOUT";
	private static final String CHECK_IN_ID = "CHECK_IN_ID";
	private static final String ACCESS_LEVEL = "ACCESS_LEVEL";


	public static void setUserInfo(Context ctx, String userInfo) {
		SharedPreferences myPrefs = ctx.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
		myPrefsEdit.putString(USER_INFO, userInfo);
		myPrefsEdit.commit();
	}

	public static String getUserInfo(Context ctx) {
		SharedPreferences myPrefs = ctx.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
		return myPrefs.getString(USER_INFO, "");
	}

	//Save entityInfo model to phone memory
	public static void setEntityInfo(Context ctx, String entityInfo) {
		SharedPreferences myPrefs = ctx.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
		myPrefsEdit.putString(ENTITY_INFO, entityInfo);
		myPrefsEdit.commit();
	}

	public static String getEntityInfo(Context ctx) {
		SharedPreferences myPrefs = ctx.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
		return myPrefs.getString(ENTITY_INFO, "");
	}

	//Save siteInfo model to phone memory
	public static void setSiteInfo(Context ctx, String siteInfo) {
		SharedPreferences myPrefs = ctx.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
		myPrefsEdit.putString(SITE_INFO, siteInfo);
		myPrefsEdit.commit();
	}

	public static String getSiteInfo(Context ctx) {
		SharedPreferences myPrefs = ctx.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
		return myPrefs.getString(SITE_INFO, "");
	}


	//Save user toke on phone memory
    public static void setTokenPush(Context ctx, String token_push) {
        SharedPreferences myPrefs = ctx.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
        SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
        myPrefsEdit.putString(TOKEN_PUSH, token_push);
        myPrefsEdit.commit();
    }

    public static String getTokenPush(Context ctx) {
        SharedPreferences myPrefs = ctx.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
        return myPrefs.getString(TOKEN_PUSH, "");
    }






    public static void setHasPush(Context ctx, boolean hasPush) {
        SharedPreferences myPrefs = ctx.getSharedPreferences(
                SHARE_PREFERENCES_NAME, 0);
        SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
        myPrefsEdit.putBoolean(HAS_PUSH, hasPush);
        myPrefsEdit.commit();
    }

    public static boolean hasPush(Context ctx) {
        SharedPreferences myPrefs = ctx.getSharedPreferences(
                SHARE_PREFERENCES_NAME, 0);
        return myPrefs.getBoolean(HAS_PUSH, false);
    }

    public static void setTotalPush(Context ctx, int total) {
        SharedPreferences myPrefs = ctx.getSharedPreferences(
                SHARE_PREFERENCES_NAME, 0);
        SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
        myPrefsEdit.putInt(TOTAL_PUSH, total);
        myPrefsEdit.commit();
    }

    public static int getTotalPush(Context ctx) {
        SharedPreferences myPrefs = ctx.getSharedPreferences(
                SHARE_PREFERENCES_NAME, 0);
        return myPrefs.getInt(TOTAL_PUSH, 0);
    }

    public static void setUpdateTokenPush(Context ctx, boolean isUpdated) {
        SharedPreferences myPrefs = ctx.getSharedPreferences(
                SHARE_PREFERENCES_NAME, 0);
        SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
        myPrefsEdit.putBoolean(UPDATED_TOKEN_PUSH, isUpdated);
        myPrefsEdit.commit();
    }



	public static void setScreenWidth(Context ctx, int width) {
		SharedPreferences myPrefs = ctx.getSharedPreferences(
				SHARE_PREFERENCES_NAME, 0);
		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
		myPrefsEdit.putInt(SCREEN_WIDTH, width);
		myPrefsEdit.commit();
	}

	public static int getScreenWidth(Context ctx) {
		SharedPreferences myPrefs = ctx.getSharedPreferences(
				SHARE_PREFERENCES_NAME, 0);
		return myPrefs.getInt(SCREEN_WIDTH, 0);
	}





//
//	public static int getUserId(Context ctx) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		return myPrefs.getInt(USER_ID, 0);
//	}
//
//	public static void setUsername(Context ctx, String username) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
//		myPrefsEdit.putString(USERNAME, username);
//		myPrefsEdit.commit();
//	}
//
//	public static void setPassword(Context ctx, String password) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
//		myPrefsEdit.putString(PASSWORD, password);
//		myPrefsEdit.commit();
//	}
//
//	public static void setLongitude(Context ctx, double longitude) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
//		myPrefsEdit.putFloat(LONGITUDE, (float) longitude);
//		myPrefsEdit.commit();
//	}
//
//	public static double getLongitude(Context ctx) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		return myPrefs.getFloat(LONGITUDE, 0);
//	}
//
//	public static void setLatitude(Context ctx, double longitude) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
//		myPrefsEdit.putFloat(LATITUDE, (float) longitude);
//		myPrefsEdit.commit();
//	}
//
//	public static double getLatitude(Context ctx) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		return myPrefs.getFloat(LATITUDE, 0);
//	}
//
//	public static void setSiteNotCheckout(Context ctx, String jsonSite) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
//		myPrefsEdit.putString(SITE_NOT_CHECKOUT, jsonSite);
//		myPrefsEdit.commit();
//	}
//
//	public static String getSiteNotCheckout(Context ctx) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		return myPrefs.getString(SITE_NOT_CHECKOUT, "");
//	}
//
//	public static void setCategory(Context ctx, String jsonCategory) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
//		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
//		myPrefsEdit.putString(CATEGORY, jsonCategory);
//		myPrefsEdit.commit();
//	}
//
//	public static String getCategory(Context ctx) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		return myPrefs.getString(CATEGORY, "");
//	}
//
//	public static void setAllProject(Context ctx, String jsonAllProject) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
//		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
//		myPrefsEdit.putString(ALL_PROJECT, jsonAllProject);
//		myPrefsEdit.commit();
//	}
//
//	public static String getAllProject(Context ctx) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		return myPrefs.getString(ALL_PROJECT, "");
//	}
//
//	public static void setGaugeState(Context ctx, int state) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
//		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
//		myPrefsEdit.putInt(SHOW_GAUSE, state);
//		myPrefsEdit.commit();
//	}
//
//	public static int getGaugeState(Context ctx) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		return myPrefs.getInt(SHOW_GAUSE, -1);
//	}
//
//	public static void setAnswerResult(Context ctx, int result) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
//		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
//		myPrefsEdit.putInt(ANSWER_RESULT, result);
//		myPrefsEdit.commit();
//	}
//
//	public static int getAnswerResult(Context ctx) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		return myPrefs.getInt(ANSWER_RESULT, 0);
//	}
//
//	public static void setSiteJsonTemp(Context ctx, String json) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
//		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
//		myPrefsEdit.putString(SITE_JSON_TEMP, json);
//		myPrefsEdit.commit();
//	}
//
//	public static String getSiteJsonTemp(Context ctx) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		return myPrefs.getString(SITE_JSON_TEMP, null);
//	}
//
//	public static void setCategoryIdTemp(Context ctx, int categoryId) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
//		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
//		myPrefsEdit.putInt(CATEGORY_ID_TEMP, categoryId);
//		myPrefsEdit.commit();
//	}
//
//	public static int getCategoryIdTemp(Context ctx) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		return myPrefs.getInt(CATEGORY_ID_TEMP, 0);
//	}
//
//	public static void setCheckInId(Context ctx, String actionId) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
//		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
//		myPrefsEdit.putString(CHECK_IN_ID, actionId);
//		myPrefsEdit.commit();
//	}
//
//	public static String getCheckInId(Context ctx) {
//		SharedPreferences myPrefs = ctx.getSharedPreferences(
//				SHARE_PREFERENCES_NAME, 0);
//		return myPrefs.getString(CHECK_IN_ID, null);
//	}
//
//	public static void setAccessLevel(Context context, String accessLevel) {
//		SharedPreferences myPrefs = context.getSharedPreferences(SHARE_PREFERENCES_NAME, 0);
//		SharedPreferences.Editor myPrefsEdit = myPrefs.edit();
//		myPrefsEdit.putString(ACCESS_LEVEL, accessLevel);
//		myPrefsEdit.commit();
//	}

}
