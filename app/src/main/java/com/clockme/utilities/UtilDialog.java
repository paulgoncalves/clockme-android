package com.clockme.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.widget.DatePicker;

import com.clockme.R;

import java.io.File;
import java.util.Calendar;

public class UtilDialog {

	private static final String TAG = UtilDialog.class.getSimpleName();
	private static Activity mActivity = null;
	private static File mFileTemp;


	public static void showAlertWith1Button(Context context, String tittle,
			String message, String positiveButtonText,
			DialogInterface.OnClickListener positiveButtonlistener) {
		try {
			AlertDialog.Builder alertbox = new AlertDialog.Builder(
					new ContextThemeWrapper(context, R.style.Theme_DialogCustom));
			alertbox.setTitle(tittle);
			alertbox.setMessage(message);
			alertbox.setCancelable(false);

			alertbox.setOnKeyListener(new OnKeyListener() {

				@Override
				public boolean onKey(DialogInterface dialog, int keyCode,
						KeyEvent event) {
					if (keyCode == KeyEvent.KEYCODE_BACK) {
						dialog.dismiss();
						return true;
					}
					return false;
				}
			});

			alertbox.setPositiveButton(positiveButtonText,
					positiveButtonlistener);
			alertbox.show();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static AlertDialog.Builder showAlertWith2Buttons(Activity act,
			String tittle, String message, String positiveButtonText,
			String negativeButtonText,
			DialogInterface.OnClickListener positiveButtonlistener,
			DialogInterface.OnClickListener negativeButtonlistener) {

		AlertDialog.Builder alertbox = new AlertDialog.Builder(new ContextThemeWrapper(act, R.style.Theme_DialogCustom));
		alertbox.setTitle(tittle);
		alertbox.setMessage(message);
		alertbox.setCancelable(false);

		alertbox.setPositiveButton(positiveButtonText, positiveButtonlistener);
		alertbox.setNegativeButton(negativeButtonText, negativeButtonlistener);
		alertbox.show();
		return alertbox;
	}

	public static AlertDialog.Builder showInputAlertWith2Buttons(Activity act,
															String tittle, String message, String positiveButtonText,
															String negativeButtonText,
															DialogInterface.OnClickListener positiveButtonlistener,
															DialogInterface.OnClickListener negativeButtonlistener) {

		AlertDialog.Builder alertbox = new AlertDialog.Builder(new ContextThemeWrapper(act, R.style.Theme_DialogCustom));
		alertbox.setTitle(tittle);
		alertbox.setMessage(message);
		alertbox.setCancelable(false);

		alertbox.setPositiveButton(positiveButtonText, positiveButtonlistener);
		alertbox.setNegativeButton(negativeButtonText, negativeButtonlistener);
		alertbox.show();
		return alertbox;
	}

	/**
	 * when we call this method need to convert the context to an activity
	 * e.g. from controller UtilDialog.showSelectImageDialog((MainActivity)mContext)
	 *
	 * @param activity reference to the activity that started the dialog so we can
	 *                 use the call back method onActivityResult()
	 *
	 */
	public static void showSelectImageDialog(final Activity activity) {
		// assign activity to local variable so we can access from other method in class
		mActivity = activity;
		AlertDialog.Builder dialog = new AlertDialog.Builder(
				new ContextThemeWrapper(activity, R.style.Theme_DialogCustom));
		dialog.setTitle(mActivity.getResources().getString(R.string.tv_upload_image))
				.setItems(R.array.image_source, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
							case 0:
								takePicture();
								break;
							default:
								openGallery();
								break;
						}
					}
				}).setNegativeButton(activity.getResources().getString(R.string.btn_cancel_title), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});

		AlertDialog alertDialog = dialog.create();
		alertDialog.setCancelable(true);
		alertDialog.show();
		//return dialog;
	}

	/*
	  ￼Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	   intput.putExtra(MediaStore.EXTRA_OUTPUT, storageURI);
	   startActivityForResult(intent, requestCode);

	   - storageURI indicates where to save the photo
	 */
	private static void takePicture() {
		createTempFile();
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		try {
			Uri mImageCaptureUri = null;

			// check for SD card
			String state = Environment.getExternalStorageState();
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				mImageCaptureUri = Uri.fromFile(mFileTemp);
			} else {
				mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
			}

			intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
			intent.putExtra(Constants.KEY_RETURN_PHOTO, true);
			mActivity.startActivityForResult(intent, Constants.REQUEST_CODE_TAKE_PICTURE);

		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
		}
	}

//	/* Check if external storage is available for read and write */
//	public static boolean isExternalStorageWritable() {
//		String state = Environment.getExternalStorageState();
//		if (Environment.MEDIA_MOUNTED.equals(state)) {
//			return true;
//		}
//		return false;
//	}

	private static void openGallery() {
		createTempFile();
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		mActivity.startActivityForResult(photoPickerIntent, Constants.REQUEST_CODE_GALLERY);
	}

	private static void createTempFile() {
		// TODO: 29/09/15 Handle when user select image from external directory
		String state = Environment.getExternalStorageState();
		// check the value of state
		Log.d(TAG, "createTempFie() - state = " + state);

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// this returns /storage/sdcard/temp_photo.jpg
			mFileTemp = new File(Environment.getExternalStorageDirectory(), Constants.TEMP_PHOTO_FILE_NAME);
		} else {
			mFileTemp = new File(mActivity.getFilesDir(), Constants.TEMP_PHOTO_FILE_NAME);
		}

		Log.d(TAG, "check FileTemp value = " + mFileTemp.toString());
	}


    /**
     * This method creates a date picker dialog.
     * Pass the context and a dateListener, when we create
     * the dateListener in this method the date picker dialog
     * only closes when the "Done" button is clicked twice.
     * So we pass the dateListener as a parameter and handle the
     * event in the class that calls this method
     * @param context
     * @param dateListener
     */
    public static void showCalendarDialog(Context context,
                                          DatePickerDialog.OnDateSetListener dateListener) {

		final Calendar calendar = Calendar.getInstance();

        new DatePickerDialog(
				context,
				dateListener,
				calendar.get(Calendar.YEAR),
				calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

	/**
	 * This method creates a date picker dialog.
	 * Pass the context and a dateListener, when we create
	 * the dateListener in this method the date picker dialog
	 * only closes when the "Done" button is clicked twice.
	 * So we pass the dateListener as a parameter and handle the
	 * event in the class that calls this method
	 * @param context
	 * @param dateListener
	 */
	public static void showCalendarDialogWithMinDate(Context context, double minDate, DatePickerDialog.OnDateSetListener dateListener) {

		final Calendar calendar = Calendar.getInstance();

		DatePickerDialog datePickerDialog =  new DatePickerDialog(
				context,
				dateListener,
				calendar.get(Calendar.YEAR),
				calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH));

		DatePicker datePicker =  datePickerDialog.getDatePicker();
		datePicker.setMinDate((long)minDate * 1000);
		datePickerDialog.show();
	}

}
