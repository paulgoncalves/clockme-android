package com.clockme.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateTimeUtils {

	static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";

	public static long getCurrentTimeStamp() {

		Long tsLong = System.currentTimeMillis()/1000;

		SimpleDateFormat df = new SimpleDateFormat(DATE_TIME_FORMAT);
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date d = Calendar.getInstance().getTime();
		String dst = df.format(d);
		try {
			Date utc = df.parse(dst);
			return (utc.getTime() / 1000);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return (d.getTime() / 1000);
	}

	public static long getCurrentDate() {
		Date d = new Date();
		return d.getTime();
	}

	public static String getSimpleDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("d/MM/yyyy");
		return sdf.format(new Date());
	}
	
	public static String convertToDatetime(double timeStamp) {
		SimpleDateFormat objFormatter = new SimpleDateFormat("dd/MM/yy HH:mm", Locale.getDefault());
		objFormatter.setTimeZone(TimeZone.getDefault());

		Calendar objCalendar = Calendar.getInstance(TimeZone.getDefault());
		objCalendar.setTimeInMillis((long)timeStamp * 1000);
		String result = objFormatter.format(objCalendar.getTime());
		objCalendar.clear();
		return result;
	}

	public static String convertToDate(double timeStamp) {
		SimpleDateFormat objFormatter = new SimpleDateFormat("dd/MM/yy", Locale.getDefault());
		objFormatter.setTimeZone(TimeZone.getDefault());

		Calendar objCalendar = Calendar.getInstance(TimeZone.getDefault());
		objCalendar.setTimeInMillis((long)timeStamp * 1000);
		String result = objFormatter.format(objCalendar.getTime());
		objCalendar.clear();
		return result;
	}

	public static String convertToTime(long timeStamp) {
		SimpleDateFormat objFormatter = new SimpleDateFormat("HH:mm", Locale.getDefault());
		objFormatter.setTimeZone(TimeZone.getDefault());

		Calendar objCalendar = Calendar.getInstance(TimeZone.getDefault());
		objCalendar.setTimeInMillis(timeStamp * 1000);
		String result = objFormatter.format(objCalendar.getTime());
		objCalendar.clear();
		return result;
	}

	public static int[] splitToComponentTimes(long longVal)
	{
		int hours = (int) longVal / 3600;
		int remainder = (int) longVal - hours * 3600;
		int mins = remainder / 60;
		remainder = remainder - mins * 60;
		int secs = remainder;

		int[] ints = {hours , mins , secs};
		return ints;
	}


	/**
	 Method to convert in hours the amount of time stamp
	 @return quantity of hours
	 */
	public static String getHoursByTimeStamp(double time){

		int hours = (int)time / Constants.SECONDS_IN_AHOUR;

		if (hours > 0) {

			int mod = ((int)time) % Constants.SECONDS_IN_AHOUR;
			int min = mod / Constants.MINUTES_IN_AHOUR;
			String minutes;

			if (min < 10) {
				minutes = String.format("0%s",min);
			}else{
				minutes = String.format("%s",min);
			}

			if (hours > 1) {
				return String.format("%s:%s hours",hours,min);
			}else{
				return String.format("%s:%s hour",hours,min);
			}

		}else{

			int minutes = (int)time / Constants.MINUTES_IN_AHOUR;

			if (minutes > 0) {
				return String.format("%s minutes",minutes);
			}else{
				return String.format("%s minute",minutes);
			}

		}

	}

	/**
	 Method to compare the given time and current time
	 @param time given time
	 @return true or faulse
	 */
	public static Boolean isTimelessThen(double time , int days){

		//Multiply the quantity of seconds for days and sum to end day
		//86400 = quantity of seconds in a day
		time += days * 86400;

		if (time > getCurrentTimeStamp()) {
			return true;
		}else{
			return false;
		}
	}

	public static String getTotalDay(double timeFrom ,double timeTo){

		double periodSeconds = (timeTo - timeFrom);
		double elapsedDays = periodSeconds / 60 / 60 / 24;
		if (elapsedDays > 1) {
			return String.format("%.1s days",elapsedDays);
		}else{
			return String.format("%.1s day",elapsedDays);
		}

	}

}
