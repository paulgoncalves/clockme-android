package com.clockme.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.clockme.controllers.SiteNotesController;
import com.clockme.controllers.SiteNotesItemController;
import com.clockme.models.NotesModel;

import java.util.ArrayList;

/**
 * Created by AppDev on 25/08/15.
 */
public class SiteNotesListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<NotesModel> mNotesList = new ArrayList<>();

    private SiteNotesController mSiteNotesController;
    private SiteNotesItemController mSiteNotesItemController;

    public SiteNotesListAdapter(Context context, ArrayList<NotesModel> list,
                                SiteNotesController controller) {
        this.mContext = context;
        this.mNotesList = list;
        this.mSiteNotesController = controller;
    }

    @Override
    public int getCount() {
        if (mNotesList != null) {
            return mNotesList.size();
        }
        return 0;
    }

    @Override
    public NotesModel getItem(int position) {
        if (mNotesList != null) {
            return mNotesList.get(position);
        }

        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SiteNoteItemView viewHolder;
        final NotesModel notesModel = getItem(position);
        if (convertView == null) {
            viewHolder = new SiteNoteItemView(mContext);
            convertView = viewHolder;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (SiteNoteItemView) convertView.getTag();
        }


        mSiteNotesItemController = new SiteNotesItemController(mContext, viewHolder, notesModel, mSiteNotesController);
        mSiteNotesItemController.initData();

        return convertView;
    }

    public void setList(ArrayList<NotesModel> notesList) {
        this.mNotesList = notesList;

        notifyDataSetChanged();
    }


}
