package com.clockme.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.clockme.controllers.SiteLogController;
import com.clockme.controllers.SiteLogItemController;

import java.util.ArrayList;

public class SiteLogListAdapter extends BaseAdapter implements Filterable {

    private Context mContext;
    private LogNameFilter mLogNameFilter;

    private SiteLogController mSiteLogController;
    private SiteLogItemController mSiteLogItemController;

    public SiteLogListAdapter(Context context, ArrayList<> list, SiteLogController
            controller) {
        this.mContext = context;
        this.mLogList = list;
        mFilterList = this.mLogList;

        this.mSiteLogController = controller;

    }

    @Override
    public int getCount() {
        if (mLogList != null) {
            return mLogList.size();
        }
        return 0;
    }

    @Override
    public LogModel getItem(int position) {
        return mLogList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SiteLogItemView viewHolder;
        final LogModel logModel = getItem(position);
        if (convertView == null) {
            viewHolder = new SiteLogItemView(mContext);
            convertView = viewHolder;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (SiteLogItemView) convertView.getTag();
        }

        mSiteLogItemController = new SiteLogItemController(mContext, viewHolder, logModel,mSiteLogController);
        mSiteLogItemController.initData();

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (mLogNameFilter == null) {
            mLogNameFilter = new LogNameFilter();
        }
        return mLogNameFilter;
    }

    private class LogNameFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<LogModel> filterList = new ArrayList<>();
                for (int i = 0; i < mFilterList.size(); i++) {
                    if ( (mFilterList.get(i).name.toUpperCase() ).contains(constraint.toString().toUpperCase())) {

                        LogModel logModel = mFilterList.get(i);
                        filterList.add(logModel);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = mFilterList.size();
                results.values = mFilterList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            mLogList = (ArrayList<LogModel>) results.values;
            notifyDataSetChanged();
        }

    }

    public void setList(ArrayList<LogModel> logList) {
        this.mLogList = logList;
        notifyDataSetChanged();
    }

    public ArrayList<LogModel> getLogList() {
        return this.mLogList;
    }

}
