package com.clockme.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.clockme.models.CKPlaceModel;

import java.util.ArrayList;

public class SiteListAdapter extends BaseAdapter {

	private Context ctx;
	private ArrayList<CKPlaceModel> list = new ArrayList<CKPlaceModel>();

	public SiteListAdapter(Context context, ArrayList<CKPlaceModel> list) {
		this.ctx = context;
		this.list = list;
	}

	@Override
	public int getCount() {
		if (list != null) {
			return list.size();
		}
		return 0;
	}

	@Override
	public CKPlaceModel getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		SiteItemView viewHolder;
		final CKPlaceModel entity = getItem(position);
		if (convertView == null) {
			viewHolder = new SiteItemView(ctx);
			convertView = viewHolder;
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (SiteItemView) convertView.getTag();
		}
		viewHolder.tvSiteItem.setText(entity.name);
		return convertView;
	}

}
