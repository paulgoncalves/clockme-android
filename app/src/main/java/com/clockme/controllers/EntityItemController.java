package com.clockme.controllers;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.clockme.R;
import com.clockme.models.CKCompanyModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.Constants;
import com.clockme.utilities.DateTimeUtils;

public class EntityItemController extends AbstractController{

	private Context ctx;
	private EntityItemView mView;
	private CKCompanyModel mEntity;

	public EntityItemController(Context context, EntityItemView view,CKCompanyModel entity) {
		this.ctx = context;
		this.mView = view;
		this.mEntity = entity;
	}

	@Override
	public void initData() {
		super.initData();
		Log.i("Entity Model", "Controller ========");
		Log.i("Entity Model","Name: " + mEntity.companyName);
		Log.i("Entity Model", "Code: " + mEntity.code);
		Log.i("Entity Model", "companyId: " + mEntity.companyId);

		mView.tvEntityName.setText(mEntity.companyName);

		if (mEntity.accessEnd == 0){
			mView.tvEntityName.setTextColor(ctx.getResources().getColor(R.color.blue));
			mView.tvDateTime.setVisibility(View.INVISIBLE);
		}else{

			if (mEntity.active == 1){
				mView.tvEntityName.setTextColor(ctx.getResources().getColor(R.color.light_gray));
				mView.tvDateTime.setVisibility(View.VISIBLE);
				mView.tvDateTime.setText("Expired: "+ DateTimeUtils.convertToDatetime(mEntity.accessEnd));
			}else{
				mView.tvEntityName.setTextColor(ctx.getResources().getColor(R.color.light_gray));
				mView.tvDateTime.setVisibility(View.VISIBLE);
				mView.tvDateTime.setText("Disconnected: "+ DateTimeUtils.convertToDatetime(mEntity.accessEnd));
			}
		}

		//Load entity image
		WebservicesHelper.getInstance().webserviceLoadImage(mView.ivAvatar, Constants.IMG_TYPE_ENTITY, mEntity.companyId);

	}

	@Override
	public void setListener() {
		super.setListener();
		if (mEntity.accessEnd != 0) {
			mView.rlEntityContent.setEnabled(true);
			mView.rlEntityContent.setOnTouchListener(new OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					return true;
				}
			});
		} else {
			mView.rlEntityContent.setEnabled(false);
			mView.rlEntityContent.setOnTouchListener(null);
		}
	}

	@Override
	public View getView() {
		return super.getView();
	}

}
