package com.clockme.controllers;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.interfaces.ILoading;
import com.clockme.interfaces.IWebservice;
import com.clockme.models.CKPersonModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.Constants;
import com.clockme.utilities.DateTimeUtils;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.UtilDialog;
import com.clockme.views.ActionCreateView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by AppDev on 29/09/15.
 */
public class CreateActionController extends AbstractController
        implements View.OnClickListener, View.OnTouchListener {

    private static final String TAG = CreateActionController.class.getSimpleName();
    private Context mContext;
    private ActionCreateView mView;

    private HazardActionsModel mHazardActionModel;
    private HazardModel mHazardModel;
    private EntityUserModel mEntityUserModel;
    private LoadingBeat mLoadingBeat;

    public CreateActionController(Context context, ActionCreateView view) {
        this.mContext = context;
        this.mView = view;
        mLoadingBeat = new LoadingBeat(mContext);
    }

    @Override
    public void initData() {
        super.initData();
        mHazardActionModel = new HazardActionsModel();
        mHazardModel = ((HazardDetailActivity)mContext).getHazardModel();
        mView.tvCompleteActionBy.setText(DateTimeUtils.getSimpleDate());
    }

    @Override
    public void setListener() {
        super.setListener();

        mView.navView.btnLeft.setOnClickListener(this);
        mView.calendarImgBtn.setOnClickListener(this);
        // add button
        mView.addActionBtn.setOnClickListener(this);

        // onTouch listener
        mView.etPersonToComplete.setOnTouchListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v == mView.navView.btnLeft) {
            returnToPreviousScreen();
        } else if(v == mView.addActionBtn) {
            saveModel();
        } else if(v == mView.calendarImgBtn) {
            displayCalendar();
        }

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if(v == mView.etPersonToComplete) {
            ((HazardDetailActivity)mContext).setIsCreateAction(true);
            ((HazardDetailActivity)mContext).swapFragment(Constants.WIZARD_ENTITY_USER_SCREEN,
                    Constants.ANIMATION_VIEW_TYPE_FADE, null);
        }

        return false;
    }

    /**
     * if a bundle has been passed from another fragment we set the text
     * @param bundle
     */
    public void initDataFromBundle(Bundle bundle) {

        mEntityUserModel = (EntityUserModel)bundle.getSerializable(Constants.KEY_SHARE_MODEL);
        if(mEntityUserModel != null) {
            mView.etPersonToComplete.setText(mEntityUserModel.firstName + " "
                    + mEntityUserModel.lastName);
        }

    }

    public void setPersonToComplete() {

        if(((HazardDetailActivity)mContext).getEntityUserModel() != null) {
            mEntityUserModel = ((HazardDetailActivity) mContext).getEntityUserModel();
            mView.etPersonToComplete.setText(mEntityUserModel.firstName + " "
                    + mEntityUserModel.lastName);
        }

    }

    /**
     * return to Hazard Actions View
     * first save new Action to the server, then
     * add the Hazard Action model to list before we start fragment so
     * the list is already updated
     */
    private void returnToPreviousScreen() {

        ((HazardDetailActivity)mContext).setIsCreateAction(false);

        //requestCreateHazardAction();
        ((HazardDetailActivity)mContext).swapFragment(Constants.FRAGMENT_HAZARD_ACTIONS,
                Constants.ANIMATION_VIEW_TYPE_FADE, null);
    }

    private void saveModel() {
        if(mView.etPersonToComplete.getText().toString().equals("") ||
                mView.etActionTitle.getText().toString().equals("") ||
                mView.etDescription.getText().toString().equals("")) {
            Toast.makeText(mContext, "Please enter all details to create action", Toast
                    .LENGTH_SHORT).show();
        } else {

            mHazardActionModel.setUserId(mEntityUserModel.userId);
            mHazardActionModel.setUserResponsible(mEntityUserModel.firstName + " " +
                    mEntityUserModel.lastName);
            mHazardActionModel.setEntityId(SBApplication.getEntityModel().companyId);

            // get data from edit text
            mHazardActionModel.setActionName(mView.etActionTitle.getText().toString());
            mHazardActionModel.setCompleteById(mEntityUserModel.userId); // person id
            //mHazardActionModel.setCompleteByDate(mView.tvCompleteActionBy.getText().toString());



            mHazardActionModel.setDescription(mView.etDescription.getText().toString());

            // set boolean value to false to indicate we have finished
            // creating a new action
            ((HazardDetailActivity)mContext).setIsCreateAction(false);
            ((HazardDetailActivity)mContext).addActionToList(mHazardActionModel);

            requestCreateHazardAction();

            returnToPreviousScreen();
        }
    }

    private void displayCalendar() {

        final Calendar calendar = Calendar.getInstance();
        UtilDialog.showCalendarDialog(mContext, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                //Save timestamp to complete hazard action
                mHazardActionModel.setCompleteByDate(calendar.getTimeInMillis() * 1000);

                int month = monthOfYear + 1;

                mView.tvCompleteActionBy.setText(
                        String.valueOf(dayOfMonth) + "/" +
                                String.valueOf(month) + "/" +
                                String.valueOf(year));
            }
        });

    }

    /****************** WEB REQUEST *******************************/
    private void requestCreateHazardAction() {
        JSONObject obj = new JSONObject();

        try {
            obj.put(Constants.KEY_PERSON_ID, SBApplication.getUserModel().personId);
            obj.put(Constants.KEY_TOKEN, SBApplication.getUserModel().token);
            obj.put(Constants.KEY_HAZARD_ID, mHazardModel.getHazardId());
            obj.put(Constants.KEY_SITE_NAME, mHazardModel.getSiteName());
            obj.put(Constants.KEY_ACTION, createActionArray());

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        WebservicesHelper.getInstance().webserviceRequestWithJson(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_HAZARD_ACTION_CREATE, obj.toString());
    }

    private JSONArray createActionArray() {
        JSONArray actionArrayJson = new JSONArray();
        long ts = DateTimeUtils.getCurrentTimeStamp();

        JSONObject obj = new JSONObject();
        try {
            obj.put(Constants.KEY_HAZARD_ACTION_NAME, mHazardActionModel.getActionName());
            obj.put(Constants.KEY_HAZARD_ACTION_DESC, mHazardActionModel.getDescription());
            obj.put(Constants.KEY_ENTITY_ID, mHazardActionModel.getEntityId());
            obj.put(Constants.KEY_TIME, ts);
            obj.put(Constants.KEY_TIME_ZONE, SBApplication.getUserModel().timezone);
            obj.put(Constants.KEY_COMPLETE_BY, mHazardActionModel.getCompleteById());
            obj.put(Constants.KEY_COMPLETE_BY_TIME, mHazardActionModel.getCompleteByDate());

            actionArrayJson.put(obj);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return actionArrayJson;

    }

    /********************** Web service call back interface ***********************/
    IWebservice IWebserviceRequest = new IWebservice() {

        @Override
        public void onComplete(JSONObject WSResponse, String WSMessage, int WSCode) {

            if (WSResponse.optInt("status") == 1){
                /** Handle response from IWebservice when save hazard action success **/
                if (WSCode == WebservicesHelper.WSCODE_HAZARD_ACTION_SAVE_SUCCESS) {
                    Log.i(TAG, "IWebserviceRequest - create hazard action save success");

                    mLoadingBeat.cancelLoading();

                    // true = has click listener
                    displayWebRequestDialog(
                            WSMessage,
                            mContext.getResources().getString(R.string.btn_continue),
                            true);

                }

            } else {
                Log.i(TAG, "IWebserviceRequest - request error");
                displayWebRequestDialog(
                        WSMessage,
                        mContext.getResources().getString(R.string.btn_ok_title),
                        false);

            }


            Log.i(TAG, "IWebserviceRequest - request complete");

        }

        /**
         * Handle IWebservice error
         * @param error response from server
         */
        @Override
        public void onError(String error) {
            mLoadingBeat.cancelLoading();

            displayWebRequestDialog(
                    error,
                    mContext.getResources().getString(R.string.btn_ok_title),
                    false);

        }

        @Override
        public void onAccessDenied(String WSMessage) {
//            mLoadingBeat.cancelLoading();
            UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
            CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
        }
    };

    ILoading iLoading = new ILoading() {

        @Override
        public void showLoading() {
            mLoadingBeat.showLoading();
        }
    };

    /**
     * this method displays a dialog with or without a click listener
     * @param webResponse       string from server
     * @param btnText           text for button
     * @param hasClickListener  value to check if we use a click listener
     */
    private void displayWebRequestDialog(
            String webResponse, String btnText, boolean hasClickListener) {

        if(hasClickListener) {
            // set value to true so when we return to Create hazard view
            // to add another hazard the edit text are cleared
            //((WizardActivity)mContext)
            /** Display dialog that will handle click event */
            UtilDialog.showAlertWith1Button(
                    mContext,
                    "",
                    webResponse,
                    btnText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            // use callback interface to set slider position
                            // back to start
                            //OnSaveHazardListener listener = (OnSaveHazardListener)mContext;
                            //listener.onSaveHazardListener();

                            //displayHazardActivity();

                        }
                    });
        } else {
            /** display dialog with null click listener */
            UtilDialog.showAlertWith1Button(
                    mContext,
                    "",
                    webResponse,
                    btnText,
                    null);
        }

    }


}
