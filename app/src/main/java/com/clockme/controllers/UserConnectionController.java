package com.clockme.controllers;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;

import com.clockme.R;
import com.clockme.utilities.Constants;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.Utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tannguyen on 5/21/15.
 */
public class UserConnectionController extends AbstractController implements View.OnClickListener {

    public Context mContext;
    public ConnectionView mView;
    public LoadingBeat loadingBeat;
    public EntityUserModel entityUserModel;
    public static final String KEY = "key";
    private String[] mGroupStrings = {Constants.PENDING_GROUP , Constants.I_REPORT_TO, Constants.REPORT_TO_ME, Constants.OTHER_CONNECTION };
    public List<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
    public List<List<Map<String, ConnectionModel>>> childData = new ArrayList<>();

    public UserConnectionController(Context context, ConnectionView view) {
        this.mContext = context;
        this.mView = view;
        loadingBeat = new LoadingBeat(mContext);
    }

    public void prepareData() {

        ArrayList<ConnectionModel> pendingArray = UserConnectionModel.getPendingArray();
        ArrayList<ConnectionModel> leaderArray = UserConnectionModel.getLeaderArray();
        ArrayList<ConnectionModel> memberArray = UserConnectionModel.getMemberArray();
        ArrayList<ConnectionModel> followerArray = UserConnectionModel.getFollowerArray();

        groupData.clear();
        childData.clear();

        for (String group : mGroupStrings) {
            if (group.equals(Constants.PENDING_GROUP) && pendingArray.size() > 0){
                Map<String, String> groupMap1 = new HashMap<String, String>();
                groupData.add(groupMap1);
                groupMap1.put(KEY, group);

                List<Map<String, ConnectionModel>> childList = new ArrayList<>();
                for (ConnectionModel obj : pendingArray) {
                    Map<String, ConnectionModel> childMap = new HashMap<>();
                    childList.add(childMap);
                    childMap.put(KEY, obj);


                }
                childData.add(childList);

            }

            if (group.equals(Constants.I_REPORT_TO) && leaderArray.size() > 0){
                Map<String, String> groupMap1 = new HashMap<String, String>();
                groupData.add(groupMap1);
                groupMap1.put(KEY, group);

                List<Map<String, ConnectionModel>> childList = new ArrayList<>();
                for (ConnectionModel obj : leaderArray) {
                    Map<String, ConnectionModel> childMap = new HashMap<>();
                    childList.add(childMap);
                    childMap.put(KEY, obj);


                }
                childData.add(childList);

            }

            if (group.equals(Constants.REPORT_TO_ME) && followerArray.size() > 0){
                Map<String, String> groupMap1 = new HashMap<String, String>();
                groupData.add(groupMap1);
                groupMap1.put(KEY, group);

                List<Map<String, ConnectionModel>> childList = new ArrayList<>();
                for (ConnectionModel obj : followerArray) {
                    Map<String, ConnectionModel> childMap = new HashMap<>();
                    childList.add(childMap);
                    childMap.put(KEY, obj);


                }
                childData.add(childList);

            }

            if (group.equals(Constants.OTHER_CONNECTION) && memberArray.size() > 0){
                Map<String, String> groupMap1 = new HashMap<String, String>();
                groupData.add(groupMap1);
                groupMap1.put(KEY, group);

                List<Map<String, ConnectionModel>> childList = new ArrayList<>();
                for (ConnectionModel obj : memberArray) {
                    Map<String, ConnectionModel> childMap = new HashMap<>();
                    childList.add(childMap);
                    childMap.put(KEY, obj);


                }
                childData.add(childList);

            }

        }
    }

    @Override
    public void initData() {
        super.initData();

        // Adjust layout

        mView.navView.tvTitle.setText(entityUserModel.firstName + " Connection");
        mView.navView.btnRight.setVisibility(View.INVISIBLE);
        mView.navView.btnRight.setClickable(true);

        mView.navView.btnLeft.setVisibility(View.VISIBLE);
        mView.navView.btnLeft.setText(mContext.getResources().getString(
                R.string.btn_back_title));
        mView.navView.btnLeft.setCompoundDrawablesWithIntrinsicBounds(
                R.drawable.selector_btn_back, 0, 0, 0);
        mView.ivConnectionBg.setVisibility(View.INVISIBLE);


        new CountDownTimer(500, 100) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                //loadUserConnectionData(false);
            }
        }.start();

    }

    public void initDataFromBundle(Bundle bundle){

        entityUserModel = (EntityUserModel) bundle.getSerializable(Constants.KEY_CONNECTION);

    }

//    @Override
//    public void loadUserConnectionData(final boolean isReload) {
//
//        UserConnectionModel.loadUserConnection(mContext, new IGetUserConnection() {
//            @Override
//            public void onComplete(ArrayList<UserConnectionObject> arrLeader,
//                                   ArrayList<UserConnectionObject> arrMember,
//                                   ArrayList<UserConnectionObject> arrPending,
//                                   ArrayList<UserConnectionObject> arrFollower) {
//
//                loadingBeat.cancelLoading();
//
//                UserConnectionModel.clearConnectionArray();
//
//                UserConnectionModel.setLeaderArray(arrLeader);
//                UserConnectionModel.setMemberArray(arrMember);
//                UserConnectionModel.setPendingArray(arrPending);
//                UserConnectionModel.setPendingArray(arrFollower);
//
//                Toast.makeText(mContext, "Loaded data successfully", Toast.LENGTH_SHORT).show();
//
//                prepareData();
//
//                if(isReload){
//                    mAdapter.notifyDataSetChanged();
//                }
//                else{
//                    mAdapter = new UserConnectionListAdapter(mContext, groupData, childData, UserConnectionController.this);
//
//                    mView.lvConnection.setAdapter(mAdapter);
//
//
//                }
//
//                // Set expanded is default
//                for(int i=0; i < mAdapter.getGroupCount(); i++) {
//                    mView.lvConnection.expandGroup(i);
//                }
//
////                if(arrLeader.size() == 0
////                        && arrMember.size() == 0
////                        && arrPending.size() == 0){
////                    mView.ivConnectionBg.setVisibility(View.VISIBLE);
////                }
////                else{
////                    mView.ivConnectionBg.setVisibility(View.INVISIBLE);
////                }
//
//                //mView.lvConnection.onRefreshComplete();
//
//            }
//
//            @Override
//            public void onError(String error) {
//                loadingBeat.cancelLoading();
//
//                UserConnectionModel.clearConnectionArray();
//                prepareData();
//
////                mView.ivConnectionBg.setVisibility(View.VISIBLE);
//                mAdapter.notifyDataSetChanged();
//
//                //mView.lvConnection.onRefreshComplete();
//                UtilDialog.showAlertWith1Button(
//                        mContext,
//                        null,
//                        error,
//                        mContext.getResources().getString(
//                                R.string.btn_cancel_title), null);
//
//
//
//            }
//        }, new ILoading() {
//            @Override
//            public void showLoading() {
//                loadingBeat.showLoading();
//            }
//
////            @Override
////            public void hideLoading() {
////                loadingBeat.cancelLoading();
////            }
//        }, String.valueOf(UsersListHelper.getInstance().getUser().personId));
//    }

    @Override
    public void setListener() {
        super.setListener();

        mView.navView.btnLeft.setOnClickListener(this);

//        mView.lvConnection.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                loadUserConnectionData(true);
//            }
//        });
    }

    @Override
    public View getView() {
        return mView;
    }

    @Override
    public void onClick(View v) {

        if (v == mView.navView.btnLeft) {
            backToHome();
        }
    }

    private void backToHome() {
        Utilities.hideKeyboard(mContext, mView);
//        new CountDownTimer(300, 100) {
//
//            @Override
//            public void onTick(long millisUntilFinished) {
//                // TODO Auto-generated method stub
//
//            }
//
//            @Override
//            public void onFinish() {
//                ((MainActivity) mContext).popFragment();
//                if (MainActivity.iRefresh != null) {
//                    MainActivity.iRefresh.onRefresh();
//                }
//            }
//        }.start();
    }

}




