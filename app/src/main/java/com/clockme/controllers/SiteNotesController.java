package com.clockme.controllers;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.activities.MainActivity;
import com.clockme.adapters.SiteNotesListAdapter;
import com.clockme.interfaces.ILoading;
import com.clockme.interfaces.IWebservice;
import com.clockme.models.CKPersonModel;
import com.clockme.models.NotesModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.DateTimeUtils;
import com.clockme.utilities.Utilities;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by AppDev on 25/08/15.
 */
public class SiteNotesController extends AbstractController implements View.OnClickListener,
        AdapterView.OnItemClickListener {

    private static final String TAG = SiteNotesController.class.getSimpleName();
    private ArrayList<NotesModel> mNotesList;
    private Context mContext;
    private SiteNotesView mView;
    private SiteNotesListAdapter mNotesListAdapter;

    private String mDateTime;

    public SiteNotesController(Context context, SiteNotesView view) {
        this.mContext = context;
        this.mView = view;
        this.mNotesList = new ArrayList<>();
        this.mDateTime = String.valueOf(DateTimeUtils.getCurrentTimeStamp());
    }

    @Override
    public void initData() {
        super.initData();

        initNotesListAdapter();

        requestSiteNote();
    }

    private void requestSiteNote() {

        String body = String.format(WebservicesHelper.PARAMS_SITE_NOTE, SBApplication.getUserModel().personId, SBApplication.getUserModel().token,SBApplication.getSiteModel().siteId);
        WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_SITE_NOTE, body);

    }

    IWebservice IWebserviceRequest = new IWebservice() {

        @Override
        public void onComplete(JSONObject WSResponse, String WSMessage, int WSCode) {

            if (WSResponse.optInt("status") == 1) {

                if (WSCode == WebservicesHelper.WSCODE_SITE_NOTE_SUCCESS) {

                    // TODO: 12/10/15 Implement fetch notes and populate the listView

                }
            }
        }

        @Override
        public void onError(String error) {
        }

        @Override
        public void onAccessDenied(String WSMessage) {
            CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
        }
    };

    ILoading iLoading = new ILoading() {

        @Override
        public void showLoading() {
        }
    };

    @Override
    public void setListener() {
        mView.navView.btnLeftText.setOnClickListener(this);
        mView.navView.btnRight.setOnClickListener(this);
        mView.notesListView.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == mView.navView.btnLeftText) {
            ((MainActivity) mContext).popFragment();
        } else if(v == mView.navView.btnRight) {
            addSiteNoteDialog();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        editSiteNoteDialog(position);
    }

    private void initNotesListAdapter() {
        mNotesListAdapter = new SiteNotesListAdapter(mContext, mNotesList, this);
        mView.notesListView.setAdapter(mNotesListAdapter);
    }

    /**
     * Pass the updated list to the adapter
     * @param list
     */
    private void updateNoteList(ArrayList<NotesModel> list) {
        if(list != null) {
            mNotesListAdapter.setList(list);
        }
    }

    /**
     * When Add button is clicked the dialog displays
     * the date & time at the top of the edit text
     */
    private void addSiteNoteDialog() {
        final Dialog siteNoteDialog = new Dialog(mContext);
        siteNoteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //siteNoteDialog.getWindow().getAttributes().width = WindowManager.LayoutParams
        // .MATCH_PARENT;
        siteNoteDialog.setContentView(R.layout.dialog_add_note);

        final EditText notesEditText = (EditText)siteNoteDialog.findViewById(R.id.addNoteEditText);
        notesEditText.setText(mDateTime + "\n");

        Button cancelButton = (Button)siteNoteDialog.findViewById(R.id.cancelButton);
        Button saveButton = (Button)siteNoteDialog.findViewById(R.id.saveButton);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                siteNoteDialog.dismiss();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotesModel note = new NotesModel();

                String[] parts = notesEditText.getText().toString().split("\\n");

                note.note = parts[1];
                note.time = Double.parseDouble(parts[0]);

                mNotesList.add(note);

                updateNoteList(mNotesList);

                Utilities.hideKeyboard(mContext, notesEditText);
                siteNoteDialog.dismiss();
            }
        });

        /**
         * Dialog show listener is fired when the dialog first starts
         */
        siteNoteDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Utilities.showKeyboard(mContext, notesEditText);
            }
        });

        siteNoteDialog.show();
    }

    /**
     * When this loads the dialog displays the note at the
     * top and the date below the note
     */
    private void editSiteNoteDialog(final int pos) {
        final Dialog siteNoteDialog = new Dialog(mContext);
        siteNoteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        siteNoteDialog.setContentView(R.layout.dialog_add_note);

        final EditText notesEditText = (EditText)siteNoteDialog.findViewById(R.id.addNoteEditText);

        NotesModel model = mNotesList.get(pos - 1);

        notesEditText.setText(
                model.note + "\n" + model.time + "\n\n" +
                        mDateTime + "\n");

        Button cancelButton = (Button)siteNoteDialog.findViewById(R.id.cancelButton);
        Button saveButton = (Button)siteNoteDialog.findViewById(R.id.saveButton);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utilities.hideKeyboard(mContext, notesEditText);
                siteNoteDialog.dismiss();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotesModel note = mNotesList.get(pos - 1);

                String[] parts = notesEditText.getText().toString().split("\\n");

                String noteStr = "";
                String dateTimeStr = "";

                for(int i = 0; i < parts.length; i++) {
                    if((i % 2) == 0) {
                        // number is even
                        noteStr += parts[i];
                    }
                    /*
                    else {
                        // number is odd
                        dateTimeStr += parts[i];
                    }
                    */
                }

                note.note = noteStr;
                note.time = Double.parseDouble(mDateTime);

                //mNotesList.add(note);
                //updateNoteList(mNotesList);

                Utilities.hideKeyboard(mContext, notesEditText);
                siteNoteDialog.dismiss();
            }
        });

        /**
         * Dialog show listener is fired when the dialog first starts
         */
        siteNoteDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Utilities.showKeyboard(mContext, notesEditText);
            }
        });

        siteNoteDialog.show();
    }

}
