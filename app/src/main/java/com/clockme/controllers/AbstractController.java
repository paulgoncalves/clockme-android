package com.clockme.controllers;

import android.view.View;

import com.clockme.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public abstract class AbstractController {

	public void initData() {
		//
	}

	public void setListener() {
		//
	}

	public View getView() {
		return null;
	}

	public void destroy() {
		//
	}

	public Marker drawMarker(GoogleMap googleMap, LatLng latLng) {
		try {
			return drawMarker(googleMap, latLng, R.mipmap.pin_black);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	private Marker drawMarker(GoogleMap googleMap, LatLng latLng, int marker) {
		// googleMap.clear();
		Marker myMarker = googleMap
				.addMarker(new com.google.android.gms.maps.model.MarkerOptions()
						.position(latLng)
						.icon(BitmapDescriptorFactory.fromResource(marker))
						.draggable(false));
		googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,
				googleMap.getCameraPosition().zoom));
		return myMarker;

	}
}
