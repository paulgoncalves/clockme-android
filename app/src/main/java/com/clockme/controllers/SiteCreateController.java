package com.clockme.controllers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.NumberPicker;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.activities.MainActivity;
import com.clockme.interfaces.IGetAddress;
import com.clockme.interfaces.ILoading;
import com.clockme.interfaces.IWebservice;
import com.clockme.models.AddressModel;
import com.clockme.models.CKPersonModel;
import com.clockme.models.CKPlaceModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.Constants;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.UtilDialog;
import com.clockme.utilities.Utilities;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class SiteCreateController extends AbstractController implements OnClickListener, OnMapReadyCallback{

	private Context mContext;
	private SiteCreateView mView;
	private LoadingBeat loadingBeat;
	private int MAP_ZOOM = 16;
	private int startX = 0, currentX = 0, prevX = 0;
	private int startY = 0, currentY = 0, prevY = 0;
	private int deltaDistance = 0;
	private double RADIUS = 50;
	private LatLng pointStart, pointEnd;
	private AddressModel mAddress;
	private CKPlaceModel mCKPlaceModel;

	final ArrayList<ProjectModel> projectStatusList = new ArrayList<>();

	public SiteCreateController(Context context, SiteCreateView view) {
		this.mContext = context;
		this.mView = view;
	}

	@Override
	public void initData() {
		super.initData();
		loadingBeat = new LoadingBeat(mContext);

		enableEdit(false);// disable PNumber Editable before loading

        //Build site map
        mView.prepareMap(SiteCreateController.this);

		//Check if editing site or creating a new one
		if (mCKPlaceModel.siteId == 0){

			//Set blackspot and safezone defaul values
			//mView.btnNoBlackpot.performClick();
			mCKPlaceModel.blackspot = false;
			mView.btnNoSafeZone.performClick();

			mView.btnDelete.setVisibility(View.INVISIBLE);

		}else{
			setDataToView(mCKPlaceModel);
			mView.btnDelete.setVisibility(View.VISIBLE);
		}

		//Show or hide the project status
		if (AddonModel.getInstance().entityPermission(Constants.ADDON_TYPE_FUEL_GAUGE)){
			mView.llPNumber.setVisibility(View.VISIBLE);
		}else{
			mView.llPNumber.setVisibility(View.GONE);
		}

	}

	public void initDataFromBundle(Bundle bundle){

		if (bundle != null){
			this.mCKPlaceModel = (CKPlaceModel) bundle.getSerializable(Constants.KEY_SITE_ID);;
		}
	}

	public void loadDataWhenMapReady() {

		if (mCKPlaceModel.siteId == 0){
			//Fetch address using the lat and lng
			requestAddress();
		}

        //Perform click to start editing the site info
        mView.navView.btnRight.performClick();

		enableEdit(true);// disable PNumber Editable before loading

		//Add pin to map
		setPinToMap(mCKPlaceModel.siteLocation);

	}

	private void setPinToMap(LatLng pos) {
		if (mView.googleMap != null) {
			mView.googleMap
					.addMarker(new com.google.android.gms.maps.model.MarkerOptions()
							.position(pos).icon(BitmapDescriptorFactory.fromResource(R.mipmap.pin_site_new)));
			mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos,MAP_ZOOM));
		}
	}

	public void requestCreateSite() {

		String name = Utilities.encodeString(mView.etSiteName.getText().toString().trim());
		String stNumber = Utilities.encodeString(mView.etStNumber.getText().toString().trim());
		String stName = Utilities.encodeString(mView.etStName.getText().toString().trim());
		String suburd = Utilities.encodeString(mView.etStSuburb.getText().toString().trim());
		String state = Utilities.encodeString(mView.etState.getText().toString().trim());

		try {
			name = URLEncoder.encode(name, "UTF-8");
			stName = URLEncoder.encode(stName, "UTF-8");
			suburd = URLEncoder.encode(suburd, "UTF-8");
			state = URLEncoder.encode(state, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		if (TextUtils.isEmpty(name) || TextUtils.isEmpty(stName)
				|| TextUtils.isEmpty(stNumber) || TextUtils.isEmpty(suburd)
				|| TextUtils.isEmpty(state)) {
			DialogInterface.OnClickListener okClick = new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			};
			UtilDialog.showAlertWith1Button(mContext, null,
					mContext.getResources().getString(R.string.msg_all_fields), mContext.getResources()
							.getString(R.string.ok), okClick);
			return;
		}

		String strSafezone = Utilities.getBooleanString(mCKPlaceModel.safezone);
		String strBlackspot = Utilities.getBooleanString(mCKPlaceModel.blackspot);
		//personId=%s&token=%s&companyId=%s&siteName=%s&lat=%s&lng=%s&radius=%s&stNumber=%s&stName=%s&suburb=%s&state=%s&safeZone=%s&blackspot=%s&postcode=%s&country=%s&projectId=%s
		String body = String.format(WebservicesHelper.PARAMS_SITE_CREATE, SBApplication.getUserModel().personId,SBApplication.getUserModel().token,
				SBApplication.getEntityModel().companyId, name, mCKPlaceModel.siteLocation.latitude, mCKPlaceModel.siteLocation.longitude, RADIUS,
				stNumber,stName, suburd, state,strSafezone, strBlackspot,mAddress.postal_code,mAddress.country, mCKPlaceModel.projectId);

		WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_SITE_CREATE, body);

	}

	public void requestUpdateSite() {

		if (mCKPlaceModel.siteId > 0) {
			String name = Utilities.encodeString(mView.etSiteName.getText().toString().trim());
			String stNumber = Utilities.encodeString(mView.etStNumber.getText().toString().trim());
			String stName = Utilities.encodeString(mView.etStName.getText().toString().trim());
			String suburd = Utilities.encodeString(mView.etStSuburb.getText().toString().trim());
			String state = Utilities.encodeString(mView.etState.getText().toString().trim());

			if (TextUtils.isEmpty(name) || TextUtils.isEmpty(stName)
					|| TextUtils.isEmpty(stNumber) || TextUtils.isEmpty(suburd)
					|| TextUtils.isEmpty(state)) {
				DialogInterface.OnClickListener okClick = new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				};
				UtilDialog.showAlertWith1Button(mContext, "",
						"All fields must be filled", mContext.getResources()
								.getString(R.string.ok), okClick);
				return;
			}

			String body = String.format(WebservicesHelper.PARAMS_SITE_UPDATE, SBApplication.getUserModel().personId, SBApplication.getUserModel().token,
					mCKPlaceModel.siteId,name, mCKPlaceModel.projectId, mCKPlaceModel.siteLocation.latitude, mCKPlaceModel.siteLocation.longitude,RADIUS,
					stNumber, stName, suburd, state, Utilities.getBooleanString(mCKPlaceModel.safezone),Utilities.getBooleanString(mCKPlaceModel.blackspot));

			WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_SITE_UPDATE, body);

		}
	}

	public void requestDeleteSite() {

		String body = String.format(WebservicesHelper.PARAMS_SITE_DELETE, SBApplication.getUserModel().personId, SBApplication.getUserModel().token, mCKPlaceModel.siteId);
		WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_SITE_DELETE, body);


	}

	public void requestSiteProject() {

		String body = String.format(WebservicesHelper.PARAMS_SITE_PROJECT, SBApplication.getUserModel().personId, SBApplication.getUserModel().token, mCKPlaceModel.siteId);
		WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_SITE_PROJECT, body);


	}

	public void requestAddress() {

		WebservicesHelper.getInstance().webserviceGetAddress(mContext,iGetAddress, mCKPlaceModel.siteLocation.latitude, mCKPlaceModel.siteLocation.longitude);

	}


	IWebservice IWebserviceRequest = new IWebservice() {

		@Override
		public void onComplete(JSONObject WSResponse, String WSMessage, int WSCode) {

			if (WSResponse.optInt("status") == 1) {

				if (WSCode == WebservicesHelper.WSCODE_SITE_CREATE_SUCCESS) {

					DialogInterface.OnClickListener okClick = new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							backToHome();
						}
					};
					UtilDialog.showAlertWith1Button(mContext, WSMessage, WSMessage,mContext.getResources().getString(R.string.ok),okClick);


				}if (WSCode == WebservicesHelper.WSCODE_SITE_DELETE_SUCCESS) {

					DialogInterface.OnClickListener okClick = new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							backToHome();
						}
					};
					UtilDialog.showAlertWith1Button(mContext, WSMessage, WSMessage,mContext.getResources().getString(R.string.ok),okClick);


				}if (WSCode == WebservicesHelper.WSCODE_SITE_UPDATE_SUCCESS) {

					DialogInterface.OnClickListener okClick = new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							backToHome();
						}
					};

					UtilDialog.showAlertWith1Button(mContext, WSMessage, WSMessage,mContext.getResources().getString(R.string.ok),okClick);

				}if (WSCode == WebservicesHelper.WSCODE_SITE_PROJECT_FETCH_SUCCESS) {

					try {

						JSONArray arrayList = WSResponse.getJSONArray("projectStatus");
						for (int i = 0; i < arrayList.length(); i++) {
							projectStatusList.add(new ProjectModel().getProjectModelFromJson(arrayList.getJSONObject(i)));
						}

					}catch (JSONException ex){
						ex.printStackTrace();
					}

					showProjectList();

				}
			}else{
				UtilDialog.showAlertWith1Button(mContext, WSMessage, WSMessage,mContext.getResources().getString(R.string.ok),null);
			}

			loadingBeat.cancelLoading();

		}

		@Override
		public void onError(String error) {
			loadingBeat.cancelLoading();
			UtilDialog.showAlertWith1Button(mContext, "", error , mContext.getResources().getString(R.string.ok), null);
			Log.e("WS error =>", error);
		}

		@Override
		public void onAccessDenied(String WSMessage) {
			loadingBeat.cancelLoading();
			UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
			CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
		}
	};

	ILoading iLoading = new ILoading() {

		@Override
		public void showLoading() {
			loadingBeat.showLoading();
		}

	};

	IGetAddress iGetAddress = new IGetAddress() {

		@Override
		public void onError(String error) {
			mView.etStName.setText("");
			mView.etStNumber.setText("");
			mView.etStSuburb.setText("");
			mView.etState.setText("");
		}

		@Override
		public void onComplete(JSONArray WSResponse) {
			mAddress = new AddressModel().getAddressModelFromJson(WSResponse);
			mView.etStNumber.setText(mAddress.street_number);
			mView.etStName.setText(mAddress.route);
			mView.etStSuburb.setText(mAddress.locality);
			mView.etState.setText(mAddress.administrative_area_level_1);
		}
	};





    @Override
    public void setListener() {
        mView.rlRootView.setOnClickListener(this);
        mView.scrollView.setOnClickListener(this);
        mView.navView.btnLeft.setOnClickListener(this);
        mView.btnSave.setOnClickListener(this);
        mView.btnDelete.setOnClickListener(this);
        mView.navView.btnRight.setOnClickListener(this);
        mView.navView.btnEditSave.setOnClickListener(this);
        mView.btnNoSafeZone.setOnClickListener(this);
        mView.btnYesSafeZone.setOnClickListener(this);
        mView.ivDot.setOnTouchListener(changRadiusListener);
        mView.btnPNumber.setOnClickListener(this);

        //mView.btnNoBlackpot.setOnClickListener(this);
        //mView.btnYesBlackpot.setOnClickListener(this);
    }

    @Override
    public void onClick (View v){
        if (v == mView.rlRootView || v == mView.scrollView) {
            Utilities.hideKeyboard(mContext, mView);
        } else if (v == mView.navView.btnLeft) {
            mView.googleMap = null;
            backToHome();
        } else if (v == mView.btnSave) {

            Utilities.hideKeyboard(mContext, mView.btnSave);
            if (mCKPlaceModel.siteId == 0){
                requestCreateSite();
            }else{
                requestUpdateSite();
            }

        } else if (v == mView.navView.btnEditSave) {
            Utilities.hideKeyboard(mContext, mView.navView.btnEditSave);
            requestUpdateSite();
        } else if (v == mView.navView.btnRight) {
            mView.navView.btnRight.setVisibility(View.GONE);
            mView.navView.btnEditSave.setVisibility(View.VISIBLE);
            enableEdit(true);
            mView.btnDelete.setVisibility(View.VISIBLE);
        } else if (v == mView.btnDelete) {
            Utilities.hideKeyboard(mContext, mView.btnDelete);
            requestDeleteSite();
        } else if (v == mView.btnYesSafeZone) {
            mCKPlaceModel.safezone = true;
            mView.btnYesSafeZone.setBackgroundResource(R.drawable.border_right);
            mView.btnNoSafeZone.setBackgroundResource(R.drawable.border_left_selected);
            mView.btnYesSafeZone.setSelected(true);
        } else if (v == mView.btnPNumber) {
            //Fetch project by entity
            requestSiteProject();
        }else if (v == mView.btnNoSafeZone) {
            mCKPlaceModel.safezone = false;
            mView.btnYesSafeZone.setBackgroundResource(R.drawable.border_right_selected);
            mView.btnNoSafeZone.setBackgroundResource(R.drawable.border_left);
            mView.btnNoSafeZone.setSelected(true);
        }

//		else if (v == mView.btnNoBlackpot) {
//			mCKPlaceModel.blackspot = false;
//			mView.btnYesBlackpot.setBackgroundResource(R.drawable.border_right_selected);
//			mView.btnNoBlackpot.setBackgroundResource(R.drawable.border_left);
//			mView.btnNoBlackpot.setSelected(true);
//		}  else if (v == mView.btnYesBlackpot) {
//			mCKPlaceModel.blackspot = true;
//			mView.btnYesBlackpot.setBackgroundResource(R.drawable.border_right);
//			mView.btnNoBlackpot.setBackgroundResource(R.drawable.border_left_selected);
//			mView.btnYesBlackpot.setSelected(true);
//		}
    }

    private OnTouchListener changRadiusListener = new OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    startX = (int) event.getX();
                    startY = (int) event.getY();
                    pointStart = mView.googleMap.getProjection()
                            .fromScreenLocation(new Point(startX, startY));
                }
                break;
                case MotionEvent.ACTION_MOVE: {
                    mView.ivRadius.setVisibility(View.INVISIBLE);
                    currentX = (int) event.getX();
                    currentY = (int) event.getY();

                    if (prevX == 0 || prevY == 0) {
                        prevX = currentX;
                        prevY = currentY;
                        pointEnd = mView.googleMap.getProjection()
                                .fromScreenLocation(new Point(currentX, currentY));
                        deltaDistance = (int) getDistance(pointStart, pointEnd);
                        if (currentX > startX) {
                            RADIUS += deltaDistance;
                        } else {
                            RADIUS -= deltaDistance;
                            if (RADIUS <= 50) {
                                RADIUS = 50;
                            }
                        }
                    } else {
                        pointStart = mView.googleMap.getProjection()
                                .fromScreenLocation(new Point(prevX, prevY));
                        pointEnd = mView.googleMap.getProjection()
                                .fromScreenLocation(new Point(currentX, currentY));
                        deltaDistance = (int) getDistance(pointStart, pointEnd);
                        if (currentX > prevX && prevX > startX) {
                            RADIUS += deltaDistance;
                        } else {
                            RADIUS -= deltaDistance;
                        }
                        prevX = currentX;
                        prevY = currentY;
                        if (RADIUS <= 50) {
                            RADIUS = 50;
                            prevX = 0;
                            prevY = 0;
                        }
                    }
                    mView.tvRadius.setText(RADIUS + " meters");
                    setMapZoom();
                    drawRadius();
                }
                break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL: {
                    currentX = (int) event.getX();
                    currentY = (int) event.getY();

                    if (prevX == 0 || prevY == 0) {
                        pointEnd = mView.googleMap.getProjection()
                                .fromScreenLocation(new Point(currentX, currentY));
                        deltaDistance = (int) getDistance(pointStart, pointEnd);
                        if (currentX >= startX) {
                            RADIUS += deltaDistance;
                        } else {
                            RADIUS -= deltaDistance;
                            if (RADIUS <= 50) {
                                RADIUS = 50;
                            }
                        }
                    } else {
                        pointStart = mView.googleMap.getProjection()
                                .fromScreenLocation(new Point(prevX, prevY));
                        pointEnd = mView.googleMap.getProjection()
                                .fromScreenLocation(new Point(currentX, currentY));
                        deltaDistance = (int) getDistance(pointStart, pointEnd);
                        if (currentX >= prevX) {
                            RADIUS += deltaDistance;
                        } else {
                            RADIUS -= deltaDistance;
                            if (RADIUS <= 50) {
                                RADIUS = 50;
                            }
                        }
                    }
                    mView.tvRadius.setText(RADIUS + " meters");
                    prevX = 0;
                    prevY = 0;
                    setMapZoom();
                    mView.ivRadius.setVisibility(View.VISIBLE);
                    setPinToMap(mCKPlaceModel.siteLocation);
                }
                break;

            }
            return true;
        }
    };


    public double getDistance (LatLng pointStart, LatLng pointEnd){
        double startLatitude = pointStart.latitude;
        double startLongitude = pointStart.longitude;
        double endLatitude = pointEnd.latitude;
        double endLongitude = pointEnd.longitude;
        float[] result = new float[1];
        Location.distanceBetween(startLatitude, startLongitude, endLatitude,
                endLongitude, result);
        return result[0];
    }

    public void setMapZoom () {
        if (RADIUS <= 300) {
            MAP_ZOOM = 16;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        } else if (RADIUS <= 700) {
            MAP_ZOOM = 15;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        } else if (RADIUS <= 1400) {
            MAP_ZOOM = 14;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        } else if (RADIUS <= 2500) {
            MAP_ZOOM = 13;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        } else if (RADIUS <= 5000) {
            MAP_ZOOM = 12;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        } else if (RADIUS <= 7000) {
            MAP_ZOOM = 11;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        } else if (RADIUS <= 10000) {
            MAP_ZOOM = 10;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        } else {
            MAP_ZOOM = 9;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        }

    }

    private void drawRadius () {
        mView.googleMap.clear();
        Marker myMarker = mView.googleMap
                .addMarker(new com.google.android.gms.maps.model.MarkerOptions()
                        .position(mCKPlaceModel.siteLocation).icon(
                                BitmapDescriptorFactory.fromResource(R.mipmap.pin_site_new)));

        mView.googleMap
                .addCircle(new CircleOptions()
                        .center(mCKPlaceModel.siteLocation)
                        .radius(RADIUS)
                        .strokeWidth(0f)
                        .fillColor(
                                mContext.getResources().getColor(
                                        R.color.alpha_blue)));
    }

    @Override
    public View getView () {
        return mView;
    }

    private void backToHome () {
        Utilities.hideKeyboard(mContext, mView);
        ((MainActivity)mContext).popFragment();

        //((MainActivity)mContext).switchFragment(Constants.FRAGMENT_HOME,Constants.ANIMATION_VIEW_TYPE_DISMISS,null);
    }

    public void showProjectList () {

        String[] projectList = new String[projectStatusList.size() + 1];

        projectList[0] = "None";
        for (int i = 0; i < projectStatusList.size(); i++) {
            String projectName;
            if (projectStatusList.get(i).projectName.length() >= 15) {
                projectName = projectStatusList.get(i).projectName.substring(0, 15) + "...";
            } else {
                projectName = projectStatusList.get(i).projectName;
            }
            projectList[i + 1] = String.format("%s - %s", projectName, projectStatusList.get(i).projectId);
        }

        final NumberPicker projectPicker = new NumberPicker(mContext);
        projectPicker.setMinValue(0);
        projectPicker.setMaxValue(projectList.length - 1);
        projectPicker.setDisplayedValues(projectList);

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(mContext, R.style.Theme_DialogCustom));

        builder.setTitle("Choose Project")
                .setView(projectPicker)
                .setPositiveButton(
                        mContext.getResources().getString(R.string.btn_ok_title),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,int which) {
                                projectPicker.clearFocus();
                                int selected = projectPicker.getValue();
                                if (which == 0) {
                                    mView.etPNumber.setText("None");
                                    mCKPlaceModel.projectId = -1;
                                } else {
                                    mView.etPNumber.setText(String.valueOf(projectStatusList.get(selected - 1).projectId));
                                    mCKPlaceModel.projectId = projectStatusList.get(selected - 1).projectStatusId;
                                }
                                dialog.dismiss();
                            }

                        })
                .setNegativeButton(
                        mContext.getResources().getString(R.string.btn_cancel_title),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();
    }


	@Override
	public void onMapReady(GoogleMap googleMap) {
		if (mView.googleMap == null) {
			mView.googleMap = googleMap;
		}

		mView.createMapHolder();
		loadDataWhenMapReady();
	}

	private void enableEdit(boolean active) {
		mView.btnNoSafeZone.setEnabled(active);
		mView.btnYesSafeZone.setEnabled(active);
		//mView.btnNoBlackpot.setEnabled(active);
		//mView.btnYesBlackpot.setEnabled(active);
		mView.etSiteName.setEnabled(active);
		mView.etPNumber.setEnabled(active);
		mView.etStNumber.setEnabled(active);
		mView.etStName.setEnabled(active);
		mView.etStSuburb.setEnabled(active);
		mView.etState.setEnabled(active);
		mView.btnPNumber.setEnabled(active);

		if (active) {
			mView.ivDot.setVisibility(View.VISIBLE);
		} else {
			mView.ivDot.setVisibility(View.GONE);
		}
	}

	private void setDataToView(CKPlaceModel site) {

		if (site != null) {
			RADIUS = site.radius;
			mView.tvRadius.setText(RADIUS + " meters");
            mView.etSiteName.setText(site.name);
			mView.etState.setText(site.state);
			mView.etStName.setText(site.stName);
			mView.etStNumber.setText(site.stNumber);
			mView.etStSuburb.setText(site.suburb);
			//Log.e("SITE_DETAIL", "site.getProjectId() =" + site.projectId);

            if (site.projectId == 0) {
				mView.etPNumber.setText("None");
			} else {
				mView.etPNumber.setText(String.valueOf(site.projectId));
			}

			if (site.safezone) {
				mView.btnYesSafeZone.setBackgroundResource(R.drawable.border_right);
				mView.btnNoSafeZone.setBackgroundResource(R.drawable.border_left_selected);
			} else {
				mView.btnYesSafeZone.setBackgroundResource(R.drawable.border_right_selected);
				mView.btnNoSafeZone.setBackgroundResource(R.drawable.border_left);
			}

//			if (site.blackspot) {
//				mView.btnYesBlackpot.setBackgroundResource(R.drawable.border_right);
//				mView.btnNoBlackpot.setBackgroundResource(R.drawable.border_left_selected);
//			} else {
//				mView.btnYesBlackpot.setBackgroundResource(R.drawable.border_right_selected);
//				mView.btnNoBlackpot.setBackgroundResource(R.drawable.border_left);
//			}

			setPinToMap(SBApplication.getSiteModel().siteLocation);

		}
	}

}