package com.clockme.controllers.menu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.activities.EntityActivity;
import com.clockme.activities.MainActivity;
import com.clockme.activities.ManDownActivity;
import com.clockme.controllers.AbstractController;
import com.clockme.controllers.ActionController;
import com.clockme.models.CKPersonModel;
import com.clockme.utilities.Constants;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.UtilDialog;
import com.clockme.views.menu.MenuView;
import com.uservoice.uservoicesdk.UserVoice;

import java.lang.reflect.Field;

public class MenuController extends AbstractController implements
        OnClickListener, OnItemClickListener {

    private static final String TAG = MenuController.class.getSimpleName();

    private Context mContext;
    private MenuView mView;
    private TypedArray arrIcons;
    private String[] arrMenuItem = null;
    private MenuListAdapter adapter;
    private int position = 0;
    private Resources res;
    private int listIcons;
    private Handler handler;

    public MenuController(Context context, MenuView view) {
        this.mContext = context;
        this.mView = view;
        this.res = mContext.getResources();
        handler = new Handler();
    }

    @Override
    public void initData() {
        super.initData();

        // hack to always show the overflow menu in the action bar
        try {
            ViewConfiguration viewConfig = ViewConfiguration.get(mContext);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(viewConfig, false);
            }
        } catch (Exception ex) {
            // Ignore
        }

        setHeaderTitle();
        setMenuContent();
    }

    @Override
    public void setListener() {
        super.setListener();
        mView.vMenuHeader.setOnClickListener(this);
        mView.lvMenu.setOnItemClickListener(this);
        mView.btnLogOff.setOnClickListener(this);
    }

    @Override
    public View getView() {
        return mView;
    }

    @Override
    public void onClick(View v) {
        if (v == mView.vMenuHeader) {

            //Show new activity to pick entity
            Intent i = new Intent(mContext, EntityActivity.class);
            mContext.startActivity(i);
            ((Activity) mContext).overridePendingTransition(R.anim.in_from_bottom, R.anim.stand_by);

        } else if (v == mView.btnLogOff) {

            if (SBApplication.getSiteModel().isCheckin) {
                UtilDialog.showAlertWith1Button(mContext, "", mContext.getResources().getString(R.string.msg_login_logoff_still_login), mContext.getResources().getString(R.string.btn_ok_title), null);
            } else {
                new LogOff().execute();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
        this.position = position - 1;
        switchView();
    }

    public void setHeaderTitle() {

        try {

            String entityName = SBApplication.getEntityModel().companyName;

            if (TextUtils.isEmpty(entityName)) {
                mView.vMenuHeader.tvHeaderTitle.setText(mContext.getResources().getString(R.string.msg_no_entity));
            } else {
                mView.vMenuHeader.tvHeaderTitle.setText(entityName);
            }

        }catch (Exception e){
            mView.vMenuHeader.tvHeaderTitle.setText(mContext.getResources().getString(R.string.msg_no_entity));
            Log.e(TAG, "setHeaderTitle() - Error :=> " + e);
        }

    }

    /**
     * To add items to the menu list, go to the strings.xml file
     * and add items to the menu arrays
     */
    public void setMenuContent() {
        if (SBApplication.getUserModel().accessLevel == CKPersonModel.ACCESS_LEVEL_ADMIN || SBApplication.getUserModel().accessLevel == CKPersonModel.ACCESS_LEVEL_TEAM_LEADER) {
            listIcons = R.array.menu_leader_user_icons;
            arrMenuItem = mContext.getResources().getStringArray(R.array.menu_leader_user);
        } else {
            listIcons = R.array.menu_normal_user_icons;
            arrMenuItem = mContext.getResources().getStringArray(R.array.menu_normal_user);
        }

        arrIcons = res.obtainTypedArray(listIcons);
        adapter = new MenuListAdapter(mContext, arrMenuItem, arrIcons);
        mView.lvMenu.setAdapter(adapter);
    }

    final class LogOff extends AsyncTask<Void, Void, Void> {

        public LoadingBeat loadingBeat;

        public LogOff() {
            loadingBeat = new LoadingBeat(mContext);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingBeat.showLoading();
        }

        @Override
        protected Void doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            //Clean all cached user info
            ActionController.getInstance().UserCheckoutSuccess(mContext);

            loadingBeat.cancelLoading();
            ((MainActivity) mContext).toggle();

            //Take user to login view
            SBApplication.gotoLoginPage();
        }

    }

    private void switchView() {

        int fragmentId = getFragmentIndex(position,SBApplication.getUserModel().accessLevel);
        if (fragmentId == Constants.SUPPORT_SCREEN) {
            UserVoice.launchUserVoice(mContext);
        }else if (fragmentId == Constants.FRAGMENT_MAN_DOWN) {
            Intent i = new Intent(((Activity) mContext),ManDownActivity.class);
            ((Activity) mContext).startActivity(i);
            ((Activity) mContext).overridePendingTransition(R.anim.in_from_bottom, R.anim.stand_by);
        } else {

            ((MainActivity) mContext).switchFragment(fragmentId,Constants.ANIMATION_VIEW_TYPE_ADD,null);

            //Toggle the view only on this cases
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ((MainActivity) mContext).toggle();
                }
            }, 250);

        }

    }

    /**
     * Get fragment index from constants class to display correct
     * fragment, when user has an entity
     * Important!! - need to update switch statement when adding/deleting
     *               menu items
     * @param position position of item selected from sliding menu
     * @return int value of screen set in Constants class
     */
    private int getFragmentIndex(int position, int accessLevel) {
        if (accessLevel == CKPersonModel.ACCESS_LEVEL_ADMIN || accessLevel == CKPersonModel.ACCESS_LEVEL_TEAM_LEADER){
            switch (position) {
                case 0:
                    return Constants.FRAGMENT_HOME;
                case 1:
                    return Constants.FRAGMENT_TEAM_STATUS;
                case 2:
                    return Constants.FRAGMENT_CONNECTION;
                case 3:
                    return Constants.FRAGMENT_NOTIFICATION;
                case 4:
                    return Constants.FRAGMENT_MESSAGE;
                case 5:
                    return Constants.FRAGMENT_DOCUMENT;
                case 6:
                    return Constants.SUPPORT_SCREEN;
                case 7:
                    return Constants.FRAGMENT_MAN_DOWN;
                case 8:
                    return Constants.FRAGMENT_PROFILE;
                default:
                    return Constants.FRAGMENT_HOME;
            }
        }else{
            switch (position) {
                case 0:
                    return Constants.FRAGMENT_HOME;
                case 1:
                    return Constants.FRAGMENT_CONNECTION;
                case 2:
                    return Constants.FRAGMENT_NOTIFICATION;
                case 3:
                    return Constants.FRAGMENT_MESSAGE;
                case 4:
                    return Constants.FRAGMENT_DOCUMENT;
                case 5:
                    return Constants.SUPPORT_SCREEN;
                case 6:
                    return Constants.FRAGMENT_MAN_DOWN;
                case 7:
                    return Constants.FRAGMENT_PROFILE;
                default:
                    return Constants.FRAGMENT_HOME;
            }
        }
    }

}
