package com.clockme.controllers.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.clockme.controllers.AbstractController;
import com.clockme.views.menu.MenuItemView;

public class MenuItemController extends AbstractController {

	private Context mContext;
	private MenuItemView mView;
	private String title;
	private Drawable icon;
	private int count;

	public MenuItemController(Context context, MenuItemView view, String title, Drawable _icon, int count) {
		this.mContext = context;
		this.mView = view;
		this.title = title;
		this.icon = _icon;
		this.count = count;
	}

	@Override
	public void initData() {
		super.initData();
		mView.tvItemTitle.setText(title);
		mView.ivItemIcon.setImageDrawable(icon);
		mView.tvItemCount.setText(Integer.toString(count));
	}

	@Override
	public void setListener() {
		super.setListener();
	}

	@Override
	public View getView() {
		return mView;
	}

}
