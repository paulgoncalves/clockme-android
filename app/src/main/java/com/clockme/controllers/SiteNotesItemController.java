package com.clockme.controllers;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;

import com.clockme.R;
import com.clockme.models.NotesModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.Constants;
import com.clockme.utilities.Utilities;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * Created by AppDev on 25/08/15.
 */
public class SiteNotesItemController extends AbstractController implements View.OnClickListener{
    private static final String TAG = "SiteNotesItemController";

    private Context mContext;
    private SiteNoteItemView mView;
    private NotesModel mNotesModel;
    private SiteNotesController mSiteNotesController;

    public SiteNotesItemController(Context context, SiteNoteItemView view, NotesModel notesModel,
                                   SiteNotesController controller) {
        this.mContext = context;
        this.mView = view;
        this.mNotesModel = notesModel;
        this.mSiteNotesController = controller;
    }

    @Override
    public void initData() {
        super.initData();

        loadNotesInfo();
        setListener();

    }

    @Override
    public void setListener() {
        super.setListener();
    }

    @Override
    public View getView() {
        return mView;
    }

    @Override
    public void onClick(View v) {

    }

    /*
     * get checkInNotes from web services
     */
    private void loadNotesInfo() {

        /*
        final String avatarUrl = String.format(
                WebservicesHelper.LOAD_USER_IMAGE_URL, mHazardModel.personId,
                mHazardModel.personId);
        */

        //Set image
        WebservicesHelper.getInstance().webserviceLoadImage(mView.imgAvatar, Constants.IMG_TYPE_USER,mNotesModel.personId);

        // todo: remove after testing
        // using constant values for user Id, need to get from Notes model
        // e.g Notes.actionId
        final String avatarUrl = String.format(
                WebservicesHelper.LOAD_USER_IMAGE_URL, "37",
                "37");

        Bitmap bmpAvatar = Utilities.loadCachedImage(avatarUrl);
        if (bmpAvatar != null) {
            mView.imgAvatar.setImageBitmap(bmpAvatar);
        } else {
            mView.imgAvatar.setImageBitmap(null);
            mView.imgAvatar.setImageResource(R.mipmap.icon_small_avatar);
            if (!TextUtils.isEmpty(avatarUrl)
                    && !avatarUrl.equalsIgnoreCase("null")) {
                ImageLoader.getInstance().displayImage(avatarUrl,
                        mView.imgAvatar, new ImageLoadingListener() {

                            @Override
                            public void onLoadingStarted(String imageUri,
                                                         View view) {
                            }

                            @Override
                            public void onLoadingFailed(String imageUri,
                                                        View view, FailReason failReason) {
                            }

                            @Override
                            public void onLoadingComplete(String imageUri,
                                                          View view, Bitmap loadedImage) {
                                if (loadedImage != null) {
                                    Utilities.cacheAvatarImage(loadedImage,
                                            avatarUrl);
                                }
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri,
                                                           View view) {
                            }
                        });
            }
        }

        mView.tvNote.setText(mNotesModel.note);
        mView.tvEntityName.setText("Test: Entity Name");
        mView.tvDateTimeChanged.setText("Last changed: " + mNotesModel.time);

        setListener();
    }



}
