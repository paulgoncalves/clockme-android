package com.clockme.controllers;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.activities.MainActivity;
import com.clockme.interfaces.ILoading;
import com.clockme.interfaces.IWebservice;
import com.clockme.models.CKPersonModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.Constants;
import com.clockme.utilities.DateTimeUtils;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.RequestConnectionDialog;
import com.clockme.utilities.UtilDialog;

import org.json.JSONObject;

/**
 * Created by tannguyen on 4/27/15.
 */
public class EntityUsersItemController extends AbstractController implements View.OnClickListener {
    public Context mContext;
    private EntityUsersItemView mView;
    private EntityUserModel mModel;
    private LoadingBeat mLoadingBeat;
    private EntityUsersController mController;

    public EntityUsersItemController(Context context, EntityUsersItemView view, EntityUserModel connObj,EntityUsersController controller) {
        this.mContext = context;
        this.mView = view;
        this.mModel = connObj;
        mLoadingBeat = new LoadingBeat(mContext);
        mController = controller;
    }

    public void initData(EntityUserModel _mModel) {
        super.initData();
        mModel = _mModel;
        mView.tvUserName.setText(mModel.firstName + " " + mModel.lastName);

        //String strAction = mModel.getAction();
        if (mModel.isConnected && !mModel.pendingRequest) {
            mView.btnAction.setBackgroundResource(R.drawable.selector_btn_red);
            mView.btnAction.setText(Constants.REMOVE);
        } else if (mModel.isConnected && mModel.pendingRequest) {
            mView.btnAction.setBackgroundResource(R.drawable.selector_btn_grey);
            mView.btnAction.setText(Constants.PENDING);
        }else {
            mView.btnAction.setBackgroundResource(R.drawable.selector_green_button);
            mView.btnAction.setText(Constants.ADD);
        }

        mView.ivAvatar.setImageResource(R.mipmap.icon_small_avatar);
        //Set image
        WebservicesHelper.getInstance().webserviceLoadImage(mView.ivAvatar, Constants.IMG_TYPE_USER, mModel.userId);
    }

    private void requestAddConnection(int permission) {

        String body = String.format(WebservicesHelper.PARAMS_CONNECTION_ADD,
                SBApplication.getUserModel().personId,SBApplication.getUserModel().token,
                SBApplication.getEntityModel().companyId,mModel.userId,permission,
                DateTimeUtils.getCurrentTimeStamp());
        WebservicesHelper.getInstance().webserviceRequest(mContext,iLoading,IWebserviceRequest,WebservicesHelper.ROUTE_CONNECTION_ADD,body);
    }

    private void requestRemoveConnection() {

        String body = String.format(WebservicesHelper.PARAMS_CONNECTION_REMOVE,SBApplication.getUserModel().personId,
                SBApplication.getUserModel().token,mModel.userId);
        WebservicesHelper.getInstance().webserviceRequest(mContext,iLoading,IWebserviceRequest,WebservicesHelper.ROUTE_CONNECTION_REMOVE,body);

    }

    IWebservice IWebserviceRequest = new IWebservice() {

        @Override
        public void onComplete(JSONObject WSResponse, String WSMessage, int WSCode) {

            if (WSResponse.optInt("status") == 1) {

                if (WSCode == WebservicesHelper.WSCODE_CONNECTION_ADD_SUCCESS) {

                    mController.requestEntityUsers();

                }else if (WSCode == WebservicesHelper.WSCODE_CONNECTION_REMOVE_SUCCESS) {

                    mController.requestEntityUsers();
                }
            }else{
                //mLoadingBeat.showLoading();
                UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
            }

        }

        @Override
        public void onError(String error) {
            UtilDialog.showAlertWith1Button(mContext, "", error, mContext.getResources().getString(R.string.ok), null);
            mLoadingBeat.cancelLoading();
        }

        @Override
        public void onAccessDenied(String WSMessage) {
            mLoadingBeat.cancelLoading();
            UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
            CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
        }
    };

    ILoading iLoading = new ILoading() {

        @Override
        public void showLoading() {
            //mLoadingBeat.showLoading();
        }
    };

    @Override
    public void setListener() {
        super.setListener();
        mView.btnAction.setOnClickListener(this);
        mView.tvUserName.setOnClickListener(this);
    }

    @Override
    public View getView() {
        return mView;
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    public void onClick(View v) {
        if (v == mView.btnAction) {

            //Do action depending on button title
            String btnTitle = mView.btnAction.getText().toString();

            if (btnTitle.equals(Constants.ADD)){

                //Handle connection request by users permission
                requestConnectionType(mModel);

            }else if (btnTitle.equals(Constants.PENDING)) {
                UtilDialog.showAlertWith2Buttons((Activity) mContext, "",
                        String.format(mContext.getResources().getString(R.string.msg_connection_remove), mModel.firstName, mModel.lastName),
                        mContext.getResources().getString(R.string.btn_no_title),
                        mContext.getResources().getString(R.string.btn_yes_title), null, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                requestRemoveConnection();
                            }
                        });

            } else if (btnTitle.equals(Constants.REMOVE)) {
                UtilDialog.showAlertWith2Buttons((Activity) mContext, "",
                        String.format(mContext.getResources().getString(R.string.msg_connection_remove), mModel.firstName, mModel.lastName),
                        mContext.getResources().getString(R.string.btn_no_title),
                        mContext.getResources().getString(R.string.btn_yes_title), null, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                requestRemoveConnection();
                            }
                        });
            }


        } else if (v == mView.tvUserName) {

            //Dont pass the bundle with object, just send the personId that will fetch the single user connection
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.KEY_CONNECTION, new ConnectionModel().getConnectionFromEntityUserModel(mModel));
            bundle.putBoolean(Constants.KEY_REFRESH,true);
            ((MainActivity) mContext).switchFragment(Constants.FRAGMENT_USER_VIEW,Constants.ANIMATION_VIEW_TYPE_SLIDE,bundle);

        }

    }

    private void requestConnectionType(final EntityUserModel connectionRequestModel) {

        final int connType;
        if (connectionRequestModel.accessLevel == Constants.USER_ACCESS_LEVEL_NORMAL){

            //If user requesting the connection is Admin or Manager prompt him about permission type
            if (SBApplication.getUserModel().accessLevel == Constants.USER_ACCESS_LEVEL_MANAGER ||
                    SBApplication.getUserModel().accessLevel == Constants.USER_ACCESS_LEVEL_LEADER){

                connType = Constants.CONNECTION_TYPE_FOLLOWER;

            }else{

                connType = Constants.CONNECTION_TYPE_TEAM_MEMBER;

            }

        }else{

            //If user requesting the connection is Admin or Manager prompt him about permission type
            if (SBApplication.getUserModel().accessLevel == Constants.USER_ACCESS_LEVEL_MANAGER ||
                    SBApplication.getUserModel().accessLevel == Constants.USER_ACCESS_LEVEL_LEADER){

                connType = Constants.CONNECTION_TYPE_LEADER_FOLLOWER;
            }else{
                connType = Constants.CONNECTION_TYPE_TEAM_LEADER;
            }

        }

        //Show dialog to choose if connection type is not Team Member request
        if (connType != Constants.CONNECTION_TYPE_TEAM_MEMBER){

            RequestConnectionDialog requestConn = new RequestConnectionDialog(mContext, Constants.THEME);
            requestConn.show();
            requestConn
                    .setContent(
                            mContext.getResources().getString(R.string.msg_connection_request_title),
                            mContext.getResources().getString(R.string.msg_connection_request_message),
                            connType,
                            mContext.getResources().getString(R.string.btn_ok_title),
                            mContext.getResources().getString(R.string.btn_cancel_title), new IRequestConnectionDialog() {
                                @Override
                                public void onOk(int permission) {
                                    requestAddConnection(permission);
                                }

                                @Override
                                public void onCancel() {
                                    // dissmiss and do nothing
                                }
                            });

        }else{

            //Request connection type Team Member
            requestAddConnection(1);

        }
    }

}