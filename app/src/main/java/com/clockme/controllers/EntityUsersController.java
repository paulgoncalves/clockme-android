package com.clockme.controllers;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.activities.MainActivity;
import com.clockme.interfaces.ILoading;
import com.clockme.interfaces.IWebservice;
import com.clockme.models.CKPersonModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.Constants;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.UtilDialog;
import com.clockme.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by tannguyen on 4/25/15.
 */
public class EntityUsersController extends AbstractController implements View.OnClickListener, AdapterView.OnItemClickListener {

    public Context mContext;
    public EntityUserView mView;
    public LoadingBeat loadingBeat;
    public EntityUsersListAdapter adapter;
    ArrayList<EntityUserModel> arrayUsersList = new ArrayList<>();

    public EntityUsersController(Context context, EntityUserView view) {
        this.mContext = context;
        this.mView = view;
        loadingBeat = new LoadingBeat(mContext);
        mView.etSearchMembers.addTextChangedListener(keyWordChangeListener);

        mView.mSwipeRefreshLayout.setColorSchemeResources(R.color.blue, R.color.orange);
        mView.mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mView.mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void initData() {
        super.initData();
        requestEntityUsers();
    }

    public void requestEntityUsers() {

        arrayUsersList.clear();

        String body = String.format(WebservicesHelper.PARAMS_MY_TEAM,SBApplication.getUserModel().personId,SBApplication.getUserModel().token,
                SBApplication.getEntityModel().companyId,SBApplication.getUserModel().accessLevel);

        WebservicesHelper.getInstance().webserviceRequest(mContext,iLoading,IWebserviceRequest,WebservicesHelper.ROUTE_USER_ENTITY,body);
    }

    IWebservice IWebserviceRequest = new IWebservice() {

        @Override
        public void onComplete(JSONObject WSResponse, String WSMessage, int WSCode) {

            if (WSResponse.optInt("status") == 1) {

                //Site retrieved
                if (WSCode == WebservicesHelper.WSCODE_CONNECTION_FETCH_SUCCESS) {

                    try {

                        JSONArray arrayUsers = WSResponse.getJSONArray("connection");

                        for (int i = 0; i < arrayUsers.length(); i++) {
                            arrayUsersList.add(new EntityUserModel().getMyTeamFromJson(arrayUsers.getJSONObject(i)));
                        }

                        adapter = new EntityUsersListAdapter(mContext,arrayUsersList,EntityUsersController.this);

                        mView.lvEntityUsers.setAdapter(adapter);
                        mView.etSearchMembers.setText("");

                    }catch (JSONException ex){
                        ex.printStackTrace();
                    }
                }
            }

            loadingBeat.cancelLoading();
            mView.mSwipeRefreshLayout.setRefreshing(false);
        }

        @Override
        public void onError(String error) {
            UtilDialog.showAlertWith1Button(mContext, "", error, mContext.getResources().getString(R.string.ok), null);
            loadingBeat.cancelLoading();
            mView.mSwipeRefreshLayout.setRefreshing(false);
        }

        @Override
        public void onAccessDenied(String WSMessage) {
            loadingBeat.cancelLoading();
            mView.mSwipeRefreshLayout.setRefreshing(false);
            UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
            CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
        }
    };

    ILoading iLoading = new ILoading() {

        @Override
        public void showLoading() {
            loadingBeat.showLoading();
        }

//        @Override
//        public void hideLoading() {
//            if (loadingBeat != null) {
//                loadingBeat.cancelLoading();
//            }
//        }
    };

    private TextWatcher keyWordChangeListener = new TextWatcher() {
        String strString = "";
        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                  int arg3) {
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
        }

        @Override
        public void afterTextChanged(final Editable arg0) {

            if (arg0 != null && arg0.length() > 0) {
                mView.navView.removeCallbacks(searchDelay);
                strString = arg0.toString().toUpperCase();
                mView.lvEntityUsers.postDelayed(searchDelay, 400);
                Log.e("SEARCH", "isWaiting = true - start searching");
            }
            else{
                Log.e("SEARCH", "null search text");
                if(adapter!= null) {
                    adapter.setArrayConnection(arrayUsersList);
                    adapter.notifyDataSetChanged();
                }
            }
        }

        Runnable searchDelay = new Runnable() {
            @Override
            public void run() {
                searchByName(strString);
                Log.e("SEARCH", "isWaiting = false search delay");
            }
        };
    };

    private void searchByName(String strString) {
        strString = strString.replace(" ", "");
        ArrayList<EntityUserModel> results = new ArrayList<>();
        if (arrayUsersList.size() != 0) {
            for (int i = 0; i < arrayUsersList.size(); i++) {
                EntityUserModel myTeam = arrayUsersList.get(i);
                String strName = myTeam.firstName + myTeam.lastName;
                strName.replace(" ", "");
                if (strName.toUpperCase().contains(strString)) {
                    results.add(myTeam);
                }
            }
            adapter.setArrayConnection(results);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void setListener() {
        super.setListener();
        mView.navView.btnRight.setOnClickListener(this);
        mView.navView.btnLeft.setOnClickListener(this);

        mView.mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestEntityUsers();
            }
        });

        mView.lvEntityUsers.setOnItemClickListener(this);
    }


    @Override
    public View getView() {
        return mView;
    }

    @Override
    public void onClick(View v) {

        if (v == mView.navView.btnLeft) {
            mView.etSearchMembers.setText("");
            backToHome();
        }
    }

    private void backToHome() {
        Utilities.hideKeyboard(mContext, mView);
        ((MainActivity) mContext).popFragment();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        //Dont pass the bundle with object, just send the personId that will fetch the single user connection
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_CONNECTION, new ConnectionModel().getConnectionFromEntityUserModel(arrayUsersList.get(position)));
        ((MainActivity) mContext).switchFragment(Constants.FRAGMENT_USER_VIEW,Constants.ANIMATION_VIEW_TYPE_SLIDE, bundle);

    }
}

