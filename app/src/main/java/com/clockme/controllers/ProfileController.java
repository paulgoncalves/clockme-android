package com.clockme.controllers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.activities.MainActivity;
import com.clockme.interfaces.IChangeAvatar;
import com.clockme.interfaces.IChangePassword;
import com.clockme.interfaces.ILoading;
import com.clockme.interfaces.IUploadImage;
import com.clockme.interfaces.IWebservice;
import com.clockme.models.CKPersonModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.Constants;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.UtilDialog;
import com.clockme.utilities.Utilities;
import com.clockme.views.ChangePasswordView;
import com.clockme.views.ProfileView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProfileController extends AbstractController implements
		OnClickListener {

	private static final String TAG = ProfileController.class.getSimpleName();

	private Context mContext;
	private ProfileView mView;
	private boolean isSaved = false;
	private ChangePasswordView vChangePassword;
	private ChangePasswordController ctrlChangePassword;
	private LoadingBeat mLoadingBeat;
	private LogsListAdapter mLogadapter;
	private String avatarUrl = "";
	private Bitmap bmp = null;

	public ProfileController(Context context, ProfileView view) {
		this.mContext = context;
		this.mView = view;
	}

	@Override
	public void initData() {
		super.initData();

		mLoadingBeat = new LoadingBeat(mContext);
		vChangePassword = new ChangePasswordView(mContext);
		ctrlChangePassword = new ChangePasswordController(mContext,vChangePassword);

		//Load user information and image
		loadUserDetail();

		//Request log
		requestUserLog();

	}

	private void requestUserLog(){

		String body = String.format(WebservicesHelper.PARAMS_USER_LOG, SBApplication.getUserModel().personId,SBApplication.getUserModel().token);
		WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_USER_LOG, body);

	}

	private void requestEditProfile() {

		String body = String.format(WebservicesHelper.PARAMS_USER_UPDATE_USER,SBApplication.getUserModel().personId,SBApplication.getUserModel().token,
				mView.etFirstName.getText(),mView.etLastName.getText(),mView.etEmail.getText(),mView.etMobile.getText(),mView.etEmergencyNumber.getText());
		WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_USER_UPDATE, body);

	}

	private void requestChangePassword(String currentPassword, String newPassword) {

		String body = String.format(WebservicesHelper.PARAMS_USER_UPDATE_USER_PASSWORD,SBApplication.getUserModel().personId,SBApplication.getUserModel().token,currentPassword,newPassword);
		WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_USER_UPDATE_PASSWORD, body);

	}

	IWebservice IWebserviceRequest = new IWebservice() {

		@Override
		public void onComplete(JSONObject WSResponse, String WSMessage, int WSCode) {

			if (WSResponse.optInt("status") == 1) {

				if (WSCode == WebservicesHelper.WSCODE_LOG_USER_SUCCESS) {

					try {

						ArrayList<LogModel> arrUserLogs = new ArrayList();
						JSONArray jArrUserLogs = WSResponse.getJSONArray("log");

						for (int i = 0; i < WSResponse.length(); i++) {
							arrUserLogs.add(new LogModel().getLogModelFromJson(jArrUserLogs.getJSONObject(i)));
						}

						mLogadapter = new LogsListAdapter(mContext, arrUserLogs);
						mView.lvLog.setAdapter(mLogadapter);

					}catch (JSONException ex){
						ex.printStackTrace();
					}

					mLoadingBeat.cancelLoading();

				}else if (WSCode == WebservicesHelper.WSCODE_USER_UPDATE_SUCCESS) {

					SBApplication.getUserModel().getUserModelFromJson(WSResponse);
					//Update user with new information after saving
					CKPersonModel.getInstance().updateUserInfo(mContext);

					UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
					mLoadingBeat.cancelLoading();

				}else if (WSCode == WebservicesHelper.WSCODE_USER_UPDATE_PASSWORD_SUCCESS) {

					UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
					mLoadingBeat.cancelLoading();

				}

			}else{

				UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
				mLoadingBeat.cancelLoading();

			}

		}

		@Override
		public void onError(String error) {
			mLoadingBeat.cancelLoading();
			UtilDialog.showAlertWith1Button(mContext, "", error, mContext.getResources().getString(R.string.ok), null);
		}

		@Override
		public void onAccessDenied(String WSMessage) {
			mLoadingBeat.cancelLoading();
			UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
			CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
		}

	};

	ILoading iLoading = new ILoading() {

		@Override
		public void showLoading() {
			mLoadingBeat.showLoading();
		}
	};

	IChangePassword iChangePassword = new IChangePassword() {

		@Override
		public void onError(String error) {
			UtilDialog.showAlertWith1Button(mContext, null, error, mContext
					.getResources().getString(R.string.btn_ok_title), null);
			mLoadingBeat.cancelLoading();
		}

		@Override
		public void onComplete(boolean isSuccessful, String message) {
			UtilDialog.showAlertWith1Button(mContext, null, message, mContext
					.getResources().getString(R.string.btn_ok_title), null);
			mLoadingBeat.destroyLoading();
		}

		@Override
		public void onChangePassword(String currentPassword, String newPassword) {
			requestChangePassword(currentPassword,newPassword);
		}
	};

	final class UploadImage extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			if (bmp  != null) {
				WebservicesHelper.getInstance().webserviceUploadImage(iUploadImage,
						WebservicesHelper.ROUTE_FOLDER_USER, SBApplication.getUserModel().personId);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}
	}

	IUploadImage iUploadImage = new IUploadImage() {

		@Override
		public void onComplete(int WSResponsecode) {

			if (WSResponsecode == 200) {
				UtilDialog.showAlertWith1Button(mContext, "", mContext.getResources().getString(R.string.msg_img_upload_success), mContext.getResources().getString(R.string.ok), null);

				//Clear cached image
				Utilities.clearCacheImage(avatarUrl);
				Utilities.cacheAvatarImage(bmp, avatarUrl);
			}
			mLoadingBeat.cancelLoading();
		}

		@Override
		public void onError(String error) {
			UtilDialog.showAlertWith1Button(mContext, "", mContext.getResources().getString(R.string.msg_img_upload_fail), mContext.getResources().getString(R.string.ok), null);
			mLoadingBeat.cancelLoading();
		}
	};


	@Override
	public void setListener() {
		super.setListener();
		ctrlChangePassword.initData();
		ctrlChangePassword.setListener();
		ctrlChangePassword.setCallBack(iChangePassword);

		mView.rlRootView.setOnClickListener(this);
		mView.scrollView.setOnClickListener(this);

		mView.navView.btnLeft.setOnClickListener(this);
		mView.navView.btnRight.setOnClickListener(this);
		mView.ivAvatar.setOnClickListener(this);
		mView.btnMyLeaders.setOnClickListener(this);
		mView.btnChangePassword.setOnClickListener(this);

		//Call back from MainActivity with image cropped
		((MainActivity) mContext).setCallBack(new IChangeAvatar() {

			@Override
			public void onImageListener() {
				bmp = BitmapFactory.decodeFile(Environment
						.getExternalStorageDirectory().getAbsolutePath()
						+ "/"
						+ Constants.TEMP_PHOTO_FILE_NAME);
				mView.ivAvatar.setImageBitmap(bmp);

				//Disable all field
				disableFields();

				//Upload image after user selects
				new UploadImage().execute();
			}
		});

		mView.lvLog.setOnTouchListener(new ListView.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int action = event.getAction();
				switch (action) {
					case MotionEvent.ACTION_DOWN:
						v.getParent().requestDisallowInterceptTouchEvent(true);
						break;

					case MotionEvent.ACTION_UP:
						v.getParent().requestDisallowInterceptTouchEvent(false);
						break;
				}

				v.onTouchEvent(event);
				return true;
			}
		});
	}

	@Override
	public void onClick(View v) {
		if (v == mView.rlRootView || v == mView.scrollView) {
			Utilities.hideKeyboard(mContext, mView);
		} else if (v == mView.navView.btnLeft) {
			Utilities.hideKeyboard(mContext, mView);
			((MainActivity) mContext).toggle();
		} else if (v == mView.navView.btnRight) {
			if (isSaved) {

				if (checkAllValidFields()) {

					//Disable all the fields
					disableFields();

					//Save the user info
					requestEditProfile();
				}
			} else {
				enableFields();
			}
		} else if (v == mView.btnMyLeaders) {
			((MainActivity) mContext).switchFragment(Constants.FRAGMENT_LEAVE,Constants.ANIMATION_VIEW_TYPE_SLIDE,null);
		} else if (v == mView.btnChangePassword) {
			ctrlChangePassword.showDialog();
		} else if (v == mView.ivAvatar) {
			Log.d(TAG, "onClick() - ImageView fired!");
			showOptionToUploadAvatar();
		}
	}

	private boolean checkAllValidFields() {
		boolean result = true;
		String firstName = mView.etFirstName.getText().toString().trim();
		if (TextUtils.isEmpty(firstName)) {
			result = false;
		}

		String lastName = mView.etLastName.getText().toString().trim();
		if (TextUtils.isEmpty(lastName)) {
			result = false;
		}

		String mobile = mView.etMobile.getText().toString().trim();
		if (TextUtils.isEmpty(mobile)) {
			result = false;
		}

		if (!result) {
			UtilDialog.showAlertWith1Button(mContext, null, mContext
							.getResources().getString(R.string.error_empty_field),
					mContext.getResources()
							.getString(R.string.btn_cancel_title), null);
		}

		return result;
	}

	public void showOptionToUploadAvatar() {

		// need to convert the context to activity
		UtilDialog.showSelectImageDialog((MainActivity)mContext);

	}

	private void loadUserDetail(){

		//Load user detail
		mView.etEmail.setText(SBApplication.getUserModel().email);
		mView.etFirstName.setText(SBApplication.getUserModel().name);
		mView.etLastName.setText(SBApplication.getUserModel().lastName);
		mView.etMobile.setText(SBApplication.getUserModel().mobile);
		mView.etEmergencyNumber.setText(SBApplication.getUserModel().emergencyNumber);

		mView.ivAvatar.setImageBitmap(null);
		mView.ivAvatar.setImageResource(R.mipmap.icon_small_avatar);

		//Set image
		WebservicesHelper.getInstance().webserviceLoadImage(mView.ivAvatar, Constants.IMG_TYPE_USER, SBApplication.getUserModel().personId);

	}

	private void enableFields(){
		isSaved = true;
		mView.navView.btnRight.setText(mContext.getResources().getString(R.string.btn_save_title));
		mView.ivAvatar.setEnabled(true);
		mView.tvEditPhoto.setVisibility(View.VISIBLE);
		mView.etFirstName.setEnabled(true);
		mView.etLastName.setEnabled(true);
		mView.etMobile.setEnabled(true);
	}

	private void disableFields(){
		isSaved = false;
		mView.navView.btnRight.setText(mContext.getResources().getString(R.string.btn_edit_title));
		mView.etFirstName.setEnabled(false);
		mView.etLastName.setEnabled(false);
		mView.etMobile.setEnabled(false);
		mView.ivAvatar.setEnabled(false);
		mView.tvEditPhoto.setVisibility(View.GONE);
	}

	@Override
	public View getView() {
		return mView;
	}
}