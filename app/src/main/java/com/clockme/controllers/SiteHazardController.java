package com.clockme.controllers;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.clockme.SBApplication;
import com.clockme.activities.MainActivity;
import com.clockme.utilities.Constants;
import com.clockme.utilities.LoadingBeat;

/**
 * Created by AppDev on 18/08/15.
 */
public class SiteHazardController extends AbstractController implements View.OnClickListener {
    private static final String TAG = SiteHazardController.class.getSimpleName();
    private Context mContext;
    private SiteHazardView mView;
    private LoadingBeat mLoadingBeat;
    SiteHazardListAdapter listAdapter;

    public SiteHazardController(Context context, SiteHazardView view) {
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void initData() {
        super.initData();
        mLoadingBeat = new LoadingBeat(mContext);

        listAdapter = new SiteHazardListAdapter(mContext, SBApplication.getSiteModel().arrayHazard,
                SiteHazardController.this);
        mView.lvHazard.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();

        SBApplication.getSiteModel().showSiteHazard = false;
    }

    @Override
    public void setListener() {
        super.setListener();

        //mView.navView.btnLeft.setOnClickListener(this);
        mView.navView.btnRight.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Log.i(TAG, "onClick() fired");

       if(v == mView.navView.btnRight) {
           if (SBApplication.getSiteModel().showProjectStatus){
               ((MainActivity)mContext).swapFragment(Constants.FRAGMENT_SITE_PROJECT_STATUS,Constants.ANIMATION_VIEW_TYPE_FADE,null);
           }else {
               ((MainActivity)mContext).popFragment();
           }
        }
    }

    public void displayDoneButton() {
        mView.navView.btnRight.setVisibility(View.VISIBLE);
    }


}
