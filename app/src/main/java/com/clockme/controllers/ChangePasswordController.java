package com.clockme.controllers;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import com.clockme.R;
import com.clockme.interfaces.IChangePassword;
import com.clockme.utilities.Utilities;
import com.clockme.views.ChangePasswordView;

public class ChangePasswordController extends AbstractController {

	private Context mContext;
	private ChangePasswordView mView;
	private IChangePassword callBack;
	private String currentPassword = "";
	private String password = "";
	private String repeatPassword = "";

	public ChangePasswordController(Context context, ChangePasswordView view) {
		this.mContext = context;
		this.mView = view;
	}

	public void setCallBack(IChangePassword callBack) {
		this.callBack = callBack;
	}

	@Override
	public void initData() {
		super.initData();
		mView.etCurrentPassword.setText("");
		mView.etPassword.setText("");
		mView.etConfirmPassword.setText("");
	}

	@Override
	public void setListener() {
		super.setListener();
		mView.builder.setPositiveButton(
				mContext.getResources().getString(R.string.btn_ok_title),
				changePassword);
		mView.builder.setNegativeButton(
				mContext.getResources().getString(R.string.btn_cancel_title),
				cancelChangePassword);
		mView.alertDialog = mView.builder.create();
		mView.alertDialog.setCancelable(false);
		mView.alertDialog.setCanceledOnTouchOutside(false);
		
	}

	@Override
	public View getView() {
		return mView;
	}

	public void showDialog() {
		mView.etCurrentPassword.setText("");
		mView.etPassword.setText("");
		mView.etConfirmPassword.setText("");
		mView.alertDialog.show();
		mView.alertDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
		mView.etPassword.addTextChangedListener(passwordWatcher);
		mView.etConfirmPassword.addTextChangedListener(passwordConfirmWatcher);
	}

	public DialogInterface.OnClickListener cancelChangePassword = new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			Utilities.hideKeyboard(mContext, mView);
			dialog.dismiss();
		}
	};
	
	public DialogInterface.OnClickListener changePassword = new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			dialog.dismiss();
			Utilities.hideKeyboard(mContext, mView);
			if (callBack != null) {
				callBack.onChangePassword(currentPassword,password);
			}
		}
	};

	private TextWatcher passwordWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void afterTextChanged(Editable s) {
			checkValidPasswords();
		}
	};

	private TextWatcher passwordConfirmWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void afterTextChanged(Editable s) {
			checkValidPasswords();
		}
	};

	private void checkValidPasswords() {
		currentPassword = mView.etCurrentPassword.getText().toString().trim();
		password = mView.etPassword.getText().toString().trim();
		repeatPassword = mView.etConfirmPassword.getText().toString().trim();
		if (mView.alertDialog != null) {
			if (!TextUtils.isEmpty(currentPassword) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(repeatPassword)) {
				if (password.equals(repeatPassword)) {
					mView.alertDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
				} else {
					mView.alertDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
				}
			} else {
				mView.alertDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
			}
		}
	}
}
