package com.clockme.controllers;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.clockme.R;
import com.clockme.activities.MainActivity;
import com.clockme.interfaces.ILoading;
import com.clockme.models.CKPlaceModel;
import com.clockme.utilities.Constants;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.UtilDialog;
import com.clockme.utilities.Utilities;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public class SiteInfoController extends AbstractController implements OnClickListener, OnMapReadyCallback {

    private Context mContext;
    private SiteInfoView mView;
    private int MAP_ZOOM = 16;
    private int RADIUS = 50;
    private LoadingBeat loadingBeat;
    private CKPlaceModel mCKPlaceModel;

    public SiteInfoController(Context context, SiteInfoView view) {
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void initData() {
        super.initData();
        loadingBeat = new LoadingBeat(mContext);
        mView.prepareMap(SiteInfoController.this);
    }

    public void initDataFromBundle(Bundle bundle){
        if (bundle != null){
            mCKPlaceModel = (CKPlaceModel)bundle.getSerializable(Constants.KEY_SITE_ID);
            mView.prepareMap(SiteInfoController.this);
            setDataToView();
        }
    }

    private void loadDataWhenMapReady() {
        setMapZoom();
        drawRadius();
        setPinToMap(mCKPlaceModel.siteLocation);

    }

    private void setPinToMap(LatLng pos) {
        if (mView.googleMap != null) {
            mView.googleMap.clear();
            mView.googleMap
                    .addMarker(new com.google.android.gms.maps.model.MarkerOptions()
                            .position(pos).icon(BitmapDescriptorFactory.fromResource(R.mipmap.pin_red)));
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, MAP_ZOOM));

//            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(pos.latitude, pos.longitude));
//            markerOption.icon(BitmapDescriptorFactory.fromResource(R.mipmap.pin_red));
//            mView.googleMap.addMarker(markerOption);

        }
    }

    private void setDataToView() {

        RADIUS = (int) mCKPlaceModel.radius;
        mView.tvRadius.setText(RADIUS + " meters");
        Log.e("SITE_DETAIL", "site.getProjectId() =" + mCKPlaceModel.projectId);
        if (AddonModel.getInstance().entityPermission(Constants.ADDON_TYPE_FUEL_GAUGE)){

            if (mCKPlaceModel.projectId == 0){
                mView.etPNumber.setText("none");
            }else{
                mView.etPNumber.setText(String.valueOf(mCKPlaceModel.projectId));
            }

            mView.llPNumber.setVisibility(View.VISIBLE);
        }else{
            mView.llPNumber.setVisibility(View.GONE);
        }

        mView.etSiteName.setText(mCKPlaceModel.name);
        mView.etState.setText(mCKPlaceModel.state);
        mView.etStName.setText(mCKPlaceModel.stName);
        mView.etStNumber.setText(mCKPlaceModel.stNumber);
        mView.etStSuburb.setText(mCKPlaceModel.suburb);
        if (mCKPlaceModel.safezone) {
            mView.btnYesSafeZone.setBackgroundResource(R.drawable.border_right);
            mView.btnNoSafeZone.setBackgroundResource(R.drawable.border_left_selected);
        } else {
            mView.btnYesSafeZone.setBackgroundResource(R.drawable.border_right_selected);
            mView.btnNoSafeZone.setBackgroundResource(R.drawable.border_left);
        }

        //delete code not using blackspot buttons anymore
            /*
            if (site.getBlackspot() == 1) {
                mView.btnYesBlackpot
                        .setBackgroundResource(R.drawable.border_right);
                mView.btnNoBlackpot
                        .setBackgroundResource(R.drawable.border_left_selected);
            } else {
                mView.btnYesBlackpot
                        .setBackgroundResource(R.drawable.border_right_selected);
                mView.btnNoBlackpot
                        .setBackgroundResource(R.drawable.border_left);
            }
            */
    }

    public void setMapZoom() {
        if (RADIUS <= 300) {
            MAP_ZOOM = 16;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        } else if (RADIUS <= 700) {
            MAP_ZOOM = 15;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        } else if (RADIUS <= 1400) {
            MAP_ZOOM = 14;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        } else if (RADIUS <= 2500) {
            MAP_ZOOM = 13;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        } else if (RADIUS <= 5000) {
            MAP_ZOOM = 12;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        } else if (RADIUS <= 7000) {
            MAP_ZOOM = 11;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        } else if (RADIUS <= 10000) {
            MAP_ZOOM = 10;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        } else {
            MAP_ZOOM = 9;
            mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    mCKPlaceModel.siteLocation, MAP_ZOOM));
        }

    }

    private void drawRadius() {
        mView.googleMap.clear();
        Marker myMarker = mView.googleMap
                .addMarker(new com.google.android.gms.maps.model.MarkerOptions()
                        .position(mCKPlaceModel.siteLocation).icon(
                                BitmapDescriptorFactory.fromResource(R.mipmap.pin_red)));

        mView.googleMap
                .addCircle(new CircleOptions()
                        .center(mCKPlaceModel.siteLocation)
                        .radius(RADIUS)
                        .strokeWidth(0f)
                        .fillColor(
                                mContext.getResources().getColor(
                                        R.color.alpha_blue)));
    }

    @Override
    public View getView() {
        return mView;
    }

    ILoading iLoading = new ILoading() {

        @Override
        public void showLoading() {
            loadingBeat.showLoading();
        }

//        @Override
//        public void hideLoading() {
//
//        }
    };

    @Override
    public void setListener() {
        mView.rlRootView.setOnClickListener(this);
        mView.scrollView.setOnClickListener(this);
        mView.navView.btnLeft.setOnClickListener(this);
        mView.navView.btnRight.setOnClickListener(this);
        //mView.btnSiteNote.setOnClickListener(this);
        //mView.btnSiteLog.setOnClickListener(this);
        //mView.siteNoteView.btnSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mView.rlRootView || v == mView.scrollView) {
            Utilities.hideKeyboard(mContext, mView);
        } else if (v == mView.navView.btnLeft) {
            mView.googleMap = null;

            Utilities.hideKeyboard(mContext, mView);
            ((MainActivity) mContext).popFragment();
            //((MainActivity) mContext).switchFragment(Constants.FRAGMENT_HOME, Constants.ANIMATION_VIEW_TYPE_DISMISS, null);

        } else if (v == mView.navView.btnRight) {

            Utilities.hideKeyboard(mContext, mView);

            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.KEY_SITE_ID, mCKPlaceModel);
            ((MainActivity) mContext).swapFragment(Constants.FRAGMENT_SITE_CREATE, Constants.ANIMATION_VIEW_TYPE_FADE, bundle);

        } else if (v == mView.siteNoteView.btnSave) {
            if (TextUtils.isEmpty(mView.siteNoteView.etNote.getText())) {
                Utilities.hideKeyboard(mContext, mView);
                UtilDialog.showAlertWith1Button(mContext,
                        "",
                        "All fields must be filled",
                        mContext.getResources().getString(R.string.btn_ok_title), null);
            } else {
                Utilities.hideKeyboard(mContext, mView);
                //sentSiteNote(mView.siteNoteView.etNote.getText().toString());
                mView.siteNoteView.setVisibility(View.GONE);
                mView.navView.btnRight.setVisibility(View.GONE);
            }
        } else if (v == mView.navView.btnRight) {
            Utilities.hideKeyboard(mContext, mView);
            mView.siteNoteView.setVisibility(View.GONE);
            mView.navView.btnRight.setVisibility(View.GONE);
        }

//        else if (v == mView.btnSiteLog) {
//            MainActivity.FRAGMENT_INDEX = Constants.FRAGMENT_SITE_LOG;
//            ((MainActivity) mContext).switchFragment(Constants.FRAGMENT_SITE_LOG, Constants.ANIMATION_VIEW_TYPE_SLIDE, null);
//        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (mView.googleMap == null) {
            mView.googleMap = googleMap;
        }

        mView.createMapHolder();
        loadDataWhenMapReady();

    }

}