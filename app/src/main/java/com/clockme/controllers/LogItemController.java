package com.clockme.controllers;

import android.content.Context;
import android.view.View;

import com.clockme.helpers.GetPinHelper;
import com.clockme.utilities.DateTimeUtils;

public class LogItemController extends AbstractController {

	private Context ctx;
	private LogItemView view;
	private LogModel log;

	public LogItemController(Context context, LogItemView mLogItem,
			LogModel mLog) {
		this.ctx = context;
		this.view = mLogItem;
		this.log = mLog;
	}

	@Override
	public void initData() {
		super.initData();
		if (log.isPulse) {
			view.tvContent.setText(log.desc.trim());
		} else {
			view.tvContent.setText(log.desc.trim() + " at " + log.name.trim());
		}

		view.tvTime.setText("Time: " + DateTimeUtils.convertToDatetime(log.time));
		view.ivStatus.setImageDrawable(GetPinHelper.getInstance(ctx).getPin(log.checkinTypeId, log.status));
	}

	@Override
	public void setListener() {
		super.setListener();
	}

	@Override
	public View getView() {
		return super.getView();
	}

}
