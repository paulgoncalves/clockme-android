package com.clockme.controllers;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuListView.OnMenuItemClickListener;
import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.interfaces.ILoading;
import com.clockme.interfaces.IWebservice;
import com.clockme.models.CKCompanyModel;
import com.clockme.models.CKPersonModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.DateTimeUtils;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.UtilDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EntityController extends AbstractController implements OnClickListener {

    private Context mContext;
    private EntityView mView;
    private ArrayList<CKCompanyModel> arrEntities = new ArrayList<CKCompanyModel>();
    private ArrayList<CKCompanyModel> arrMyEntities = new ArrayList<CKCompanyModel>();
    private EntitiesListAdapter adapter;
    private EnterEntityCodeView vEnterEntityCode;
    private EnterEntityCodeController ctrlEnterEntityCode;
    private LoadingBeat loadingBeat;

    public EntityController(Context context, EntityView view) {
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void initData() {
        super.initData();

        loadingBeat = new LoadingBeat(mContext);

        vEnterEntityCode = new EnterEntityCodeView(mContext);
        ctrlEnterEntityCode = new EnterEntityCodeController(mContext,vEnterEntityCode);

        requestGetUserEntites();
        setLoggedWith();
    }

    public void setLoggedWith() {

        if (!TextUtils.isEmpty(SBApplication.getEntityModel().companyName)) {
            mView.tvLoggedWith.setText(SBApplication.getEntityModel().companyName);
        } else {
            mView.tvLoggedWith.setText(mContext.getResources().getString(R.string.tv_tap_to_select_entity_title));
        }

    }

    private void requestGetAllEntities() {

        String body = String.format(WebservicesHelper.PARAMS_ENTITY_ALL, SBApplication.getUserModel().personId, SBApplication.getUserModel().token);
        WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_ENTITY_ALL_ENTITIES, body);

    }

    private void requestJoinEntity(int entityId) {

        String body = String.format(WebservicesHelper.PARAMS_ENTITY_JOIN_ENTITY, SBApplication.getUserModel().personId, SBApplication.getUserModel().token,entityId, DateTimeUtils.getCurrentTimeStamp(),0);
        WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_ENTITY_JOIN, body);

    }

    private void requestGetUserEntites() {

        arrMyEntities.clear();

        String body = String.format(WebservicesHelper.PARAMS_ENTITY_USER_ENTITIES, SBApplication.getUserModel().personId, SBApplication.getUserModel().token);
        WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_ENTITY_USER_ENTITIES, body);

    }

    private void requestDeleteUserEntity() {

        String body = String.format(WebservicesHelper.PARAMS_ENTITY_DELETE_ENTITY, SBApplication.getUserModel().personId, SBApplication.getUserModel().token,SBApplication.getEntityModel().companyId,DateTimeUtils.getCurrentTimeStamp());
        WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_ENTITY_DELETE, body);

    }


    IWebservice IWebserviceRequest = new IWebservice() {

        @Override
        public void onComplete(JSONObject WSResponse, String WSMessage, int WSCode) {


            if (WSResponse.optInt("status") == 1){

                if(WSCode == WebservicesHelper.WSCODE_ENTITY_USER_FETCH_SUCCESS){

                    try {

                        JSONArray jsonArrEntities = WSResponse.optJSONArray("entity");

                        for (int i = 0; i < jsonArrEntities.length(); i++) {

                            CKCompanyModel entity = new CKCompanyModel().getEntityModelFromJson(jsonArrEntities.getJSONObject(i));
                            if (entity.accessEnd == 0 || DateTimeUtils.isTimelessThen(entity.accessEnd,30)){
                                arrMyEntities.add(entity);
                            }
                        }

                        //Sort entity by active


                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }

                    updateListView();
                    loadingBeat.cancelLoading();

                }else if (WSCode == WebservicesHelper.WSCODE_ENTITY_FETCH_SUCCESS){

                    try {

                        arrEntities.clear();
                        JSONArray jsonArrEntities = WSResponse.optJSONArray("entity");
                        int size = jsonArrEntities.length();
                        if (size > 0) {
                            for (int i = 0; i < size; i++) {
                                arrEntities.add(new CKCompanyModel().getEntityModelFromJson(jsonArrEntities.getJSONObject(i)));
                            }

                            ctrlEnterEntityCode.showDialog();
                        }

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }

                    loadingBeat.cancelLoading();

                }else if(WSCode == WebservicesHelper.WSCODE_ENTITY_JOIN_SUCCESS){

                    for (CKCompanyModel entity : arrEntities) {

                        if (entity.companyId == WSResponse.optInt("companyId")){
                            SBApplication.setEntityModel(entity);
                            CKCompanyModel.updateEntityInfo(mContext);

                        }

                    }

                    requestGetUserEntites();

                }else if(WSCode == WebservicesHelper.WSCODE_ENTITY_DELETE_SUCCESS){
                    requestGetUserEntites();
                }

            }else{

                UtilDialog.showAlertWith1Button(mContext, mContext.getResources().getString(R.string.warning), WSMessage, mContext.getResources().getString(R.string.btn_cancel_title), null);

            }

        }

        @Override
        public void onError(String error) {

            mView.btnTryAgain.setVisibility(View.GONE);
            mView.lvEntities.setVisibility(View.GONE);
            UtilDialog.showAlertWith1Button(mContext, null, error, mContext.getResources().getString(R.string.btn_ok_title), null);

        }

        @Override
        public void onAccessDenied(String WSMessage) {
            UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
            CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
        }
    };


    ILoading iLoading = new ILoading() {

        @Override
        public void showLoading() {
            loadingBeat.showLoading();
        }

//        @Override
//        public void hideLoading() {
//
//        }
    };

    public void onEnterEntityCode(String entityCode) {

        int entityId = 0;
        for (CKCompanyModel entity : arrEntities) {
            if (entity.code.equals(entityCode)) {
                entityId = entity.companyId;
                break;
            }
        }

        if (entityId != 0) {
            requestJoinEntity(entityId);
        } else {
            UtilDialog.showAlertWith1Button(mContext, mContext.getResources()
                    .getString(R.string.warning),mContext.getResources()
                    .getString(R.string.error_not_found_entity_code), mContext.getResources()
                    .getString(R.string.btn_ok_title), null);
        }

    }

    @Override
    public void setListener() {
        super.setListener();

        ctrlEnterEntityCode.initData();
        ctrlEnterEntityCode.setListener();
        ctrlEnterEntityCode.setCallBack(this);

        mView.navView.btnLeft.setOnClickListener(this);
        mView.navView.btnRight.setOnClickListener(this);
        mView.btnTryAgain.setOnClickListener(this);

        mView.lvEntities.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0: {
                        //entityIdToDelete = arrMyEntities.get(position).companyId;
                        requestDeleteUserEntity();
                    }
                    break;
                }
                return false;
            }
        });

        mView.lvEntities.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (position <= arrMyEntities.size()) {
                    setChoosedEntity(arrMyEntities.get(position).companyId);
					((Activity) mContext).finish();
					((Activity) mContext).overridePendingTransition(R.anim.stand_by, R.anim.out_to_bottom);
				}
            }
        });
    }

    @Override
    public View getView() {
        return mView;
    }

    @Override
    public void onClick(View v) {
        if (v == mView.navView.btnLeft) {
            ((Activity) mContext).finish();
            ((Activity) mContext).overridePendingTransition(R.anim.stand_by,R.anim.out_to_bottom);
        } else if (v == mView.navView.btnRight) {

            //Check if already fetch the site codes
            if (arrEntities.size() == 0){
                requestGetAllEntities();
            }else{
                ctrlEnterEntityCode.showDialog();
            }

        } else if (v == mView.btnTryAgain) {
            mView.btnTryAgain.setVisibility(View.GONE);
            requestGetUserEntites();
        }
    }

    private void setChoosedEntity(int entityId) {

        for (CKCompanyModel entity : arrMyEntities) {

            if (entity.companyId == entityId){

                SBApplication.setEntityModel(entity);
                CKCompanyModel.updateEntityInfo(mContext);
                break;
            }
        }
    }

    private void updateListView() {

        adapter = new EntitiesListAdapter(mContext, arrMyEntities);
        mView.lvEntities.setAdapter(adapter);
        mView.lvEntities.setVisibility(View.VISIBLE);
        adapter.setArrEntities(arrMyEntities);
        adapter.notifyDataSetChanged();
        loadingBeat.cancelLoading();

    }

}