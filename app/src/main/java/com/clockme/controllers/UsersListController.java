package com.clockme.controllers;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;

import com.clockme.activities.MainActivity;
import com.clockme.utilities.Constants;
import com.clockme.views.UsersListView;

import java.util.ArrayList;

public class UsersListController extends AbstractController implements OnClickListener {

	private Context ctx;
	private UsersListView view;
	private ArrayList<EntityUserModel> arrUsers = new ArrayList();
	private ArrayList<EntityUserModel> arrSearchedUsers = new ArrayList();
	private UsersListAdapter adapter;
	private UsersListAdapter adapterForSearching;
	
	public UsersListController(Context context, UsersListView vUsersList) {
		this.ctx = context;
		this.view = vUsersList;
	}
	
	@Override
	public void initData() {
		super.initData();
		view.etSearchMembers.setText("");
		//arrUsers = UsersListHelper.getInstance().getUsersList();
		adapter = new UsersListAdapter(ctx, arrUsers);
		view.lvUsers.setAdapter(adapter);
		
		adapterForSearching = new UsersListAdapter(ctx, arrSearchedUsers);
	}

	public void initDataFromBundle(Bundle bundle){
		arrUsers = (ArrayList<EntityUserModel>) bundle.getSerializable(Constants.KEY_CONNECTION);
	}

	@Override
	public void setListener() {
		super.setListener();
		view.navView.btnLeft.setOnClickListener(this);
		view.navView.btnRight.setOnClickListener(this);
		view.etSearchMembers.addTextChangedListener(searchUsers);
		
//		view.lvUsers.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> parent, View _view,
//					int position, long id) {
//				if (view.lvUsers.getAdapter().equals(adapter)) {
//					UsersListHelper.getInstance().setUser(arrUsers.get(position));
//				} else {
//					UsersListHelper.getInstance().setUser(arrSearchedUsers.get(position));
//				}
//
//				((MainActivity)ctx).swapFragment(Constants.FRAGMENT_USER_VIEW, null);
//			}
//		});
	}

	@Override
	public void onClick(View v) {
		if (v == view.navView.btnLeft) {
			((MainActivity) ctx).toggle();
		} else if (v == view.navView.btnRight) {
			((MainActivity) ctx).popFragment();
		}
	}
	
	private TextWatcher searchUsers = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void afterTextChanged(final Editable s) {
			if (s != null && s.length() > 0) {
				if (view.lvUsers.getAdapter().equals(adapter)) {
					view.lvUsers.setAdapter(adapterForSearching);
				}
				String keyWords = view.etSearchMembers.getText().toString().trim();
				searchLeaders(keyWords);
			} else {
				arrSearchedUsers.clear();
				view.lvUsers.setAdapter(adapter);
				adapter.notifyDataSetChanged();
			}
		}
	};

	private void searchLeaders(String keyWords) {
		int sizeOfList = arrUsers.size();
		if (sizeOfList > 0) {
			arrSearchedUsers.clear();
			for (int i = 0; i < sizeOfList; i++) {
				String name = arrUsers.get(i).firstName + " "
						+ arrUsers.get(i).lastName;
				if (name.toLowerCase().contains(keyWords.toLowerCase())) {
					arrSearchedUsers.add(arrUsers.get(i));
				}
			}
		}
		adapterForSearching.notifyDataSetChanged();
	}
}
