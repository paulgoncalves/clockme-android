package com.clockme.controllers;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.activities.MainActivity;
import com.clockme.interfaces.ILoading;
import com.clockme.interfaces.IWebservice;
import com.clockme.models.CKPersonModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.Constants;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.UtilDialog;
import com.clockme.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by AppDev on 31/08/15.
 */
public class ShareHazardController extends AbstractController implements
        View.OnClickListener,
        AdapterView.OnItemLongClickListener{

    private static String TAG = ShareHazardController.class.getSimpleName();
    private static ArrayList<EntityUserModel> sEntityUserList = new ArrayList<>();

    private Context mContext;
    private CustomAdapter mAdapter;
    private ShareHazardView mView;
    private LoadingBeat mLoadingBeat;

    private HazardModel mHazardModel;
    private EntityUserModel mEntityUserModel;

    public ShareHazardController(Context context, ShareHazardView view) {
        mContext = context;
        mView = view;
        mLoadingBeat = new LoadingBeat(mContext);
    }

    @Override
    public void initData() {
        super.initData();

        // get hazard model when fragment starts so we can use it later
        // when we make a request to share hazard
        mHazardModel = ((HazardDetailActivity)mContext).getHazardModel();

    }




    @Override
    public void setListener() {
        //mView.editButton.setOnClickListener(this);
        mView.navView.btnLeft.setOnClickListener(this);
        mView.btnAdd.setOnClickListener(this);
        mView.btnBrowse.setOnClickListener(this);
        mView.btnCancel.setOnClickListener(this);
        mView.btnSend.setOnClickListener(this);

        mView.lvShare.setOnItemLongClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick() fired!");
        if(v == mView.navView.btnLeft) {

            // todo: remove if not using on main activity
            if(mContext instanceof MainActivity) {


            } else {

                ((HazardDetailActivity)mContext).swapFragment(Constants.FRAGMENT_HAZARD_DETAIL,
                        Constants.ANIMATION_VIEW_TYPE_FADE, null);

                // clear the list
                ((HazardDetailActivity)mContext).clearList();
            }

        } else if(v == mView.btnBrowse) {
            displayEntityUserScreen();
        } else if(v == mView.btnAdd) {
            addDetails();
        } else if(v == mView.btnCancel) {

            // need to check which activity we are on
            if(mContext instanceof MainActivity) {
                //((MainActivity) mContext).popFragment();
            } else {

                // clear the list
                //sEntityUserList.clear();

                clearEntityUserList();

                clearEditText();

                ((HazardDetailActivity) mContext).swapFragment(Constants.FRAGMENT_HAZARD_DETAIL,
                        Constants.ANIMATION_VIEW_TYPE_FADE, null);

            }

        } else if(v == mView.btnSend) {

            if(!sEntityUserList.isEmpty()) {
                // make request to sever to send emails
                requestShareHazard();
            }

        }
    }

    /**
     * Long Item click listener for the list view
     * @param parent
     * @param view
     * @param position
     * @param id
     * @return
     */
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        displayContextualActionBar(position);

        return false;
    }

    private void displayContextualActionBar(final int position) {
        // create dialog as contextual actionbar
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_contextual_actionbar);

        // place dialog at the bottom of the screen
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wm = window.getAttributes();
        wm.gravity = Gravity.BOTTOM;
        wm.flags &= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wm);

        ImageButton editButton = (ImageButton)dialog.findViewById(R.id.editImageButton);
        ImageButton deleteButton = (ImageButton)dialog.findViewById(R.id.deleteImageButton);

        /**
         * Contextual ActionBar edit button click listener
         */
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // close contextual actionbar dialog
                dialog.dismiss();

                displayConfirmChangesDialog(position);
            }
        });

        /**
         * Confirm changes dialog delete button listener
         */
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!sEntityUserList.isEmpty()) {
                    sEntityUserList.remove(position);
                    updateList();
                }

                dialog.dismiss();

            }
        });

        dialog.show();
    }

    private void displayConfirmChangesDialog(final int position) {
        // create dialog to edit user details
        final Dialog confirmEditDialog = new Dialog(mContext);
        confirmEditDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmEditDialog.setContentView(R.layout.dialog_change_details);

        final EditText nameEditText = (EditText)confirmEditDialog.findViewById(R.id.etDialogName);
        final EditText emailEditText = (EditText)confirmEditDialog.findViewById(R.id.etDialogEmail);

        nameEditText.setText(sEntityUserList.get(position).firstName);
        nameEditText.setFocusable(true);

        emailEditText.setText(sEntityUserList.get(position).email);

        final Button cancelButton = (Button)confirmEditDialog.findViewById(R.id.btnCancel);
        final Button okButton = (Button)confirmEditDialog.findViewById(R.id.btnOk);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.hideKeyboard(mContext, nameEditText);
                confirmEditDialog.dismiss();
            }
        });

        /**
         * Confirm changes OK click listener
         */
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EntityUserModel model = new EntityUserModel();
                model.firstName = nameEditText.getText().toString();
                model.email = emailEditText.getText().toString();

                sEntityUserList.remove(position); // remove old item
                sEntityUserList.add(position, model); // add new item

                updateList();

                Utilities.hideKeyboard(mContext, nameEditText);

                confirmEditDialog.dismiss();
            }
        });

        // display keypad when dialog loads
        confirmEditDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                // display keyboard
                Utilities.showKeyboard(mContext, nameEditText);
            }
        });

        confirmEditDialog.show();
    }

    private void updateList() {
        mAdapter = new CustomAdapter(mContext, sEntityUserList, this);
        mView.lvShare.setAdapter(mAdapter);

    }

    private void displayEntityUserScreen() {
        //SBApplication.isDisplaySelectButton(true);

        SBApplication.setIsFromShareHazard(true);

        if(mContext instanceof MainActivity) {
            Log.d(TAG, "displayEntityUserScreen() - mContext instanceof MainActivity");
            MainActivity.FRAGMENT_INDEX = Constants.FRAGMENT_ENTITIES_USER;
            ((MainActivity) mContext).switchFragment(Constants.FRAGMENT_ENTITIES_USER,Constants
                    .ANIMATION_VIEW_TYPE_FADE,null);
        }

        if(mContext instanceof HazardDetailActivity) {
            Log.d(TAG, "displayEntityUserScreen() - mContext instanceof HazardDetailActivity");
            ((HazardDetailActivity)mContext).swapFragment(Constants.WIZARD_ENTITY_USER_SCREEN,
                    Constants.ANIMATION_VIEW_TYPE_FADE, null);
        }

    }

    // every time we add a new user to share hazard with
    // we create a new model
    private void addDetails() {
        // get email from edit text to check if it is valid email
        String email = mView.etEmail.getText().toString().trim();
        if(!Utilities.checkEmailFormat(email)) {
            mView.etEmail.setError("Email invalid - tray again");
        } else if(mView.etName.getText().toString().equals("")) {
            mView.etName.setError("Enter companyName");
        } else {

            EntityUserModel shareDetail = new EntityUserModel();
            shareDetail.firstName = mView.etName.getText().toString();
            shareDetail.email = email;

            sEntityUserList.add(shareDetail);

            updateEntityUserList();
            clearEditText();
        }
    }

    public void updateEntityUserList() {
        if(mAdapter == null) {
            mAdapter = new CustomAdapter(mContext, sEntityUserList, ShareHazardController.this);
            mView.lvShare.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void addEntityUser(EntityUserModel model) {
        sEntityUserList.add(model);
    }

    public void clearEntityUserList() {
        if(sEntityUserList != null) {
            sEntityUserList.clear();
        }
    }

    // clear edit text after user clicks add button
    public void clearEditText() {
        mView.etName.setText("");
        mView.etEmail.setText("");
    }


    /**
     * Inner class
     * List adapter for ListView in dialog
     * It handles all the click events
     * and displaying text for each row view
     * */
    private static class CustomAdapter extends ArrayAdapter<EntityUserModel> {

        private final ArrayList<EntityUserModel> mList;

        private final Context mContext;
        private final ShareHazardController mController;

        public CustomAdapter(Context context, ArrayList<EntityUserModel> list, ShareHazardController
                controller) {
            super(context, R.layout.row_share_hazard_item, list);
            mList = list;
            mContext = context;
            mController = controller;
        }


        // use ViewHolder to recycle row views
        static class ViewHolder {
            //public ImageView ivAvatar;
            public TextView tvName;
            public TextView tvEmail;

        }


        @Override
        public View getView(final int pos, View convertView, ViewGroup parent) {
            //View rowView = convertView;

            final ViewHolder viewHolder;

            // reuse views
            if(convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context
                        .LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.row_share_hazard_item, null);

                viewHolder = new ViewHolder();
                viewHolder.tvName = (TextView)convertView.findViewById(R.id.shareNameTextView);
                viewHolder.tvEmail = (TextView)convertView.findViewById(R.id.shareEmailTextView);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder)convertView.getTag();
            }

            EntityUserModel model = mList.get(pos);

            // display data in row
            viewHolder.tvName.setText(model.firstName);
            viewHolder.tvEmail.setText(model.email);  // todo: add email verification

            //WebservicesHelper.getInstance().webserviceLoadImage(viewHolder.ivAvatar, Constants
            //        .IMG_TYPE_USER, model.personId);

            //Log.i(TAG, "getView() model.firstname = " + model.companyName);
            //Log.i(TAG, "getView() model.email = " + model.email);

            return convertView;
        }
    }

    /************************** WEB REQUEST CALL ********************************/
    private void requestShareHazard() {
        JSONObject obj = new JSONObject();

        try {
            obj.put(Constants.KEY_PERSON_ID, SBApplication.getUserModel().personId);
            obj.put(Constants.KEY_TOKEN, SBApplication.getUserModel().token);
            obj.put(Constants.KEY_HAZARD_ID, mHazardModel.getHazardId());
            obj.put(Constants.KEY_SHARE, createShareArray(new JSONArray()));

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        WebservicesHelper.getInstance()
                .webserviceRequestWithJson(
                        mContext,
                        iLoading,
                        IWebserviceRequest,
                        WebservicesHelper.ROUTE_HAZARD_HAZARD_SHARE,
                        obj.toString());
    }

    private JSONArray createShareArray(JSONArray jsonArray) {

        //if(!mShareList.isEmpty()) {
            for(EntityUserModel sm : sEntityUserList) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put(Constants.KEY_HAZARD_ID, mHazardModel.getHazardId());
                    obj.put(Constants.KEY_PERSON_NAME, sm.firstName);
                    obj.put(Constants.KEY_PERSON_EMAIL, sm.email);
                    obj.put(Constants.KEY_SITE_NAME, mHazardModel.getSiteName());
                    jsonArray.put(obj); // add json object to json arrayAnswers
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        //}

        return jsonArray;
    }


    /********************** Web service call back interface ***********************/
    IWebservice IWebserviceRequest = new IWebservice() {

        @Override
        public void onComplete(JSONObject WSResponse, String WSMessage, int WSCode) {

            if (WSResponse.optInt("status") == 1){
                /** Handle response from IWebservice when save hazard success **/
                if (WSCode == WebservicesHelper.WSCODE_HAZARD_SHARE_SUCCESS) {
                    Log.i(TAG, "IWebserviceRequest - create hazard save complete");

                    // display dialog to confirm request successful
                    UtilDialog.showAlertWith1Button(
                            mContext,
                            "",
                            WSMessage,
                            mContext.getResources().getString(R.string.btn_ok_title),
                            null);

                }
            } else {
                Log.i(TAG, "IWebserviceRequest - request error");

                // display dialog to confirm request successful
                UtilDialog.showAlertWith1Button(
                        mContext,
                        "",
                        WSMessage,
                        mContext.getResources().getString(R.string.btn_ok_title),
                        null);

            }

            Log.i(TAG, "IWebserviceRequest - request complete");
            mLoadingBeat.cancelLoading();

        }

        /**
         * Handle IWebservice error
         * @param error response from server
         */
        @Override
        public void onError(String error) {
            mLoadingBeat.cancelLoading();

            // display dialog to confirm request successful
            UtilDialog.showAlertWith1Button(
                    mContext,
                    "",
                    error,
                    mContext.getResources().getString(R.string.btn_ok_title),
                    null);

        }

        @Override
        public void onAccessDenied(String WSMessage) {
            mLoadingBeat.cancelLoading();
            UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
            CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
        }
    };

    ILoading iLoading = new ILoading() {

        @Override
        public void showLoading() {
            mLoadingBeat.showLoading();
        }
    };

    /**
     * Encode string to UTF-8 format for server to process,
     * mainly to remove space character etc...
     * @param uncoded
     * @return
     */
    public String encodeString(String uncoded){
        try {
            return URLDecoder.decode(uncoded, "UTF-8");
        }catch (Exception ex){
            ex.printStackTrace();
            return "";
        }
    }
}
