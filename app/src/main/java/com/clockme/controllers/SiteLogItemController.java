package com.clockme.controllers;

import android.content.Context;
import android.view.View;

/**
 * Created by AppDev on 25/08/15.
 */
public class SiteLogItemController extends AbstractController implements View.OnClickListener{
    private static final String TAG = "SiteLogItemController";

    private Context mContext;
    private SiteLogItemView mView;
    private LogModel mLogModel;
    private SiteLogController mSiteLogController;

    public SiteLogItemController(Context context, SiteLogItemView view, LogModel logModel,
                                 SiteLogController controller) {
        this.mContext = context;
        this.mView = view;
        this.mLogModel = logModel;
        this.mSiteLogController = controller;
    }

    @Override
    public void initData() {
        super.initData();

        loadLogInfo();
        setListener();
    }

    @Override
    public void setListener() {
        super.setListener();
    }

    @Override
    public View getView() {return mView;}

    @Override
    public void onClick(View v) {

    }

    // get log info from web service
    private void loadLogInfo() {
        /*
        // todo: remove after testing
        // using constant values for user Id, need to get from Notes model
        // e.g Notes.actionId
        final String avatarUrl = String.format(
                WebservicesHelper.LOAD_USER_IMAGE_URL, "37",
                "37");

        Bitmap bmpAvatar = Utilities.loadCachedImage(avatarUrl);
        if (bmpAvatar != null) {
            mView.imgAvatar.setImageBitmap(bmpAvatar);
        } else {
            mView.imgAvatar.setImageBitmap(null);
            mView.imgAvatar.setImageResource(R.mipmap.icon_small_avatar);
            if (!TextUtils.isEmpty(avatarUrl)
                    && !avatarUrl.equalsIgnoreCase("null")) {
                ImageLoader.getInstance().displayImage(avatarUrl,
                        mView.imgAvatar, new ImageLoadingListener() {

                            @Override
                            public void onLoadingStarted(String imageUri,
                                                         View view) {
                            }

                            @Override
                            public void onLoadingFailed(String imageUri,
                                                        View view, FailReason failReason) {
                            }

                            @Override
                            public void onLoadingComplete(String imageUri,
                                                          View view, Bitmap loadedImage) {
                                if (loadedImage != null) {
                                    Utilities.cacheAvatarImage(loadedImage,
                                            avatarUrl);
                                }
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri,
                                                           View view) {
                            }
                        });
            }
        }
        */

        /*
        String tvTimeString;
        if(!TextUtils.isEmpty(mNotesModel.mDateTime())
                && Utilities.isLongNumber(mNotesModel.mDateTime())) {
            tvTimeString = DateTimeUtils.convertToDatetime(Long.parseLong(mNotesModel.mDateTime())) + " " +
                    "on " +
                    "Test: Demo Site";
        }else{
            tvTimeString = mNotesModel.mDateTime() + " on " + "Test: Demo Site";
        }
        */

        mView.tvUserName.setText("Test: User Name11");
        mView.tvTime.setText("Test: 12/05/15 12:00pm");
        mView.tvLength.setText("Test: 29:58 hours");
        mView.tvLogDesc.setText("Test: This is test log info");

        setListener();
    }
}
