package com.clockme.controllers;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;

import com.clockme.R;
import com.clockme.activities.MainActivity;
import com.clockme.adapters.LeadersListAdapter.ILeadersListAction;
import com.clockme.interfaces.ILoading;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.UtilDialog;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MyLeadersController extends AbstractController implements
		OnClickListener {

	private Context mContext;
	private MyLeadersView mView;
	private int position = -1;
	private int entityId;
	private String leaderId = "";
	private ArrayList<LeaderModel> arrMyLeaders = new ArrayList<LeaderModel>();
	private LeadersListAdapter adapter;
	private LoadingBeat loadingBeat;

	public MyLeadersController(Context context, MyLeadersView view) {
		this.mContext = context;
		this.mView = view;
	}

	@Override
	public void initData() {
		super.initData();

		loadingBeat = new LoadingBeat(mContext);
		
//		arrMyLeaders = DatabaseHelper.getSharedDatabaseHelper(mContext).getMyLeadersArray();
//		Collections.sort(arrMyLeaders, new SortLeader());
//		adapter = new LeadersListAdapter(mContext, arrMyLeaders);
//		mView.lvLeaders.setAdapter(adapter);

		if (arrMyLeaders.size() == 0) {
			requestGetMyLeaders();
		}
	}

	@Override
	public void setListener() {
		super.setListener();
		mView.navView.btnLeft.setOnClickListener(this);
		mView.btnTryAgain.setOnClickListener(this);

		adapter.setCallBack(new ILeadersListAction() {

			@Override
			public void onRemoveLeader(int _position, String _leaderId) {
				position = _position;
				leaderId = _leaderId;
				checkLeaderInforBeforeRemoving();
			}

			@Override
			public void onAddLeader(int _position, String _leaderId) {
			}
		});
	}

	@Override
	public View getView() {
		return mView;
	}

	@Override
	public void onClick(View v) {
		if (v == mView.navView.btnLeft) {
			ImageLoader.getInstance().stop();
			((MainActivity) mContext).popFragment();
		} else if (v == mView.btnTryAgain) {
			mView.btnTryAgain.setVisibility(View.GONE);
			requestGetMyLeaders();
		}
	}
	
	public class SortLeader implements Comparator<LeaderModel> {

		public int compare(LeaderModel l1, LeaderModel l2) {
			String name1 = l1.firstName +" "+l1.lastName;
			String name2 = l2.firstName +" "+l2.lastName;
			return name1.compareTo(name2);
		}

	}

	private void requestGetMyLeaders() {
		//EventObject.getInstance().getMyTeamLeaders(mContext, iLoading,iGetMyTeamLeaders);
	}

	private void checkLeaderInforBeforeRemoving() {
		LeaderModel leader = arrMyLeaders.get(position);
		if (leader.leaderId.equals(leaderId)) {
			entityId = Integer.parseInt(leader.entityId);
			requestRemoveTeamLeader();
		}
	}

	private void requestRemoveTeamLeader() {
		//EventObject.getInstance().removeTeamLeader(mContext, iLoading,iRemoveTeamLeader, companyId, leaderId);
	}

	ILoading iLoading = new ILoading() {

		@Override
		public void showLoading() {
			loadingBeat.showLoading();
		}

//		@Override
//		public void hideLoading() {
//
//		}
	};

	IGetMyTeamLeaders iGetMyTeamLeaders = new IGetMyTeamLeaders() {

		@Override
		public void onError(String error) {
			UtilDialog.showAlertWith1Button(mContext, null, error, mContext
					.getResources().getString(R.string.btn_cancel_title), null);
			if (arrMyLeaders.size() == 0) {
				mView.btnTryAgain.setVisibility(View.VISIBLE);
				mView.lvLeaders.setVisibility(View.GONE);
			}
			loadingBeat.cancelLoading();
		}

		@Override
		public void onComplete(boolean isSuccessful, String message,
				ArrayList<LeaderModel> _arrLeaders) {
			if (isSuccessful) {
				if (_arrLeaders.size() > 0) {
					mView.btnTryAgain.setVisibility(View.GONE);
					mView.lvLeaders.setVisibility(View.VISIBLE);
					Collections.sort(_arrLeaders, new SortLeader());
					arrMyLeaders.clear();
					arrMyLeaders.addAll(_arrLeaders);
					adapter.notifyDataSetChanged();
					new StoreMyLeaders(_arrLeaders).execute();
				} else {
					if (arrMyLeaders.size() == 0) {
						mView.btnTryAgain.setVisibility(View.VISIBLE);
						mView.lvLeaders.setVisibility(View.GONE);
					}
				}
			} else {
				if (message.contains("have no team")) {
					mView.btnTryAgain.setVisibility(View.GONE);
					mView.lvLeaders.setVisibility(View.GONE);
				} else {
					if (arrMyLeaders.size() == 0) {
						mView.btnTryAgain.setVisibility(View.VISIBLE);
						mView.lvLeaders.setVisibility(View.GONE);
						UtilDialog.showAlertWith1Button(
								mContext,
								null,
								mContext.getResources().getString(
										R.string.try_again),
								mContext.getResources().getString(
										R.string.btn_ok_title), null);
					}
				}
			}

			loadingBeat.cancelLoading();
		}
	};

	IRemoveTeamLeader iRemoveTeamLeader = new IRemoveTeamLeader() {

		@Override
		public void onError(String error) {
			UtilDialog.showAlertWith1Button(mContext, null, mContext
					.getResources().getString(R.string.try_again), mContext
					.getResources().getString(R.string.btn_cancel_title), null);
			loadingBeat.cancelLoading();
		}

		@Override
		public void onComplete(boolean isSuccessful, String message) {
			if (isSuccessful) {
//				DatabaseHelper.getSharedDatabaseHelper(mContext)
//						.deleteMyLeader(companyId, leaderId);
//				arrMyLeaders.remove(position);
//				adapter.notifyDataSetChanged();
			} else {
				UtilDialog.showAlertWith1Button(mContext, null, mContext
						.getResources().getString(R.string.try_again), mContext
						.getResources().getString(R.string.btn_cancel_title), null);
			}
			loadingBeat.cancelLoading();
		}
	};

	final class StoreMyLeaders extends AsyncTask<Void, Void, Void> {

		private ArrayList<LeaderModel> arrLeaders = new ArrayList<LeaderModel>();

		public StoreMyLeaders(ArrayList<LeaderModel> _arrLeaders) {
			arrLeaders.addAll(_arrLeaders);
		}

		@Override
		protected Void doInBackground(Void... params) {
//			DatabaseHelper.getSharedDatabaseHelper(mContext).deleteMyLeaders();
//			for (LeaderModel leader : arrLeaders) {
//				ContentValues cv = DatabaseHelper.getSharedDatabaseHelper(
//						mContext).setMyLeaderValues(leader);
//				DatabaseHelper.getSharedDatabaseHelper(mContext)
//						.insertMyLeader(cv);
//			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}
	}
}