package com.clockme.controllers;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.activities.MainActivity;
import com.clockme.helpers.GetPinHelper;
import com.clockme.interfaces.ILoading;
import com.clockme.interfaces.IWebservice;
import com.clockme.models.CKPersonModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.Constants;
import com.clockme.utilities.DateTimeUtils;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.UtilDialog;
import com.clockme.views.MarkerView;
import com.clockme.views.MyTeamView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MyTeamController extends AbstractController implements OnClickListener {

	private Context mContext;
	private MyTeamView mView;
	private LoadingBeat loadingBeat;
	private ArrayList<EntityUserModel> arrUsers = new ArrayList();
	private HashMap<Marker, EntityUserModel> arrayMyTeam = new HashMap<>();

	public MyTeamController(Context context, MyTeamView view) {
		this.mContext = context;
		this.mView = view;
		loadingBeat = new LoadingBeat(mContext);
	}

	@Override
	public void initData() {
		super.initData();

		//Only load my team if empty arrayAnswers
		if (arrUsers.size() == 0){
			requestGetMyTeam();
		}

	}

	/**
	 * Request join site using interface call back
	 * **/
	private void requestGetMyTeam (){

		arrUsers.clear();

		String body = String.format(WebservicesHelper.PARAMS_MY_TEAM,SBApplication.getUserModel().personId,SBApplication.getUserModel().token,
				SBApplication.getEntityModel().companyId,SBApplication.getUserModel().accessLevel);
		WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_MY_TEAM, body);

	};

	IWebservice IWebserviceRequest = new IWebservice() {

		@Override
		public void onComplete(JSONObject WSResponse, String WSMessage, int WSCode) {

			if (WSResponse.optInt("status") == 1) {

				//Site retrieved
				if (WSCode == WebservicesHelper.WSCODE_MYTEAM_SUCCESS) {

					try {
						JSONArray temp = WSResponse.getJSONArray("team");
						for (int i = 0; i < temp.length(); i++){
							arrUsers.add(new EntityUserModel().getMyTeamFromJson(temp.getJSONObject(i)));
						}
					}catch (JSONException ex){
						ex.printStackTrace();
					}

					if (arrUsers.size() > 0) {
						mView.navView.btnRight.setEnabled(true);
						showMyTeam();
					} else {
						mView.navView.btnRight.setEnabled(false);
					}

				}else{
					UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
				}
			}

			loadingBeat.cancelLoading();
		}

		@Override
		public void onError(String error) {
			UtilDialog.showAlertWith1Button(mContext, "", error, mContext.getResources().getString(R.string.ok), null);
			Log.e("WS error =>", error);
			loadingBeat.cancelLoading();
		}

		@Override
		public void onAccessDenied(String WSMessage) {
			loadingBeat.cancelLoading();
			UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
			CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
		}
	};


	ILoading iLoading = new ILoading() {

		@Override
		public void showLoading() {
			loadingBeat.showLoading();
		}
	};

	@Override
	public void setListener() {
		super.setListener();
		mView.navView.btnLeft.setOnClickListener(this);
		mView.navView.btnRight.setOnClickListener(this);
		mView.btnCurrentLocation.setOnClickListener(this);

		mView.googleMap.setInfoWindowAdapter(new InfoWindowAdapter() {
			MarkerView vMarker = null;

			@Override
			public View getInfoWindow(Marker arg0) {
				EntityUserModel user = arrayMyTeam.get(arg0);
				if (user != null) {
					vMarker = new MarkerView(mContext);
					vMarker.tvSiteName.setText(user.firstName+" "+user.lastName);
					vMarker.tvDistance.setText(user.firstName +" at " + DateTimeUtils.convertToDatetime(user.time));
				}
				return vMarker;
			}

			@Override
			public View getInfoContents(Marker arg0) {
				return null;
			}
		});

		mView.googleMap
				.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

					@Override
					public void onInfoWindowClick(Marker arg0) {
						//UsersListHelper.getInstance().setUser(arrayMyTeam.get(arg0));
						Bundle bundle = new Bundle();
						bundle.putSerializable(Constants.KEY_CONNECTION, arrayMyTeam.get(arg0));
						((MainActivity) mContext).switchFragment(Constants.FRAGMENT_USER_VIEW,Constants.ANIMATION_VIEW_TYPE_SLIDE,bundle);
					}
				});
	}

	@Override
	public View getView() {
		return mView;
	}

	@Override
	public void onClick(View v) {
		if (v == mView.navView.btnLeft) {
			((MainActivity) mContext).toggle();
		} else if (v == mView.navView.btnRight) {
			Bundle bundle = new Bundle();
			bundle.putSerializable(Constants.KEY_CONNECTION,arrUsers);
			((MainActivity) mContext).switchFragment(Constants.FRAGMENT_USER_LIST,Constants.ANIMATION_VIEW_TYPE_SLIDE,bundle);
		} else if (v == mView.btnCurrentLocation) {

			//Request new data on button refresh pressed
			requestGetMyTeam();
		}
	}

	private void showMyTeam() {

		mView.googleMap.clear();

		//int size = ;
		LatLngBounds.Builder builder = new LatLngBounds.Builder();
		for (int i = 0; i < arrUsers.size(); i++) {

			try {

				EntityUserModel user = arrUsers.get(i);

				if (DateTimeUtils.isTimelessThen(user.time, 1)) {
					int type = user.checkinTypeId;
					int status = user.status;
					int resourceIdPin = GetPinHelper.getInstance(mContext).getPinResourceId(type, status);
					MarkerOptions markerOption = new MarkerOptions().position(user.coordinate);
					markerOption.icon(BitmapDescriptorFactory.fromResource(resourceIdPin));
					Marker marker = mView.googleMap.addMarker(markerOption);
					arrayMyTeam.put(marker, user);
					builder.include(marker.getPosition());
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}

		LatLngBounds bounds = builder.build();
		CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 0);
		mView.googleMap.animateCamera(cu);
		//mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(SBApplication.getUserModel().location, 17));

	}

}
