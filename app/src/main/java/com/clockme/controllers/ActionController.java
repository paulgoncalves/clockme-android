package com.clockme.controllers;

import android.content.Context;

import com.clockme.SBApplication;
import com.clockme.models.CKPersonModel;
import com.clockme.models.CKPlaceModel;
import com.clockme.utilities.Constants;
import com.clockme.utilities.DateTimeUtils;
import com.clockme.utilities.Utilities;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by clockme on 17/09/15.
 */
public class ActionController {

    private static ActionController instance = null;

    public static ActionController getInstance() {
        if (instance == null) {
            instance = new ActionController();
        }
        return instance;
    }

    public ActionController(){

    }

    //User action methods
    public void UserCheckinSuccess(Context context, JSONObject WSResponse){

        SBApplication.getSiteModel().checkinId = Utilities.getIntValue(WSResponse, "checkinId");
        SBApplication.getSiteModel().reportInId = Utilities.getIntValue(WSResponse, "reportinId");

        try {
            JSONArray arrayEmergency = WSResponse.getJSONArray("emergency");
            for (int i = 0; i < arrayEmergency.length(); i++) {
                SBApplication.getSiteModel().arrayEmergency.add(new EmergencyModel().getEmergencyModel(arrayEmergency.getJSONObject(i)));
            }
        } catch (JSONException exx) {
            exx.printStackTrace();
        }

        try {
            JSONArray arrayLeaders = WSResponse.getJSONArray("leaders");
            for (int i = 0; i < arrayLeaders.length(); i++) {
                SBApplication.getSiteModel().arrayLeaders.add(new EmergencyModel().getEmergencyModel(arrayLeaders.getJSONObject(i)));
            }
        } catch (JSONException exx) {
            exx.printStackTrace();
        }

        try {
            JSONArray arrayProject = WSResponse.getJSONArray("projectStatus");
            for (int i = 0; i < arrayProject.length(); i++) {
                SBApplication.getSiteModel().arrayProjectStatus.add(new ProjectModel().getProjectModelFromJson(arrayProject.getJSONObject(i)));
            }
                    } catch (JSONException exx) {
            exx.printStackTrace();
        }

        SBApplication.getSiteModel().arrayHazard = HazardModel.getHazardListFromJson(WSResponse);


        //Check if need to show the post checkin view
        if (SBApplication.getSiteModel().arrayProjectStatus != null){
            if (AddonModel.getInstance().entityPermission(Constants.ADDON_TYPE_HAZARD_REPORT)) {
                SBApplication.getSiteModel().showProjectStatus = true;
            }else{
                SBApplication.getSiteModel().showProjectStatus = false;
            }
        }else{
            SBApplication.getSiteModel().showProjectStatus = false;
        }

        if (AddonModel.getInstance().entityPermission(Constants.ADDON_TYPE_HAZARD_REPORT)) {
            if (SBApplication.getSiteModel().arrayHazard != null){
                SBApplication.getSiteModel().showSiteHazard= true;
            }else{
                SBApplication.getSiteModel().showSiteHazard= false;
            }
        }else{
            SBApplication.getSiteModel().showSiteHazard= false;
        }

        //Build siteModel object
        SBApplication.getSiteModel().isCheckin = true;
        SBApplication.getSiteModel().checkinTime = DateTimeUtils.getCurrentTimeStamp();
        SBApplication.getSiteModel().usedTime = DateTimeUtils.getCurrentTimeStamp();

        //Save to memory
        CKPlaceModel.getInstance().updateSiteInfo(context);

    }

    public void UserReportinSuccess(Context context, JSONObject WSResponse){

        SBApplication.getSiteModel().usedTime = DateTimeUtils.getCurrentTimeStamp();

        try {
            SBApplication.getSiteModel().reportInId = WSResponse.getInt("reportinId");
        }catch (JSONException ex){
            ex.printStackTrace();
        }

        //Safe to memory
        CKPlaceModel.getInstance().updateSiteInfo(context);

    }

    public void UserCheckoutSuccess(Context context){

        //Remove site model detail
        SBApplication.getSiteModel().checkinId = 0;
        SBApplication.getSiteModel().checkingCategoryId = 0;
        SBApplication.getSiteModel().name = "";
        SBApplication.getSiteModel().reportInId = 0;
        SBApplication.getSiteModel().checkinId = 0;
        SBApplication.getSiteModel().isCheckin = false;
        SBApplication.getSiteModel().isOffsite = false;
        SBApplication.getSiteModel().siteId = 0;
        SBApplication.getSiteModel().usedTime = 0;
        SBApplication.getSiteModel().radius = 0;
        SBApplication.getSiteModel().siteLocation = new LatLng(0,0);

        //Remove user model detail
        SBApplication.getUserModel().session = false;
        SBApplication.getUserModel().token = "";

        //Safe to memory
        CKPlaceModel.getInstance().updateSiteInfo(context);
        CKPersonModel.getInstance().updateUserInfo(context);

    }


    public void scheduleLocalNotificationWithInterval(double interval, String alert, Boolean defaultSound, Boolean action){


    }

    public void scheduleLocalNotificationInteractiveWithInterval(double interval ,String alert, String option, String notificationType, Boolean defaultSound){


    }


}
