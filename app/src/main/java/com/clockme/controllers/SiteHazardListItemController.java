package com.clockme.controllers;

import android.content.Context;
import android.view.View;

/**
 * Created by AppDev on 25/08/15.
 */
public class SiteHazardListItemController extends AbstractController implements View.OnClickListener{
    private static final String TAG = "SiteHazardListItemController";

    private Context mContext;
    private SiteHazardListItemView mView;
    private HazardModel mHazardModel;
    private HazardController mHazardListController;


    public SiteHazardListItemController(Context context, SiteHazardListItemView view, HazardModel hazardModel, HazardController controller) {
        this.mContext = context;
        this.mView = view;
        this.mHazardModel = hazardModel;
        this.mHazardListController = controller;
    }

    @Override
    public void initData() {
        super.initData();

        loadHazardInfo();
        setListener();
    }

    @Override
    public void setListener() {
        super.setListener();
    }

    @Override
    public View getView() {return mView;}

    @Override
    public void onClick(View v) {

    }

    private void loadHazardInfo() {
        // todo: remove test data
        mView.tvSiteName.setText("Test: Hazard Site Name");
        mView.tvHazardTitle.setText("Test: Hazard Title");
        mView.tvLocation.setText("Test: Hazard Location");
        mView.tvHazardDesc.setText("Test: Hazard Description");

        setListener();
    }
}
