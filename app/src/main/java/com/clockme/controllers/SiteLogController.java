package com.clockme.controllers;

import android.content.Context;
import android.view.View;

import com.clockme.R;
import com.clockme.activities.MainActivity;
import com.clockme.adapters.SiteLogListAdapter;
import com.clockme.interfaces.ILoading;
import com.clockme.models.CKPlaceModel;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.UtilDialog;
import com.clockme.utilities.Utilities;

import java.util.ArrayList;

/**
 * Created by antkingmacmini1 on 5/13/15.
 */
public class SiteLogController extends AbstractController implements View.OnClickListener{
    private static final String TAG = "SiteLogController";
    private static ArrayList<ConnectionModel> mUserList;
    private static ArrayList<LogModel> mLogList;

    private Context mContext;
    private LoadingBeat mLoadingBeat;
    private CKPlaceModel mCKPlaceModel;
    private SiteLogView mView;
    private SiteLogListAdapter mSiteLogListAdapter;


    public SiteLogController(Context context, SiteLogView view) {
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void initData() {
        super.initData();
        mLoadingBeat = new LoadingBeat(mContext);

        // todo: remove test code!!!
        // create test data to display list
        initListAdapter();

        mLogList = new ArrayList<>();
//        for(int i = 0; i < 10; i++) {
//            LogModel model = new LogModel();
//            model.personId = Integer.toString(i);
//            model.desc = "This is a test description";
//
//            mLogList.add(model);
//        }

        if(mLogList != null) {
            mSiteLogListAdapter.setList(mLogList);
        }



        /* original code from previous developer
        new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                String site_not_checkout = SharePref.getSiteNotCheckout(mContext);
                try {
                    JSONObject site = new JSONObject(site_not_checkout);

                    mCKPlaceModel = new SiteItemParser(site).siteModel;

                    getSiteLog();
                    getEntityUserData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }.start();
        */
    }

    private void initListAdapter() {
        mSiteLogListAdapter = new SiteLogListAdapter(mContext, mLogList, this);
        mView.lvAllSiteLogs.setAdapter(mSiteLogListAdapter);
    }

    @Override
    public void setListener() {


        /*
        mView.navView.btnLeftText.setOnClickListener(this);
        mView.navView.btnRight.setOnClickListener(this);

        mView.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mSiteLogListAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mSiteLogListAdapter.getFilter().filter(newText);
                return false;
            }
        });

        mView.lvAllSiteLogs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(mContext, "View User profile", Toast.LENGTH_SHORT).show();
                CKPersonModel aUser = new CKPersonModel();
                ConnectionModel mModel = null;
                CKPersonModel userInfo = UserInfoHelper.getInstance().getUserInfo(mContext);

                for (ConnectionModel obj : mUserList) {
                    if (obj.getUserId().equals(mSiteLogListAdapter.getLogList().get(position).personId)) {
                        aUser.getDataFromConnectionObject(obj);
                        mModel = obj;
                        break;
                    }
                }

                if (mModel == null) {
                    aUser = userInfo;
                    UsersListHelper.getInstance().setUser(aUser);
                    ((MainActivity) mContext).swapFragment(Constants.FRAGMENT_USER_VIEW, null);
                    return;
                }

                UserProfileModel.set_connectionObj(mModel);

                String strAction = mModel.getAction();
                if (strAction.equals(ConnectionModel.ADD)) {
                    UserProfileModel.isConnected = "0";
                    UserProfileModel.isPendingConnection = "0";
                } else if (strAction.equals(ConnectionModel.PENDING)) {
                    //User can change connection status here
                    UserProfileModel.isConnected = "0";
                    UserProfileModel.isPendingConnection = "1";

                } else if (strAction.equals(ConnectionModel.REMOVE)) {
                    //will show Lead me, Follow here. Alway show Connections. Member will not show status only show connections
                    UserProfileModel.isConnected = "1";
                    UserProfileModel.isPendingConnection = "1";
                    CKPersonModel currentUser = UserInfoHelper.getInstance().getUserInfo(mContext);
                    if (mModel.arrConnDetail.size() > 0) {
                        for (ConnectionDetailModel connDetail : mModel.arrConnDetail) {
                            if (connDetail.getLeaderId().equals(currentUser.personId)) {
                                // follow me
                                UserProfileModel.connectionStatus = Constants.GROUP_FOLLOWER;
                                break;
                            }

                            if (connDetail.getFollowerId().equals(currentUser.personId)) {
                                // Lead me
                                UserProfileModel.connectionStatus = Constants.CONNECTION_TYPE_LEADER;
                                break;
                            }
                        }

                    }
                }

                UsersListHelper.getInstance().setUser(aUser);
                ((MainActivity) mContext).swapFragment(Constants.FRAGMENT_USER_VIEW, null);
            }
        });
        */
    }

    @Override
    public void onClick(View v) {
        if (v == mView.navView.btnLeftText) {
            backToSiteDetail();
        } else if (v == mView.navView.btnRight) {
            getSiteLog();
        }
    }

    @Override
    public View getView() {
        return mView;
    }

    private void backToSiteDetail() {
        Utilities.hideKeyboard(mContext, mView);
        ((MainActivity) mContext).popFragment();
    }

    ILoading iLoading = new ILoading() {

        @Override
        public void showLoading() {
            mLoadingBeat.showLoading();
        }

//        @Override
//        public void hideLoading() {
//
//        }
    };

    public void getSiteLog() {
        ISiteLog iSiteLog = new ISiteLog() {
            @Override
            public void onComplete(ArrayList<LogModel> list) {
                mLoadingBeat.cancelLoading();

                //mSiteLogListAdapter = new SiteLogListAdapter(mContext, list);
                //mView.lvAllSiteLogs.setAdapter(mSiteLogListAdapter);
            }

            @Override
            public void onError(String error) {

                mLoadingBeat.cancelLoading();
            }
        };

        //WebservicesHelper.getInstance().siteLog(mContext, iLoading, iSiteLog, mCKPlaceModel);
    }

    public void getEntityUserData() {
        IGetEntityUsers iGetEntityUsers = new IGetEntityUsers() {
            @Override
            public void onComplete(ArrayList<ConnectionModel> users) {
                mLoadingBeat.cancelLoading();
                mUserList = users;
            }

            @Override
            public void onError(String error) {
                mLoadingBeat.cancelLoading();
                UtilDialog.showAlertWith1Button(
                        mContext,
                        null,
                        error,
                        mContext.getResources().getString(
                                R.string.btn_cancel_title), null);
            }
        };

        //EntityUsersModel.loadEntityUser(mContext, iGetEntityUsers, iLoading);
    }

}
