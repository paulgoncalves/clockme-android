package com.clockme.controllers;

import android.content.Context;

import com.clockme.helpers.GetPinHelper;
import com.clockme.utilities.DateTimeUtils;
import com.clockme.views.UserItemView;

public class UserItemController extends AbstractController {

	private Context ctx;
	private UserItemView view;
	private EntityUserModel user;

	public UserItemController(Context context, UserItemView vUserItem,EntityUserModel mUser) {
		this.ctx = context;
		this.view = vUserItem;
		this.user = mUser;
	}

	@Override
	public void initData() {
		super.initData();

		try{

			view.tvUser.setText(user.firstName + " " + user.lastName);
			view.tvSite.setText("Site: " + user.siteName.trim());
			view.tvTime.setText("Time: " + DateTimeUtils.convertToDatetime(user.time));

			int type = user.checkinTypeId;
			int status = user.status;
			view.ivStatus.setImageDrawable(GetPinHelper.getInstance(ctx).getPin(type, status));

		}catch (Exception ex){
			ex.printStackTrace();
		}

	}

	@Override
	public void setListener() {
		super.setListener();
	}

}
