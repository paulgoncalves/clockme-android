package com.clockme.controllers;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.clockme.activities.MainActivity;
import com.clockme.utilities.Constants;

/**
 * Created by AppDev on 24/08/15.
 */
public class SiteOptionsController extends AbstractController implements View.OnClickListener {

    private Context mContext;
    private SiteOptionsView mView;

    public SiteOptionsController(Context context, SiteOptionsView view) {
        mContext = context;
        mView = view;
    }

    @Override
    public void initData() {
        super.initData();
    }

    @Override
    public void setListener() {
        // listeners for Relative layout
        mView.rlSiteInfo.setOnClickListener(this);
        mView.rlSiteNote.setOnClickListener(this);
        mView.rlSiteLog.setOnClickListener(this);
        mView.rlSiteHazard.setOnClickListener(this);

        mView.imgBtnSiteInfo.setOnClickListener(this);
        mView.imgBtnSiteNotes.setOnClickListener(this);
        mView.imgBtnSiteLog.setOnClickListener(this);
        mView.imgBtnSiteHazard.setOnClickListener(this);

        mView.btnClose.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v == mView.btnClose) {
            ((MainActivity) mContext).swapFragment(Constants.FRAGMENT_HOME,Constants
                    .ANIMATION_VIEW_TYPE_FADE,null);
        } else if(v == mView.rlSiteInfo || v == mView.imgBtnSiteInfo) {
            ((MainActivity) mContext).swapFragment(Constants.FRAGMENT_SITE_DETAIL,Constants
                    .ANIMATION_VIEW_TYPE_FADE,null);
        } else if(v == mView.rlSiteNote || v == mView.imgBtnSiteNotes) {
            ((MainActivity) mContext).swapFragment(Constants.FRAGMENT_SITE_NOTE,Constants
                    .ANIMATION_VIEW_TYPE_FADE,null);
        } else if(v == mView.rlSiteLog || v == mView.imgBtnSiteLog) {
            ((MainActivity) mContext).swapFragment(Constants.FRAGMENT_SITE_LOG,Constants
                    .ANIMATION_VIEW_TYPE_FADE,null);
        } else if(v == mView.rlSiteHazard || v == mView.imgBtnSiteHazard) {
            // start new activity
            Intent intent = new Intent(mContext, HazardActivity.class);
            mContext.startActivity(intent);
        }
    }

}
