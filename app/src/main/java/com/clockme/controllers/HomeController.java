package com.clockme.controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.activities.MainActivity;
import com.clockme.adapters.SiteListAdapter;
import com.clockme.interfaces.ILoading;
import com.clockme.interfaces.IWebservice;
import com.clockme.models.CKPersonModel;
import com.clockme.models.CKPlaceModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.Constants;
import com.clockme.utilities.DateTimeUtils;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.UtilDialog;
import com.clockme.utilities.Utilities;
import com.clockme.views.HomeView;
import com.clockme.views.MarkerView;
import com.clockme.views.TopPopupView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class HomeController extends AbstractController implements OnClickListener{

    private Context mContext;
    private HomeView mView;
    private JoinSiteView vJoinSiteView;
    private JoinSiteController ctrlJoinSite;
    private Handler mHandler;
    private Marker currentMarker = null, createSiteMarker = null;
    private HashMap<Marker, CKPlaceModel> listMarkerSite = new HashMap<Marker, CKPlaceModel>();
    private ArrayList<CKPlaceModel> listSite = new ArrayList<CKPlaceModel>();
    private LatLng createSitePosition;
    int ZOOM_CURRENT = 13, ZOOM_CHECK_IN = 16;
    private LoadingBeat loadingBeat;
    private Animation animShowSafeSite, animHideSafeSite;
    private AlertDialog alertUserOutSite;
    private int helpStep;

    public HomeController(Context context, HomeView view) {
        this.mContext = context;
        this.mView = view;
        mHandler = new Handler();
        mView.helpView.setController(this);

        vJoinSiteView = new JoinSiteView(mContext);
        ctrlJoinSite = new JoinSiteController(mContext, vJoinSiteView);

        loadingBeat = new LoadingBeat(mContext);
        animShowSafeSite = AnimationUtils.loadAnimation(mContext,R.anim.in_from_bottom);
        animHideSafeSite = AnimationUtils.loadAnimation(mContext,R.anim.out_to_bottom);
        mView.createMapHolder();
        currentMarker = drawMarker(mView.googleMap,SBApplication.getUserModel().location);

    }

    @Override
    public void initData() {
        super.initData();
        resumeController();
    }

    public void resumeController() {

        if (SBApplication.getSiteModel().isCheckin) {

            CheckinSuccess();

            //remove call to fetch site
            mHandler.removeCallbacks(autoRefreshSiteThread);

            //Get user detail
            if (!autoRefreshHomeThread.isAlive()){
                ((Activity) mContext).runOnUiThread(autoRefreshHomeThread);
            }

        } else {

            if (SBApplication.getEntityModel().companyId != 0 ){

                //remove call to fetch site
                mHandler.removeCallbacks(autoRefreshHomeThread);

                if (!autoRefreshSiteThread.isAlive()){
                    ((Activity) mContext).runOnUiThread(autoRefreshSiteThread);
                }
            }else{
                UtilDialog.showAlertWith1Button(mContext, null,
                        mContext.getResources().getString(R.string.msg_entity_no_entity_joined),
                        mContext.getResources().getString(R.string.btn_ok_title), null);
                ((MainActivity) mContext).toggle();

                // TODO: 12/10/15 Flash the menu where show join entity

            }

        }
    }

    /**
     * get data from server
     */
    public void requestFetchSite() {

        String body = String.format(WebservicesHelper.PARAM_SITE_BY_ENTITY,SBApplication.getUserModel().personId,SBApplication.getUserModel().token, SBApplication.getEntityModel().companyId,
                DateTimeUtils.getCurrentTimeStamp(),SBApplication.getUserModel().location.latitude, SBApplication.getUserModel().location.longitude);
        WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_SITE_BY_ENTITY, body);
    }

    /**
     * Check int
     */
    public void requestCheckIn(final int categoryId) {

        int success = 1;
        String body = String.format(WebservicesHelper.PARAMS_ACTION_CHECK_IN,SBApplication.getUserModel().personId,SBApplication.getUserModel().token,categoryId,SBApplication.getSiteModel().siteId,
                DateTimeUtils.getCurrentTimeStamp(), SBApplication.getUserModel().timezone, SBApplication.getUserModel().location.latitude, SBApplication.getUserModel().location.longitude,
                SBApplication.getEntityModel().companyId, Utilities.getBatteryLevel(mContext),success);

        WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_ACTION_CHECKIN, body);

    }

    /**
     * Check out
     */
    public void requestCheckOut(int done) {

        //Check if user is on site radius
        int onRadius = 0;
        double distanceFromSite = Utilities.calculateDistance(SBApplication.getSiteModel().siteLocation,SBApplication.getUserModel().location);
        if (distanceFromSite <= SBApplication.getSiteModel().radius + 0.25) {
            onRadius = 1;
        }

        String body = String.format(WebservicesHelper.PARAMS_ACTION_CHECKOUT, SBApplication.getUserModel().personId,SBApplication.getUserModel().token,
                SBApplication.getSiteModel().siteId,DateTimeUtils.getCurrentTimeStamp(),SBApplication.getUserModel().timezone,
                SBApplication.getUserModel().location.latitude, SBApplication.getUserModel().location.longitude, SBApplication.getEntityModel().companyId,
                Utilities.getBatteryLevel(mContext),done,SBApplication.getSiteModel().checkinId,onRadius);

        WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_ACTION_CHECKOUT, body);

    }

    /**
     * report in
     */
    public void requestReportIn() {

        String body = String.format(WebservicesHelper.PARAMS_ACTION_REPORTIN, SBApplication.getUserModel().personId,SBApplication.getUserModel().token,SBApplication.getSiteModel().siteId,
                SBApplication.getSiteModel().checkinId, DateTimeUtils.getCurrentTimeStamp(),SBApplication.getUserModel().timezone,
                SBApplication.getUserModel().location.latitude, SBApplication.getUserModel().location.longitude,SBApplication.getEntityModel().companyId, Utilities.getBatteryLevel(mContext));
        WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_ACTION_REPORTIN, body);
    }

    /**
     * send a pulse
     */
    private void requestSendPulse(String pulseType) {

        String body = String.format(WebservicesHelper.PARAMS_ACTION_PULSE, SBApplication.getUserModel().personId,SBApplication.getUserModel().token, DateTimeUtils.getCurrentTimeStamp(),SBApplication.getUserModel().timezone,
                SBApplication.getUserModel().location.latitude, SBApplication.getUserModel().location.longitude,pulseType,SBApplication.getEntityModel().companyId, Utilities.getBatteryLevel(mContext));

        WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_ACTION_PULSE, body);

    }

    /**
     * Request site question for site
     * **/
    public void requestSiteQuestion(final CKPlaceModel CKPlaceModel) {

        String body = String.format(WebservicesHelper.PARAMS_SITE_QUESTION,SBApplication.getUserModel().personId,SBApplication.getUserModel().token, CKPlaceModel.siteId, SBApplication.getEntityModel().companyId);
        WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_SITE_QUESTION, body);

    }

    /**
     * Request site question for site
     * **/
    public void requestUserStatus() {

        String body = String.format(WebservicesHelper.PARAMS_USER_STATUS,SBApplication.getUserModel().personId,SBApplication.getUserModel().token);
        WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_USER_STATUS, body);

    }


    /**
     * Request join site using interface call back
     * **/
    IJoinSite iJoinSite = new IJoinSite() {

        @Override
        public void onEnterCode(String siteCode) {

            String body = String.format(WebservicesHelper.PARAMS_SITE_JOIN,SBApplication.getUserModel().personId,SBApplication.getUserModel().token,SBApplication.getEntityModel().companyId,
                    siteCode,DateTimeUtils.getCurrentTimeStamp());
            WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_SITE_JOIN, body);

        }
    };


    IWebservice IWebserviceRequest = new IWebservice() {

        @Override
        public void onComplete(JSONObject WSResponse, String WSMessage, int WSCode) {

            if (WSResponse.optInt("status") == 1){

                //Site retrieved
                if (WSCode == WebservicesHelper.WSCODE_SITE_FETCH_SUCCESS) {

                    //Set banner message
                    showStatusBar(WSMessage);

                    //1. Clear old marker
                    clearMarkerAndListSite();
                    //Reload site with new fetched data
                    loadSiteMapNearMe(WSResponse);

                    mView.pbGetLocation.setVisibility(View.GONE);

                    hideStatusBar(WSMessage);
                    loadingBeat.cancelLoading();

                } else if (WSCode == WebservicesHelper.WSCODE_ACTION_CHECKIN_SUCCESS) {

                    //Build siteModel object
                    ActionController.getInstance().UserCheckinSuccess(mContext, WSResponse);

                    //Handle the checkin orders and loading process
                    HandleCheckin(WSResponse);

                    //resumeController();

                } else if (WSCode == WebservicesHelper.WSCODE_ACTION_REPORTIN_SUCCESS){

                    //Build siteModel object
                    ActionController.getInstance().UserReportinSuccess(mContext, WSResponse);

                    resumeController();

                } else if (WSCode == WebservicesHelper.WSCODE_ACTION_CHECKOUT_SUCCESS) {

                    SBApplication.getSiteModel().isCheckin = false;

                    mView.viewSafeSite.setVisibility(View.GONE);
                    mView.viewSafeSite.startAnimation(animHideSafeSite);
                    CKPlaceModel.getInstance().updateSiteInfo(mContext);

                    resumeController();

                } else if (WSCode == WebservicesHelper.WSCODE_QUESTION_ANSWERED){

                    //Save answers
                    //Proceed to checkin if user has answer the questions previous
                    requestCheckIn(SBApplication.getSiteModel().checkingCategoryId);

                } else if (WSCode == WebservicesHelper.WSCODE_QUESTION_SUCCESS){

                    try {

                        JSONArray arrayQuestion = WSResponse.getJSONArray("question");
                        for (int i = 0; i < arrayQuestion.length(); i++) {
                            SBApplication.getSiteModel().arrayQuestion.add(new QuestionModel().getQuestionFromJson(arrayQuestion.getJSONObject(i)));
                        }

                        showQuestion();
                    }catch (JSONException ex){
                        ex.printStackTrace();

                        //If failed to get questions for site, resume checkin
                        //SBApplication.getSiteModel().isCheckin = true;
                        CKPlaceModel.getInstance().updateSiteInfo(mContext);
                        resumeController();
                    }

                    loadingBeat.cancelLoading();

                } else if (WSCode == WebservicesHelper.WSCODE_ACTION_PULSE_SUCCESS){

                    UtilDialog.showAlertWith1Button(mContext, mContext.getResources().getString(R.string.success), WSMessage, mContext.getResources().getString(R.string.btn_ok_title), null);
                    loadingBeat.cancelLoading();

                } else if (WSCode == WebservicesHelper.WSCODE_SITE_JOIN_SUCCESS){

                    UtilDialog.showAlertWith1Button(mContext, null, WSMessage, mContext.getResources().getString(R.string.btn_ok_title), null);
                    loadingBeat.cancelLoading();

                }else if (WSCode == WebservicesHelper.WSCODE_USER_STATUS_SUCCESS){

                    //Update user model with new info
                    SBApplication.getUserModel().status = Utilities.getIntValue(WSResponse,"userStatus");
                    //SBApplication.getUserModel().debub = Utilities.getIntValue(WSResponse,"debug");
                    SBApplication.getUserModel().badge = Utilities.getIntValue(WSResponse,"badge");
                    SBApplication.getUserModel().badgeMessage = Utilities.getIntValue(WSResponse,"badgeMessage");
                    SBApplication.getUserModel().badgeInDanger = Utilities.getIntValue(WSResponse,"badgeInDanger");

                    int totalBadge = SBApplication.getUserModel().badge + SBApplication.getUserModel().badgeMessage + SBApplication.getUserModel().badgeInDanger;
                    if (totalBadge != 0){
                        //Implement the red dot on navigation when push pending

                    }

                    //Update user model
                    CKPersonModel.getInstance().updateUserInfo(mContext);

                    //Show banner by user status
                    showStatusBar(CKPersonModel.getInstance().getUserBannerStatusById(SBApplication.getUserModel().status));

                    loadingBeat.cancelLoading();
                }

            } else{

                if (WSCode == WebservicesHelper.WSCODE_SITE_JOIN_FAIL || WSCode == WebservicesHelper.WSCODE_SITE_JOIN_NO_MATCH || WSCode == WebservicesHelper.WSCODE_SITE_JOIN_SAME_ENTITY){

                    UtilDialog.showAlertWith1Button(mContext, null, WSMessage, mContext.getResources().getString(R.string.btn_cancel_title), null);

                }else {
                    //UtilDialog.showAlertWith1Button(mContext, mContext.getResources().getString(R.string.warning), WSMessage, mContext.getResources().getString(R.string.btn_cancel_title), null);
                }

            }

            //loadingBeat.cancelLoading();

        }

        @Override
        public void onError(String error) {

            UtilDialog.showAlertWith1Button(mContext, "", error , mContext.getResources().getString(R.string.ok), null);
            Log.e("WS error =>", error);
            //0. show topup alert
            //Show banner by user status
            showStatusBar(error);
            //1. Clear old marker
            clearMarkerAndListSite();
            //2. Show current marker
            showCurrentLocationMarker();
            //3. Handle Checkin Button
            mView.blurCheckIn.setVisibility(View.VISIBLE);
            mView.btnCheckIn.setEnabled(false);
            mView.pbGetLocation.setVisibility(View.GONE);

            loadingBeat.cancelLoading();
        }

        @Override
        public void onAccessDenied(String WSMessage) {
            loadingBeat.cancelLoading();

            mHandler.removeCallbacks(autoRefreshSiteThread);
            UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
            CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
        }

    };

    ILoading iLoading = new ILoading() {

        @Override
        public void showLoading() {
            loadingBeat.showLoading();
        }
    };

    public void getCurrentLocation() {

        if (currentMarker != null) {
            currentMarker.remove();
        }
        currentMarker = drawMarker(mView.googleMap,SBApplication.getUserModel().location);
        requestFetchSite();
    }

    private void showCurrentLocationMarker() {

        LatLng latLng = new LatLng(SBApplication.getUserModel().location.latitude,SBApplication.getUserModel().location.longitude);
        if (currentMarker != null) {
            currentMarker.remove();
        }
        currentMarker = drawMarker(mView.googleMap, latLng);
        mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_CURRENT));
    }

    private void reloadSite(){

        //Clear map pins
        mView.googleMap.clear();
        //2. Show current marker
        showCurrentLocationMarker();
        //3. Show new marker and zoom to cover all marker
        zoomToCoverAllMarkers(listSite);

    }

    private void clearMarkerAndListSite() {
        //1. Clear old marker
        listMarkerSite.clear();
        listSite.clear();
    }

    /**
     * Method will sort the sites around user and only show those that are near to checkin
     * **/
    public void loadSiteMapNearMe(JSONObject response){

        try {

            JSONArray arraySite = new JSONArray(response.getString("site"));

            for (int i = 0; i < arraySite.length(); i++) {

                CKPlaceModel CKPlaceModel = new CKPlaceModel().getSiteModelFromJson(arraySite.getJSONObject(i));

                double distanceFromSite = Utilities.calculateDistance(CKPlaceModel.siteLocation, SBApplication.getUserModel().location);
                if (distanceFromSite < Constants.DISTANCE_FROM_ME){
                    listSite.add(CKPlaceModel);
                }
            }

        }catch (JSONException ex){
            ex.printStackTrace();
        }

        //4. Handle Checkin Button
        if (listSite != null && listSite.size() > 0) {
            mView.blurCheckIn.setVisibility(View.GONE);
            mView.btnCheckIn.setEnabled(true);
        } else {
            mView.blurCheckIn.setVisibility(View.VISIBLE);
            mView.btnCheckIn.setEnabled(false);
        }

        reloadSite();
    }

    public void zoomToCoverAllMarkers(ArrayList<CKPlaceModel> siteList) {
        try {

            if (siteList != null && siteList.size() > 0) {
                int size = siteList.size();
                for (int i = 0; i < size; i++) {

                    CKPlaceModel site = siteList.get(i);
                    MarkerOptions markerOption = new MarkerOptions().position(new LatLng(site.siteLocation.latitude, site.siteLocation.longitude));
                    markerOption.icon(BitmapDescriptorFactory.fromResource(R.mipmap.pin_red));
                    Marker marker = mView.googleMap.addMarker(markerOption);
                    listMarkerSite.put(marker, site);
                    //listSite.add(latLngList.get(i));

                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void HandleCheckin(JSONObject WSResponse){

        if (AddonModel.getInstance().entityPermission(Constants.ADDON_TYPE_HAZARD_REPORT)){
            if (SBApplication.getSiteModel().showSiteHazard){
                showHazard();
            }else{
                if (SBApplication.getSiteModel().showProjectStatus){
                    showFuelGauge();
                }else{
                    resumeController();
                }
            }
        }else{

            if (SBApplication.getSiteModel().showProjectStatus){
                showFuelGauge();
            }else{
                resumeController();
            }
        }

        loadingBeat.cancelLoading();
    }

    public void showStatusBar(final String message){
        Log.e("banner show", " :: message =>" + message);
        if (SBApplication.getSiteModel().isCheckin){

            int userStatus = SBApplication.getUserModel().status;

            if (userStatus == 1){

                //Show banner by user status
                mView.topPopupView.showBanner(TopPopupView.STATE_BANNER_CONSTANT, message);

                //Hide status bar after 2 seconds
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideStatusBar(message);
                    }
                }, 2000);

            }else{

                if (userStatus > 3 && userStatus < 7){

                    //Flash the banner in read and don't dismiss
                    mView.topPopupView.showBanner(TopPopupView.STATE_BANNER_CONSTANT, message);

                }else{
                    //Remove flash and still display banner
                    mView.topPopupView.showBanner(TopPopupView.STATE_BANNER_CONSTANT, message);

                    //Hide status bar after 2 seconds
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            hideStatusBar(message);
                        }
                    }, 2000);

                }
            }

        }else{

            //If flashing set banner back to normal
            mView.topPopupView.showBanner(TopPopupView.STATE_BANNER_CONSTANT, message);

        }

    }

    public void hideStatusBar(final String message){
        Log.e("banner hide" , " :: message =>" + message);
        //Hide status bar after 2 seconds
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.topPopupView.hideBanner(message);
            }
        }, 2000);

    }

    @Override
    public void setListener() {
        super.setListener();
        ctrlJoinSite.setListener();
        ctrlJoinSite.setCallBack(iJoinSite);

        mView.navView.btnLeft.setOnClickListener(this);
        mView.navView.btnRight.setOnClickListener(this);

        mView.btnCurrentLocation.setOnClickListener(this);
        mView.btnCheckIn.setOnClickListener(this);
        mView.btnSendAPulse.setOnClickListener(this);
        mView.btnCreateSite.setOnClickListener(this);
        mView.btnJoinSite.setOnClickListener(this);

        mView.viewCheckIn.rlCheckIn.setOnClickListener(this);
        mView.viewCheckIn.btnSendAPulseForCheckIn.setOnClickListener(this);
        mView.viewCheckIn.btnCreateSiteForCheckIn.setOnClickListener(this);

        mView.viewSiteCheckIn.rlSiteCheckIn.setOnClickListener(this);
        mView.viewSiteCheckIn.btnCheckInForSite.setOnClickListener(this);
        mView.viewSiteCheckIn.btnInfoForSite.setOnClickListener(this);

        mView.helpView.btnPrevious.setOnClickListener(this);
        mView.helpView.btnNext.setOnClickListener(this);
        mView.helpView.helpDialogView.btnPrevious.setOnClickListener(this);
        mView.helpView.helpDialogView.btnNext.setOnClickListener(this);

        mView.listCategoryView.btnCancel.setOnClickListener(this);

        mView.googleMap.setInfoWindowAdapter(new InfoWindowAdapter() {
            MarkerView markerView = null;

            @Override
            public View getInfoWindow(Marker arg0) {
                if (arg0.equals(currentMarker)) {
                    markerView = new MarkerView(mContext);
                    markerView.tvSiteName.setText("Current Location");
                    markerView.tvDistance.setVisibility(View.GONE);
                    markerView.tvCurrentLocation.setVisibility(View.GONE);
                } else {
                    CKPlaceModel CKPlaceModel = listMarkerSite.get(arg0);
                    if (CKPlaceModel != null) {
                        Log.d("site model", CKPlaceModel.name);
                        markerView = new MarkerView(mContext);
                        markerView.tvSiteName.setText(CKPlaceModel.name);
                        double distance = Utilities.calculateDistance(SBApplication.getSiteModel().siteLocation,SBApplication.getUserModel().location);
                        markerView.tvDistance.setText(distance + " KM");
                    }
                }
                // marker infor view at here
                return markerView;
            }

            @Override
            public View getInfoContents(Marker arg0) {
                return null;
            }
        });

        mView.googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

            @Override
            public void onInfoWindowClick(Marker marker) {

                if (marker.equals(currentMarker)) {
                    bottomCheckIn(new LatLng(SBApplication.getUserModel().location.latitude - Constants.DELTA_LAT, SBApplication.getUserModel().location.longitude + Constants.DELTA_LNG));
                } else {

                    final CKPlaceModel CKPlaceModel = listMarkerSite.get(marker);
                    if (CKPlaceModel != null) {
                        //Animate when user tap on the site pin detail
                        mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                                CKPlaceModel.siteLocation.latitude - Constants.DELTA_LAT,
                                CKPlaceModel.siteLocation.longitude + Constants.DELTA_LNG),
                                ZOOM_CHECK_IN));
                        mView.viewSiteCheckIn.setVisibility(View.VISIBLE);
                        mView.viewSiteCheckIn.tvSiteName.setText(CKPlaceModel.name);
                        mView.viewSiteCheckIn.tvSiteAddress.setText(CKPlaceModel.stNumber + " " + CKPlaceModel.stName);
                        mView.viewSiteCheckIn.btnInfoForSite
                                .setOnClickListener(new OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        mView.viewSiteCheckIn.setVisibility(View.GONE);
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable(Constants.KEY_SITE_ID, CKPlaceModel);
                                        ((MainActivity) mContext).addFragment(Constants.FRAGMENT_SITE_DETAIL, Constants.ANIMATION_VIEW_TYPE_FADE, bundle);
                                    }
                                });

                        mView.viewSiteCheckIn.btnCheckInForSite
                                .setOnClickListener(new OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        mView.viewSiteCheckIn.setVisibility(View.GONE);

                                        double siteRadius = CKPlaceModel.radius / 1000;
                                        double distanceFromSite = Utilities.calculateDistance(CKPlaceModel.siteLocation,SBApplication.getUserModel().location);

                                        if (distanceFromSite <= siteRadius + 0.25) {

                                            if (SBApplication.getEntityModel().arrayCheckinCategory.size() > 0){
                                                showCategoryList(SBApplication.getSiteModel());
                                            }else{
                                                requestSiteQuestion(SBApplication.getSiteModel());
                                            }

                                        } else if ((distanceFromSite > siteRadius + 0.25) && (distanceFromSite <= siteRadius + 0.8)) {
                                            showAlertCheckIn(mContext, CKPlaceModel);
                                        } else {
                                            //block checkin
                                            showAlertNotInSiteAcceptedDistance(mContext, SBApplication.getSiteModel());
                                        }
                                    }
                                });
                    }
                }
            }
        });

        mView.googleMap.setOnMapLongClickListener(new OnMapLongClickListener() {

            @Override
            public void onMapLongClick(LatLng pos) {
                if (createSiteMarker != null) {
                    createSiteMarker.remove();
                }
                createSiteMarker = mView.googleMap.addMarker(new MarkerOptions()
                        .position(pos)
                        .title("")
                        .icon(BitmapDescriptorFactory
                        .fromResource(R.mipmap.pin_site_new)));
                createSitePosition = pos;
                bottomCheckIn(new LatLng(pos.latitude - Constants.DELTA_LAT,pos.longitude + Constants.DELTA_LNG));
            }
        });
    }

    @Override
    public View getView() {
        return mView;
    }

    /**
     * remove create site pin
     */
    private void removeCreateSitePin() {
        if (createSiteMarker != null) {
            createSiteMarker.remove();
            SBApplication.getSiteModel().siteLocation = null;
        }
        mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(SBApplication.getUserModel().location,ZOOM_CURRENT));
    }

    @Override
    public void onClick(View v) {
        if (v == mView.navView.btnLeft) {
            ((MainActivity) mContext).toggle();
        } else if (v == mView.navView.btnRight) {

            if (SBApplication.getSiteModel().isCheckin) {
                mView.helpView.setTargets(mView.targetsHelpCheckIn, SBApplication.getSiteModel().isCheckin);
            } else {
                mView.helpView.setTargets(mView.targetsHelpNotCheckIn, SBApplication.getSiteModel().isCheckin);
            }
            helpStep = 0;
            mView.helpView.showHelpByStep(helpStep, SBApplication.getSiteModel().isCheckin);
        } else if (v == mView.btnCurrentLocation) {

            //Remove auto refresh so it can start the 15 seconds again
            mHandler.removeCallbacks(autoRefreshSiteThread);
            getCurrentLocation();

        } else if (v == mView.btnCheckIn) {

            bottomCheckIn(new LatLng(SBApplication.getUserModel().location.latitude - Constants.DELTA_LAT, SBApplication.getUserModel().location.longitude + Constants.DELTA_LNG));

        } else if (v == mView.btnSendAPulse || v == mView.viewCheckIn.btnSendAPulseForCheckIn) {
            removeCreateSitePin();
            mView.viewCheckIn.setVisibility(View.GONE);
            mView.blurBottom.setVisibility(View.GONE);
            enableButtonBottom(true);
            if (SBApplication.getEntityModel().companyId != 0){
                showOptionToSendAPulse();
            }

        } else if (v == mView.btnCreateSite || v == mView.viewCheckIn.btnCreateSiteForCheckIn) {
            // removeCreateSitePin();
            if (createSiteMarker != null) {
                createSiteMarker.remove();
            }
            mView.viewCheckIn.setVisibility(View.GONE);
            mView.blurBottom.setVisibility(View.GONE);
            Log.e("CREATE SITE", "=> YES");

            if (SBApplication.getEntityModel().companyId != 0){
                Bundle bundle = new Bundle();
                CKPlaceModel CKPlaceModel = new CKPlaceModel();
                if (createSitePosition != null) {
                    CKPlaceModel.siteLocation = new LatLng(createSitePosition.latitude, createSitePosition.longitude);
                    createSitePosition = null;
                }else{
                    CKPlaceModel.siteLocation = new LatLng(SBApplication.getUserModel().location.latitude, SBApplication.getUserModel().location.longitude);
                }
                bundle.putSerializable(Constants.KEY_SITE_ID, CKPlaceModel);
                ((MainActivity) mContext).addFragment(Constants.FRAGMENT_SITE_CREATE,Constants.ANIMATION_VIEW_TYPE_FADE,bundle);
            }

            //Remove call for site
            stopCallBacks();

        } else if (v == mView.btnJoinSite) {
            ctrlJoinSite.showDialog();
        } else if (v == mView.viewCheckIn.rlCheckIn) {
            removeCreateSitePin();
            mView.blurBottom.setVisibility(View.GONE);
            enableButtonBottom(true);
            mView.viewCheckIn.setVisibility(View.GONE);
        } else if (v == mView.viewSiteCheckIn.rlSiteCheckIn) {
            removeCreateSitePin();
            mView.viewSiteCheckIn.setVisibility(View.GONE);
            mView.blurBottom.setVisibility(View.GONE);
        } else if (v == mView.viewSiteCheckIn.btnCheckInForSite) {
            removeCreateSitePin();
            mView.viewSiteCheckIn.setVisibility(View.GONE);
            mView.blurBottom.setVisibility(View.GONE);
        } else if (v == mView.viewSiteCheckIn.btnInfoForSite) {
            removeCreateSitePin();
            mView.viewSiteCheckIn.setVisibility(View.GONE);
            mView.blurBottom.setVisibility(View.GONE);
        } else if (v == mView.helpView.helpDialogView.btnNext) {
            helpStep++;
            mView.helpView.showHelpByStep(helpStep, SBApplication.getSiteModel().isCheckin);
        } else if (v == mView.helpView.helpDialogView.btnPrevious) {
            helpStep--;
            mView.helpView.showHelpByStep(helpStep, SBApplication.getSiteModel().isCheckin);
        } else if (v == mView.helpView.btnNext) {
            helpStep++;
            mView.helpView.showHelpByStep(helpStep, SBApplication.getSiteModel().isCheckin);
        } else if (v == mView.helpView.btnPrevious) {
            helpStep--;
            mView.helpView.showHelpByStep(helpStep, SBApplication.getSiteModel().isCheckin);
        } /*else if (v == mView.listCategoryView.btnCancel) {
            mView.listCategoryView.setVisibility(View.GONE);
			mView.listCategoryView.startAnimation(animHideSafeSite);
			enableViewWhenHelp(true);
			showPopUp = false;
		}*/
    }

    public void showFuelGauge() {

        if (SBApplication.getSiteModel().arrayProjectStatus != null) {
            ((MainActivity) mContext).addFragment(Constants.FRAGMENT_SITE_PROJECT_STATUS, Constants.ANIMATION_VIEW_TYPE_FADE, null);
        } else {
            requestCheckIn(SBApplication.getSiteModel().checkingCategoryId);
        }
    }

    public void showHazard(){

         //load Site Hazard Fragment - copy this code to the correct place
        ((MainActivity) mContext).addFragment(Constants.FRAGMENT_SITE_HAZARD,Constants.ANIMATION_VIEW_TYPE_FADE,null);

    }

    /**
     * after user has checked in - the SafeSiteView is displayed
     * and this method is called.
     * create on click listeners for the various buttons
     */
    public void CheckinSuccess() {

        mView.viewSafeSite.tvSiteName.setText("Checked-in on: " + SBApplication.getSiteModel().name);
        mView.viewSafeSite.tvTimeToReport.setText(mContext.getResources().getString(R.string.tv_safety_content) + " " + Utilities.getTimeReport());

        mView.blurBottom.setVisibility(View.GONE);
        enableButtonBottom(true);
        mView.viewSafeSite.setVisibility(View.VISIBLE);
        //if (!isResume) {
            //mView.viewSafeSite.startAnimation(animShowSafeSite);
        //}

        mView.viewSafeSite.btnReportIn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                requestReportIn();
            }
        });
        mView.viewSafeSite.btnEmergency.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showEmergencyList();
                //emergency(siteModel);
            }
        });
        mView.viewSafeSite.btnCheckOut.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showOptionCheckOut(mContext, SBApplication.getSiteModel());
            }
        });
        mView.viewSafeSite.btnSiteOptions.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //((MainActivity) mContext).switchFragment(Constants.FRAGMENT_SITE_DETAIL,
                // null);
                //((MainActivity) mContext).switchFragment(Constants.FRAGMENT_SITE_OPTION,Constants.ANIMATION_VIEW_TYPE_FADE,null);
                ((MainActivity) mContext).addFragment(Constants.FRAGMENT_SITE_OPTION, Constants.ANIMATION_VIEW_TYPE_FADE,null);

						/*
						// custom dialog
						final Dialog dialog = new Dialog(mContext);
						dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
						dialog.setContentView(R.layout.dialog_site_options);
						dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
								WindowManager.LayoutParams.MATCH_PARENT);

						dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(220, 225, 225, 225)));



						Button dialogButton = (Button) dialog.findViewById(R.id.btnClose);
						// if button is clicked, close the custom dialog
						dialogButton.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								dialog.dismiss();
							}
						});

						RelativeLayout rlSiteInfo = (RelativeLayout)dialog
								.findViewById(R.id.rlSiteInfo);
						rlSiteInfo.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View view) {
								((MainActivity) mContext).switchFragment(Constants.FRAGMENT_SITE_DETAIL, null);
								dialog.dismiss();
							}
						});

						dialog.show();
						*/

            }
        });
    }

    public void showQuestion() {

        //remove call to fetch site
        mHandler.removeCallbacks(autoRefreshSiteThread);
        mView.blurBottom.setVisibility(View.GONE);
        enableButtonBottom(true);
        //mView.llTaskBar.setVisibility(View.INVISIBLE);

        //((MainActivity) mContext).switchFragment(Constants.FRAGMENT_QUESTION, Constants.ANIMATION_VIEW_TYPE_FADE, null);
        ((MainActivity) mContext).addFragment(Constants.FRAGMENT_QUESTION,Constants.ANIMATION_VIEW_TYPE_FADE,null);

    }

    public void showCategoryList(final CKPlaceModel CKPlaceModel) {

        SBApplication.getSiteModel().canProceed = false;
        Log.e("arrayCategory:", " =>" + SBApplication.getEntityModel().arrayCheckinCategory);
        String[] categoryName = new String[SBApplication.getEntityModel().arrayCheckinCategory.size()];

        //Create arrayAnswers with checkinCategory titles
        for (int i = 0; i < SBApplication.getEntityModel().arrayCheckinCategory.size(); i++) {
            categoryName[i] = SBApplication.getEntityModel().arrayCheckinCategory.get(i).name;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(mContext, R.style.Theme_DialogCustom));
        builder.setTitle(mContext.getResources().getString(R.string.msg_action_checkin_category))
                .setItems(categoryName,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                requestCheckIn(Integer.valueOf(SBApplication.getEntityModel().arrayCheckinCategory.get(which).checkinCategoryId));
                            }
                        })
                .setPositiveButton(
                        mContext.getResources().getString(R.string.btn_cancel_title), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                enableButtonBottom(true);
                                mView.blurBottom.setVisibility(View.GONE);
                            }
                        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    /**
     * Check int
     */
    public void checkInToSite(final CKPlaceModel siteInfo) {

        mView.viewCheckIn.setVisibility(View.GONE);
        mView.blurBottom.setVisibility(View.GONE);
        enableButtonBottom(true);

        //Save siteModel global
        SBApplication.setSiteModel(siteInfo);

        if (siteInfo != null) {
            double siteRadius = siteInfo.radius / 1000;
            double distanceFromSite = Utilities.calculateDistance(siteInfo.siteLocation,SBApplication.getUserModel().location);

            if (distanceFromSite <= siteRadius + 0.25) {

                if (siteInfo.safezone){

                    if (SBApplication.getEntityModel().arrayCheckinCategory.size() > 0){
                        showCategoryList(siteInfo);
                    }else{
                        requestCheckIn(SBApplication.getSiteModel().checkingCategoryId);
                    }

                }else{

                    if (SBApplication.getEntityModel().arrayCheckinCategory.size() > 0){
                        showCategoryList(siteInfo);
                    }else{
                        requestSiteQuestion(siteInfo);
                    }
                }

            } else if ((distanceFromSite > siteRadius + 0.25) && (distanceFromSite <= siteRadius + 0.8)) {
                showAlertCheckIn(mContext, siteInfo);
            } else {
                //block check in
                showAlertNotInSiteAcceptedDistance(mContext, siteInfo);
            }
        }
    }

    public void showEmergencyList() {

        String[] emergencyName = new String[SBApplication.getSiteModel().arrayEmergency.size()];

        for (int i = 0; i < SBApplication.getSiteModel().arrayEmergency.size(); i++) {
            EmergencyModel emergencyInfo = SBApplication.getSiteModel().arrayEmergency.get(i);
            emergencyName[i] = emergencyInfo.name;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(mContext, R.style.Theme_DialogCustom));

        builder.setTitle("Emergency")
                .setItems(emergencyName,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                EmergencyModel emergencyInfo = SBApplication.getSiteModel().arrayEmergency.get(which);
                                callIntent.setData(Uri.parse("tel:" + emergencyInfo.phone));
                                mContext.startActivity(callIntent);

                            }
                        })
                .setPositiveButton(
                        mContext.getResources().getString(
                                R.string.btn_cancel_title),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }

                        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    public void showOptionToSendAPulse() {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                new ContextThemeWrapper(mContext, R.style.Theme_DialogCustom));
        final String[] pulseValue = mContext.getResources().getStringArray(
                R.array.pulse_array_value);
        builder.setTitle("Send Pulse")
                .setItems(R.array.pulse_array,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                                requestSendPulse(pulseValue[which]);
                            }
                        })
                .setPositiveButton(
                        mContext.getResources().getString(
                                R.string.btn_cancel_title),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }

                        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    /**
     * Show option checkout
     * When you first click on checkout it will show this prompt
     */
    public void showOptionCheckOut(Context mContext, final CKPlaceModel CKPlaceModel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(mContext, R.style.Theme_DialogCustom));
        builder.setTitle(mContext.getResources().getString(R.string.msg_action_checkout_finish_work))
                .setItems(R.array.checkout_array,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int which) {
                                dialog.dismiss();
                                int done = 0;
                                if (which == 0) {
                                    done = 1;
                                }
                                requestCheckOut(done);
                            }
                        })
                .setPositiveButton(
                        mContext.getResources().getString(R.string.btn_cancel_title),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,int which) {
                                dialog.dismiss();
                            }

                        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    /**
     * Show option alert user
     */
    public void showOptionAlertUser(Context mContext) {
        if (alertUserOutSite == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    new ContextThemeWrapper(mContext, R.style.Theme_DialogCustom));
            builder.setTitle("Warning! You go too far from site!")
                    .setPositiveButton(
                            mContext.getResources().getString(
                                    R.string.btn_ok_title),
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    dialog.dismiss();

                                }

                            });

            alertUserOutSite = builder.create();
            alertUserOutSite.setCancelable(true);
        }

        if ((mView.viewSiteCheckIn.getVisibility() == View.GONE || mView.viewCheckIn.getVisibility() == View.GONE) && !alertUserOutSite.isShowing()) {
            alertUserOutSite.show();
        }
    }

    /**
     * Show option alert check in when user is off site geofence but still under the acceptance distance
     */
    public void showAlertCheckIn(Context mContext, final CKPlaceModel CKPlaceModel) {

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(mContext, R.style.Theme_DialogCustom));
        builder.setTitle(String.format(mContext.getResources().getString(R.string.msg_action_checkin_geofence_acceptance), CKPlaceModel.radius))
                .setPositiveButton(
                        mContext.getResources().getString(R.string.btn_yes_title),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if (CKPlaceModel.safezone) {

                                    if (SBApplication.getEntityModel().arrayCheckinCategory.size() > 0) {
                                        showCategoryList(CKPlaceModel);
                                    } else {
                                        requestCheckIn(SBApplication.getSiteModel().checkingCategoryId);
                                    }

                                } else {

                                    if (SBApplication.getEntityModel().arrayCheckinCategory.size() > 0) {
                                        showCategoryList(CKPlaceModel);
                                    } else {
                                        requestSiteQuestion(CKPlaceModel);
                                    }

                                }

                                dialog.dismiss();
                            }

                        })
                .setNegativeButton(
                        mContext.getResources().getString(R.string.btn_cancel_title),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    /**
     * Show option checkout
     */
    public void showAlertNotInSiteAcceptedDistance(Context mContext, final CKPlaceModel CKPlaceModel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                new ContextThemeWrapper(mContext, R.style.Theme_DialogCustom));
        builder.setTitle(String.format(mContext.getResources().getString(R.string.msg_action_checkin_out_site_geofence), CKPlaceModel.radius))
                .setPositiveButton(
                        mContext.getResources().getString(R.string.btn_ok_title),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                enableButtonBottom(true);
                                mView.blurBottom.setVisibility(View.GONE);
                                dialog.dismiss();
                                reloadSite();
                            }

                        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    /**
     * set enable button in the bottom
     */
    private void enableButtonBottom(boolean select) {
        mView.btnCheckIn.setEnabled(select);
        mView.btnCreateSite.setEnabled(select);
        mView.btnSendAPulse.setEnabled(select);
        mView.btnJoinSite.setEnabled(select);
    }

    /**
     * Bottom check in
     */
    private void bottomCheckIn(LatLng latLng) {

        mView.blurBottom.setVisibility(View.VISIBLE);
        enableButtonBottom(false);
        mView.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,ZOOM_CHECK_IN));

        mView.viewCheckIn.setVisibility(View.VISIBLE);
        SiteListAdapter adapter = new SiteListAdapter(mContext, listSite);
        mView.viewCheckIn.lvAllSites.setAdapter(adapter);

        mView.viewCheckIn.lvAllSites.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                removeCreateSitePin();
                mView.viewCheckIn.setVisibility(View.GONE);
                checkInToSite(listSite.get(position));
            }
        });
    }

    private Thread autoRefreshSiteThread = new Thread(new Runnable() {
        @Override
        public void run() {

            //if ((Utilities.calculateDistance(SBApplication.getSiteModel().siteLocation,SBApplication.getUserModel().location) != 0.01)) {
            if (canRefresh()) {
                //Show banner by user status
                showStatusBar("Fetching sites");
                requestFetchSite();
            }
            //}
            mHandler.postDelayed(autoRefreshSiteThread, Constants.SECONDS_IN_A_MINUTE / 4 * 1000);
        }
    });

    private Thread autoRefreshHomeThread = new Thread(new Runnable() {
        @Override
        public void run() {
            //Show banner by user status
            showStatusBar("Retrieving your status");
            requestUserStatus();
            mHandler.postDelayed(autoRefreshHomeThread, Constants.SECONDS_IN_A_MINUTE * 5 * 1000);
        }
    });

    @Override
    public void destroy() {
        super.destroy();
        mHandler.removeCallbacks(autoRefreshSiteThread);
        mHandler.removeCallbacks(autoRefreshHomeThread);
    }

    public void stopCallBacks() {
        mHandler.removeCallbacks(autoRefreshSiteThread);
        mHandler.removeCallbacks(autoRefreshHomeThread);
    }

    public boolean canRefresh(){
        if (mView.viewSiteCheckIn.getVisibility() == View.GONE && mView.viewCheckIn.getVisibility() == View.GONE){
            return true;
        }
        return false;
    }
}
