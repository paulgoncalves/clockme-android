package com.clockme.controllers;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.activities.LoginActivity;
import com.clockme.activities.MainActivity;
import com.clockme.interfaces.ILoading;
import com.clockme.interfaces.IWebservice;
import com.clockme.models.CKCompanyModel;
import com.clockme.models.CKPersonModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.UtilDialog;
import com.clockme.utilities.Utilities;
import com.clockme.views.LoginView;
import com.crashlytics.android.Crashlytics;

import org.json.JSONObject;

public class LoginController extends AbstractController implements OnClickListener {

    private static String TAG = LoginController.class.getSimpleName();

    private Context mContext;
    private LoginView mView;
    private String email = "";
    private String password = "";
    private LoadingBeat loadingBeat;
    int loginFlag = 0;

    public LoginController(Context context, LoginView view) {
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void initData() {
        super.initData();
        loadingBeat = new LoadingBeat(mContext);

        Log.i(TAG, "initData() CKPersonModel email = " + SBApplication.getUserModel().email);

        try{
            if(SBApplication.getUserModel().email.length() != 0){
                mView.etEmail.setText(SBApplication.getUserModel().email);
                mView.etPassword.requestFocus();
                Utilities.showKeyboard(mContext, mView.etPassword);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    @Override
    public void setListener() {
        super.setListener();
        mView.rlRootView.setOnClickListener(this);
        mView.scrollView.setOnClickListener(this);
        mView.btnLogin.setOnClickListener(this);
        mView.btnResetPassword.setOnClickListener(this);
    }

    @Override
    public View getView() {
        return mView;
    }

    @Override
    public void onClick(View v) {
        if (v == mView.rlRootView || v == mView.scrollView) {
            Utilities.hideKeyboard(mContext, mView.btnLogin);
        } else if (v == mView.btnLogin) {
            Utilities.hideKeyboard(mContext, mView.btnLogin);
            if (checkEmail(true) && checkPassword())

                requestLogin();

        } else if (v == mView.btnResetPassword) {
            forgotPassword();
        }
    }

    private void requestLogin() {

        String body = String.format(WebservicesHelper.PARAMS_LOGIN, email, password);
        WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_LOGIN, body);

    }

    private void requestResetPassword() {

        String body = String.format(WebservicesHelper.PARAMS_LOGIN_FORGOT_PASSWORD, email);
        WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_LOGIN_FORGOT_PASSWORD, body);

    }

    IWebservice IWebserviceRequest = new IWebservice() {

        @Override
        public void onComplete(JSONObject WSResponse, String WSMessage, int WSCode) {

            if (WSResponse.optInt("status") == 1){

                //Site retrieved
                if (WSCode == WebservicesHelper.WSCODE_LOGIN_SUCCESS) {

                    Crashlytics.setString("Sign_In", "Signed in successfully");

                    //Save user detail
                    CKPersonModel.getInstance().getUserModelFromJson(WSResponse);
                    CKCompanyModel.getInstance().getEntityModelFromJson(WSResponse);

                    //Create session
                    SBApplication.getUserModel().session = true;

                    //Update data saving to phone memory
                    CKPersonModel.getInstance().updateUserInfo(mContext);
                    CKCompanyModel.updateEntityInfo(mContext);

                    ((Activity) mContext).overridePendingTransition(R.anim.stand_by,R.anim.out_to_bottom);

                    //Build again main activity after login success
                    Intent i = new Intent(mContext, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mContext.startActivity(i);
                    ((Activity)mContext).finish();

                } else if(WSCode == WebservicesHelper.WSCODE_LOGIN_FORGOT_PASSWORD) {

                    UtilDialog.showAlertWith1Button(mContext, WSMessage, WSMessage, mContext
                            .getResources().getString(R.string.btn_ok_title), null);

                }  else if (WSCode == WebservicesHelper.WSCODE_LOGIN_FAIL) {

                    loginFlag++;
                    //Crashlytics.setString("Sign_In", "Signed in fail");
                    if (loginFlag < 3) {
                        UtilDialog.showAlertWith1Button(mContext, "",WSMessage,mContext.getResources().getString(R.string.btn_ok_title), null);
                    } else {
                        loginFlag = 0;

                        UtilDialog.showAlertWith2Buttons(((Activity) mContext), "",
                                mContext.getResources()
                                        .getString(R.string.message_to_reset_password),
                                mContext.getResources().getString(
                                        R.string.btn_ok_title),  mContext.getResources().getString(
                                        R.string.btn_cancel_title), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        forgotPassword();
                                    }
                                }, null);
                    }
                }

            }else{

                UtilDialog.showAlertWith1Button(mContext, "",WSMessage,mContext.getResources().getString(R.string.btn_ok_title), null);

            }

            loadingBeat.cancelLoading();
        }

        @Override
        public void onError(String error) {

            Crashlytics.setString("Attention", error);
            loadingBeat.cancelLoading();
            UtilDialog.showAlertWith1Button(mContext, "", error, mContext.getResources().getString(R.string.btn_ok_title), null);

        }

        @Override
        public void onAccessDenied(String WSMessage) {
            UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
            CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
            loadingBeat.cancelLoading();
        }

    };

    ILoading iLoading = new ILoading() {

        @Override
        public void showLoading() {
            loadingBeat.showLoading();
        }

//        @Override
//        public void hideLoading() {
//
//        }
    };


    private void forgotPassword() {
        Utilities.hideKeyboard(mContext, mView.btnResetPassword);
        if (checkEmail(false))
            UtilDialog.showAlertWith2Buttons(
                    (Activity) mContext,
                    null,
                    mContext.getResources().getString(R.string.tv_reset_password_title),
                    mContext.getResources().getString(
                            R.string.btn_yes_title), mContext
                            .getResources()
                            .getString(R.string.btn_no_title),
                    resetPassword, null);
    }

    private boolean checkEmail(boolean isCheckedLogin) {
        email = mView.etEmail.getText().toString().trim();

        String message;
        if (isCheckedLogin) {
            message = mContext.getResources().getString(
                    R.string.error_empty_field);
        } else {
            message = mContext.getResources().getString(R.string.error_email);
        }

        if (TextUtils.isEmpty(email)) {
            UtilDialog.showAlertWith1Button(mContext, null, message, mContext
                    .getResources().getString(R.string.btn_cancel_title), null);
            return false;
        }

        if (!Utilities.checkEmailFormat(email)) {
            UtilDialog.showAlertWith1Button(mContext, null, mContext
                            .getResources().getString(R.string.error_invalid_email),
                    mContext.getResources()
                            .getString(R.string.btn_cancel_title), null);
            return false;
        }

        return true;
    }

    private boolean checkPassword() {
        password = mView.etPassword.getText().toString().trim();
        if (TextUtils.isEmpty(password)) {
            UtilDialog.showAlertWith1Button(mContext, null, mContext
                            .getResources().getString(R.string.error_empty_field),
                    mContext.getResources()
                            .getString(R.string.btn_cancel_title), null);
            return false;
        }

        return true;
    }

    public DialogInterface.OnClickListener resetPassword = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            requestResetPassword();
        }
    };

}
