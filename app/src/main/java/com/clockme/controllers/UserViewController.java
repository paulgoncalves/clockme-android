package com.clockme.controllers;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.activities.MainActivity;
import com.clockme.interfaces.ILoading;
import com.clockme.interfaces.IWebservice;
import com.clockme.models.CKPersonModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.Constants;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.RequestConnectionDialog;
import com.clockme.utilities.UtilDialog;
import com.clockme.utilities.Utilities;
import com.clockme.views.UserProfileView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class UserViewController extends AbstractController implements OnClickListener {

	private Context mContext;
	private UserProfileView mView;
	private ConnectionModel mConnection = new ConnectionModel();
	ArrayList<LogModel> logs = new ArrayList<>();
	HashMap <String, ArrayList<ConnectionModel>> arrayConnection = new HashMap<>();
	ArrayList<String> arrayConnectionType = new ArrayList<>();
	private LogsListAdapter adapter;
	private LoadingBeat loadingBeat;

	public UserViewController(Context context, UserProfileView vUserProfile) {
		this.mContext = context;
		this.mView = vUserProfile;
		loadingBeat = new LoadingBeat(mContext);
	}

	@Override
	public void initData() {
		super.initData();
	}

	public void initDataFromBundle(Bundle bundle){

		if (bundle.containsKey(Constants.KEY_CONNECTION)){

			//Init conneciton from bundle Seriliazer
			mConnection = (ConnectionModel) bundle.getSerializable(Constants.KEY_CONNECTION);

			//Fetch connection detail
			requestUserConnectionSingle(mConnection.userId);

		}

	}

	public void requestUserConnectionSingle(int userId) {

		arrayConnection.clear();
		String body = String.format(WebservicesHelper.PARAMS_CONNECTION_USERS_SINGLE, SBApplication.getUserModel().personId, SBApplication.getUserModel().token,
				SBApplication.getEntityModel().companyId,userId,SBApplication.getUserModel().accessLevel);
		WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_CONNECTION_USER_SINGLE, body);

	}

	public void requestAddConnection(int permission) {

		String body = String.format(WebservicesHelper.PARAMS_CONNECTION_ADD, SBApplication.getUserModel().personId, SBApplication.getUserModel().token,
				SBApplication.getEntityModel().companyId,mConnection.userId,permission,SBApplication.getUserModel().timezone);
		WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_CONNECTION_ADD, body);

	}

	public void requestUserLog(int userId) {

		String body = String.format(WebservicesHelper.PARAMS_USER_LOG_BY_ENTITY, SBApplication.getUserModel().personId, SBApplication.getUserModel().token,
				SBApplication.getEntityModel().companyId,userId);
		WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_USER_LOG_BY_ENTITY, body);

	}

//	public void requestRemoveConnection(ConnectionModel userObj) {
//
//		String body = String.format(WebservicesHelper.PARAMS_SITE_PROJECT, SBApplication.getUserModel().personId, SBApplication.getUserModel().token);
//		WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_SITE_PROJECT, body);
//
//	}
//
//	public void requestAcceptConnection(ConnectionModel userObj) {
//
//		String body = String.format(WebservicesHelper.PARAMS_SITE_PROJECT, SBApplication.getUserModel().personId, SBApplication.getUserModel().token);
//		WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_SITE_PROJECT, body);
//
//	}

	IWebservice IWebserviceRequest = new IWebservice() {

		@Override
		public void onComplete(JSONObject WSResponse, String WSMessage, int WSCode) {

			if (WSResponse.optInt("status") == 1) {

				if (WSCode == WebservicesHelper.WSCODE_CONNECTION_USER_SINGLE_FETCH_SUCCESS) {

					handleConnectionData(WSResponse);

				} else if (WSCode == WebservicesHelper.WSCODE_CONNECTION_ADD_SUCCESS) {

					requestUserConnectionSingle(mConnection.userId);

				}else if (WSCode == WebservicesHelper.WSCODE_CONNECTION_ACCEPT_SUCCESS) {

					requestUserConnectionSingle(mConnection.userId);

				}else if (WSCode == WebservicesHelper.WSCODE_CONNECTION_REMOVE_SUCCESS) {

					requestUserConnectionSingle(mConnection.userId);

				}else if (WSCode == WebservicesHelper.WSCODE_LOG_USER_SUCCESS) {

					try {

						JSONArray arrayLog = WSResponse.getJSONArray("log");

						for (int i = 0; i < arrayLog.length(); i++) {
							logs.add(new LogModel().getLogModelFromJson(arrayLog.getJSONObject(i)));
						}

						adapter = new LogsListAdapter(mContext, logs);
						mView.lvLogs.setAdapter(adapter);

						loadingBeat.cancelLoading();

					}catch (JSONException ex){
						ex.printStackTrace();
					}


					loadingBeat.cancelLoading();

				}
			}else{

				loadingBeat.cancelLoading();
			}

		}

		@Override
		public void onError(String error) {
			loadingBeat.cancelLoading();
			UtilDialog.showAlertWith1Button(mContext, "", error , mContext.getResources().getString(R.string.ok), null);
			Log.e("WS error =>", error);
		}

		@Override
		public void onAccessDenied(String WSMessage) {
			loadingBeat.cancelLoading();
			UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
			CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
		}
	};

	ILoading iLoading = new ILoading() {

		@Override
		public void showLoading() {
			loadingBeat.showLoading();
		}

	};

	public void handleConnectionData(JSONObject WSResponse) {

		ArrayList<ConnectionModel> arrLeader = new ArrayList<>();
		ArrayList<ConnectionModel> arrFollower = new ArrayList<>();
		ArrayList<ConnectionModel> arrMember = new ArrayList<>();
		ArrayList<ConnectionModel> arrPending = new ArrayList<>();

		try {

			//Get connections
			JSONObject connectObj = WSResponse.getJSONObject("myConnections");

			//Get connection by type Leader
			try {

				if (connectObj.has(Constants.KEY_CONNECTION_LEADER)) {

					JSONArray arrayLeaders = connectObj.getJSONArray(Constants.KEY_CONNECTION_LEADER);
					for (int i = 0; i < arrayLeaders.length(); i++) {
						arrLeader.add(new ConnectionModel().getConnectionFromJson(arrayLeaders.getJSONObject(i)));
					}

					arrayConnection.put(Constants.KEY_CONNECTION_LEADER, arrLeader);
					arrayConnectionType.add(Constants.KEY_CONNECTION_LEADER);
				}

			} catch (JSONException ex) {
				ex.printStackTrace();
			}

			//Get connection by type Follower
			try {

				if (connectObj.has(Constants.KEY_CONNECTION_FOLLOWER)) {

					JSONArray arrayFollowers = connectObj.getJSONArray(Constants.KEY_CONNECTION_FOLLOWER);
					for (int i = 0; i < arrayFollowers.length(); i++) {
						arrFollower.add(new ConnectionModel().getConnectionFromJson(arrayFollowers.getJSONObject(i)));
					}

					arrayConnection.put(Constants.KEY_CONNECTION_FOLLOWER, arrFollower);
					arrayConnectionType.add(Constants.KEY_CONNECTION_FOLLOWER);
				}

			} catch (JSONException ex) {
				ex.printStackTrace();
			}

			//Get connection by type Member
			try {

				if (connectObj.has(Constants.KEY_CONNECTION_MEMBER)) {

					JSONArray arrayMembers = connectObj.getJSONArray(Constants.KEY_CONNECTION_MEMBER);
					for (int i = 0; i < arrayMembers.length(); i++) {
						arrMember.add(new ConnectionModel().getConnectionFromJson(arrayMembers.getJSONObject(i)));
					}

					arrayConnection.put(Constants.KEY_CONNECTION_MEMBER, arrMember);
					arrayConnectionType.add(Constants.KEY_CONNECTION_MEMBER);
				}

			} catch (JSONException ex) {
				ex.printStackTrace();
			}

			//Get connection by type Pending
			try {

				if (connectObj.has(Constants.KEY_CONNECTION_PENDING)) {

					JSONArray arrayPending = connectObj.getJSONArray(Constants.KEY_CONNECTION_PENDING);
					for (int i = 0; i < arrayPending.length(); i++) {
						arrPending.add(new ConnectionModel().getConnectionFromJson(arrayPending.getJSONObject(i)));
					}

					arrayConnection.put(Constants.KEY_CONNECTION_PENDING, arrPending);
					arrayConnectionType.add(Constants.KEY_CONNECTION_PENDING);
				}

			} catch (JSONException ex) {
				ex.printStackTrace();
			}

		} catch (JSONException ex) {
			ex.printStackTrace();
		}

		//After loading the connection object, process the data
		loadConnectionData();
	}

	private void loadConnectionData(){

		mView.tvUsername.setText(mConnection.firstName + " " + mConnection.lastName);
		mView.tvEmail.setText(mConnection.email);
		mView.tvPhone.setText(mConnection.mobile);
		mView.navView.tvTitle.setText(mConnection.firstName + " Profile");

		//Set image
		WebservicesHelper.getInstance().webserviceLoadImage(mView.ivAvatar, Constants.IMG_TYPE_USER, mConnection.userId);

		//Hide or show log if user has privilege
		if (mConnection.isConnected || SBApplication.getUserModel().accessLevel == Constants.USER_ACCESS_LEVEL_MANAGER){

			mView.lvLogs.setVisibility(View.VISIBLE);
			mView.tvIsConnection.setText("Connections");
			mView.lnIsConnected.setVisibility(View.VISIBLE);
			mView.lnIsConnected.setOnClickListener(this);

			//Only fetch user log if user has access to it
			requestUserLog(mConnection.userId);

		}else{
			mView.lvLogs.setVisibility(View.INVISIBLE);
			mView.lnIsConnected.setVisibility(View.INVISIBLE);
		}

		try {

			if (mConnection.isConnected && !mConnection.pendingRequest) {

				if (arrayConnection.size() > 1){
					//Show Leader
					mView.lnConnectionStatusLeader.setVisibility(View.VISIBLE);

					//Show Follower
					mView.lnConnectionStatusFollower.setVisibility(View.VISIBLE);

					mView.tvConnecionStatusLeader.setText(ConnectionModel.getConnectionType(arrayConnection.get(arrayConnectionType.get(0)).get(0)));
					mView.tvConnecionStatusFollower.setText(ConnectionModel.getConnectionType(arrayConnection.get(arrayConnectionType.get(1)).get(0)));

					mView.ivConnStatusLeader.setImageResource(R.mipmap.icon_check);
					mView.ivConnStatusFollower.setImageResource(R.mipmap.icon_check);

				}else if (arrayConnection.size() == 1){
					//Show Leader
					mView.lnConnectionStatusLeader.setVisibility(View.VISIBLE);

					//Show Follower
					mView.lnConnectionStatusFollower.setVisibility(View.INVISIBLE);

					mView.tvConnecionStatusLeader.setText(ConnectionModel.getConnectionType(arrayConnection.get(arrayConnectionType.get(0)).get(0)));

					mView.ivConnStatusLeader.setImageResource(R.mipmap.icon_check);
				}

			} else {

				if(mConnection.pendingRequest){

					//Show Leader but with pending information
					mView.tvConnecionStatusLeader.setText(ConnectionModel.getConnectionType(arrayConnection.get(arrayConnectionType.get(0)).get(0)));
					mView.ivConnStatusLeader.setImageResource(R.mipmap.icon_pending);

					//Show Leader
					mView.lnConnectionStatusLeader.setVisibility(View.VISIBLE);

					//Hide Follower
					mView.lnConnectionStatusFollower.setVisibility(View.INVISIBLE);

				}else{

					//Show Leader but with button to request connection
					mView.tvConnecionStatusLeader.setText("Request Connection");
					mView.ivConnStatusLeader.setImageResource(R.mipmap.icon_add_conn);

					//Show Leader
					mView.lnConnectionStatusLeader.setVisibility(View.VISIBLE);

					//Hide Follower
					mView.lnConnectionStatusFollower.setVisibility(View.INVISIBLE);

					//Allow the user to click on and request connection
					mView.tvConnecionStatusLeader.setOnClickListener(this);
				}

			}

		}catch (Exception ex){
			ex.printStackTrace();
		}

	}

	@Override
	public void setListener() {
		super.setListener();
		mView.navView.btnLeft.setOnClickListener(this);
		mView.navView.btnRight.setOnClickListener(this);
		mView.tvEmail.setOnClickListener(this);
		mView.tvPhone.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (v == mView.navView.btnLeft) {
			((MainActivity) mContext).toggle();
		} else if (v == mView.navView.btnRight) {
			((MainActivity) mContext).popFragment();
		} else if (v == mView.tvEmail) {
			Utilities.sendEmail(mContext, mConnection.email);
		} else if (v == mView.tvPhone) {
			Utilities.makeAPhoneCall(mContext, mConnection.mobile);
		}else if(v == mView.lnIsConnected){

			Bundle bundle = new Bundle();
			bundle.putSerializable(Constants.KEY_CONNECTION,mConnection);
			((MainActivity) mContext).switchFragment(Constants.FRAGMENT_CONNECTION,Constants.ANIMATION_VIEW_TYPE_ADD,bundle);
			//((MainActivity) mContext).swapFragment(Constants.FRAGMENT_USER_CONNECTION, bundle);

		} else if(v == mView.lnConnectionStatusLeader) {

			//Add connection request
			requestConnectionType(mConnection);

		}
	}

	private void requestConnectionType(final ConnectionModel connectionRequestModel) {

		final int connType;
		if (connectionRequestModel.accessLevel == Constants.USER_ACCESS_LEVEL_NORMAL){

			//If user requesting the connection is Admin or Manager prompt him about permission type
			if (SBApplication.getUserModel().accessLevel == Constants.USER_ACCESS_LEVEL_MANAGER ||
					SBApplication.getUserModel().accessLevel == Constants.USER_ACCESS_LEVEL_LEADER){

				connType = Constants.CONNECTION_TYPE_FOLLOWER;

			}else{

				connType = Constants.CONNECTION_TYPE_TEAM_MEMBER;

			}

		}else{

			//If user requesting the connection is Admin or Manager prompt him about permission type
			if (SBApplication.getUserModel().accessLevel == Constants.USER_ACCESS_LEVEL_MANAGER ||
					SBApplication.getUserModel().accessLevel == Constants.USER_ACCESS_LEVEL_LEADER){

				connType = Constants.CONNECTION_TYPE_LEADER_FOLLOWER;
			}else{
				connType = Constants.CONNECTION_TYPE_TEAM_LEADER;
			}

		}

		//Show dialog to choose if connection type is not Team Member request
		if (connType != Constants.CONNECTION_TYPE_TEAM_MEMBER){

			final RequestConnectionDialog requestConn = new RequestConnectionDialog(mContext, Constants.THEME);
			requestConn.show();
			requestConn
					.setContent(
							mContext.getResources().getString(R.string.msg_connection_request_title),
							mContext.getResources().getString(R.string.msg_connection_request_message),
							connType,
							mContext.getResources().getString(R.string.btn_ok_title),
							mContext.getResources().getString(R.string.btn_cancel_title), new IRequestConnectionDialog() {
								@Override
								public void onOk(int permission) {
									requestAddConnection(permission);
								}

								@Override
								public void onCancel() {
									// dissmiss and do nothing
								}
							});

		}else{

			//Request connection type Team Member
			requestAddConnection(1);

		}
	}

}
