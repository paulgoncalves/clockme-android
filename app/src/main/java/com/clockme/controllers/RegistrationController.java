package com.clockme.controllers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.activities.LoginActivity;
import com.clockme.interfaces.ILoading;
import com.clockme.interfaces.IWebservice;
import com.clockme.models.CKPersonModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.LoadingBeat;
import com.clockme.utilities.UtilDialog;
import com.clockme.utilities.Utilities;

import org.json.JSONObject;

public class RegistrationController extends AbstractController implements OnClickListener {

	private Context mContext;
	private RegistrationView mView;
	private String repeatPassword = "";
	private CKPersonModel userInfo = new CKPersonModel();
	private LoadingBeat loadingBeat;

	public RegistrationController(Context context, RegistrationView view) {
		this.mContext = context;
		this.mView = view;
	}

	@Override
	public void initData() {
		super.initData();
		loadingBeat = new LoadingBeat(mContext);
	}

	@Override
	public void setListener() {
		super.setListener();
		mView.rlRootView.setOnClickListener(this);
		mView.scrollView.setOnClickListener(this);
		mView.btnRegister.setOnClickListener(this);
		mView.btnCancel.setOnClickListener(this);
	}

	@Override
	public View getView() {
		return mView;
	}

	@Override
	public void onClick(View v) {
		if (v == mView.rlRootView || v == mView.scrollView) {
			Utilities.hideKeyboard(mContext, mView.btnRegister);
		} else if (v == mView.btnRegister) {
			Utilities.hideKeyboard(mContext, mView.btnRegister);
			int result = checkAllValidFields();
			afterCheckValidFields(result);
		} else if (v == mView.btnCancel) {
			Utilities.hideKeyboard(mContext, mView.btnCancel);
			Intent i = new Intent(mContext, LoginActivity.class);
			((Activity) mContext).startActivity(i);
			((Activity) mContext).finish();
			((Activity) mContext).overridePendingTransition(R.anim.animation_enter,
					R.anim.animation_leave);
		}
	}


	private void requestRegistration() {

		String body = String.format(WebservicesHelper.PARAMS_REGISTRATION, userInfo.name,userInfo.lastName, userInfo.email, userInfo.password,userInfo.mobile);
		WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_LOGIN_REGISTRATION, body);

	}

	private void requestLogin() {

		String body = String.format(WebservicesHelper.PARAMS_LOGIN, userInfo.email, userInfo.password);
		WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_LOGIN, body);

	}

	IWebservice IWebserviceRequest = new IWebservice() {

		@Override
		public void onComplete(JSONObject WSResponse, String WSMessage, int WSCode) {

			if (WSResponse.optInt("status") == 1) {

				if (WSCode == WebservicesHelper.WSCODE_REGISTER_SUCCESS) {

					//Login after user information is saved
					requestLogin();

				} else if (WSCode == WebservicesHelper.WSCODE_LOGIN_SUCCESS) {

					loadingBeat.cancelLoading();

					//Save user detail
					CKPersonModel.getInstance().getUserModelFromJson(WSResponse);

					//Create session
					SBApplication.getUserModel().session = true;

					//Update data saving to phone memory
					CKPersonModel.getInstance().updateUserInfo(mContext);

					//Switch user to Join Entity fragment
					((LoginActivity) mContext).SwitchToFragment(LoginActivity.FRAGMENT_JOIN_ENTITY);


//					//Take user to registartion wizard
//					mContext.startActivity(new Intent(mContext, JoinEntityActivity.class));
//					((Activity) mContext).finish();
//					((Activity) mContext).overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

				}

			}else{

				loadingBeat.cancelLoading();
				UtilDialog.showAlertWith1Button(
						mContext,
						mContext.getResources().getString(R.string.warning),
						WSMessage,
						mContext.getResources().getString(
								R.string.btn_ok_title), null);
			}
		}

		@Override
		public void onError(String error) {
			UtilDialog.showAlertWith1Button(mContext, "", error, mContext.getResources().getString(R.string.ok), null);
			Log.e("WS error =>", error);
			loadingBeat.cancelLoading();
		}

		@Override
		public void onAccessDenied(String WSMessage) {
			loadingBeat.cancelLoading();
			UtilDialog.showAlertWith1Button(mContext, "", WSMessage, mContext.getResources().getString(R.string.ok), null);
			CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
		}
	};

	ILoading iLoading = new ILoading() {

		@Override
		public void showLoading() {
			loadingBeat.showLoading();
		}

	};

	private int checkAllValidFields() {
		String firstName = mView.etFirstName.getText().toString().trim();
		if (TextUtils.isEmpty(firstName)) {
			return 1;
		}

		String lastName = mView.etLastName.getText().toString().trim();
		if (TextUtils.isEmpty(lastName)) {
			return 1;
		}

		String mobile = mView.etMobile.getText().toString().trim();
		if (TextUtils.isEmpty(mobile)) {
			return 1;
		}

		String email = mView.etEmail.getText().toString().trim();
		if (TextUtils.isEmpty(email)) {
			return 1;
		}

		if (!Utilities.checkEmailFormat(email)) {
			return 2;
		}

		String password = mView.etPassword.getText().toString().trim();
		if (TextUtils.isEmpty(password)) {
			return 1;
		}

		repeatPassword = mView.etRepeatPassword.getText().toString().trim();
		if (TextUtils.isEmpty(repeatPassword)) {
			return 1;
		}

		if (!password.equals(repeatPassword)) {
			return 3;
		}

		userInfo.name = firstName;
		userInfo.lastName = lastName;
		userInfo.mobile = mobile;
		userInfo.email = email;
		userInfo.password = password;

		return 0;
	}

	private void afterCheckValidFields(int result) {
		switch (result) {
		case 0:
			requestRegistration();
			break;
		default: {
			String message = "";
			switch (result) {
			case 1:
				message = mContext.getResources().getString(R.string.error_empty_field);
				break;
			case 2:
				message = mContext.getResources().getString(R.string.error_invalid_email);
				break;
			case 3:
				message = mContext.getResources().getString(R.string.error_matched_password);
				break;
			}
			UtilDialog.showAlertWith1Button(mContext, null, message, mContext
					.getResources().getString(R.string.btn_cancel_title), null);
		}
			break;
		}
	}
}
