package com.clockme.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Display;

import com.clockme.R;
import com.clockme.utilities.SharePref;

public class SplashActivity extends Activity{

	Intent i;
    Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        mContext = this;
		setContentView(R.layout.activity_splash);

		if (SharePref.getScreenWidth(this) == 0) {
			Display display = this.getWindowManager().getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			SharePref.setScreenWidth(this, size.x);
		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		MainCountDown countDown = new MainCountDown(600, 100);
		countDown.start();
	}

	private void switchScreen() {
		i = new Intent(SplashActivity.this, MainActivity.class);
		startActivity(i);
		finish();
	}

	protected class MainCountDown extends CountDownTimer {

		public MainCountDown(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {
			switchScreen();
		}

		@Override
		public void onTick(long millisUntilFinished) {
			//
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.gc();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		android.os.Process.killProcess(android.os.Process.myPid());
	}

}
