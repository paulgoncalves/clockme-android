package com.clockme.activities;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.fragments.ActionNoteAddFragment;
import com.clockme.fragments.HomeFragment;
import com.clockme.fragments.MenuFragment;
import com.clockme.fragments.MyTeamFragment;
import com.clockme.fragments.ProfileFragment;
import com.clockme.fragments.SheduleFragment;
import com.clockme.helpers.FragmentHelper;
import com.clockme.helpers.LocationHelper;
import com.clockme.interfaces.IChangeAvatar;
import com.clockme.interfaces.IGetLocation;
import com.clockme.models.CKPersonModel;
import com.clockme.models.LoginModel;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.Constants;
import com.clockme.views.MainView;
import com.google.android.gms.maps.model.LatLng;
import java.io.File;

public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private Context mContext;
    private MainView view;
    private FragmentManager mFragmentManager;
    private FragmentTransaction fmTransaction;
    private Fragment mFragment = null;
    private String mFragmentTag;
    public static int FRAGMENT_INDEX = 0;
    private File mFileTemp;
    private IChangeAvatar callBack;
    private boolean isPause = false;

    public void setCallBack(IChangeAvatar callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        SBApplication.setMainActivity(this);
        mFragmentManager = getSupportFragmentManager();
        fmTransaction = mFragmentManager.beginTransaction();

        mContext = this;
        view = new MainView(mContext);
        setContentView(view);

        //Load main objects
        CKPersonModel.getInstance().initSession(this);

        //If simulator set location manually
        if(WebservicesHelper.BASE_URL.contains("10.0.2.2")){
            SBApplication.getUserModel().location = new LatLng(Constants.DEFAULT_LAT,Constants.DEFAULT_LNG);
        }else{
            //Init location service
            LocationHelper.getInstance(mContext).addIGetLocationListener(iGetLocation);
        }

        if (SBApplication.getUserModel().session){

            //FRAGMENT_INDEX = Constants.FRAGMENT_HOME;
            switchFragment(Constants.FRAGMENT_HOME, Constants.ANIMATION_VIEW_TYPE_ADD, null);

            //Check user updated the version
            LoginModel.getInstance(mContext).checkPushToken();

        } else {
            CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (SBApplication.getUserModel().personId == 0){

            CKPersonModel.getInstance().initSession(this);

        }else if (SBApplication.getUserModel().session) {

            //openPushActivity();

        }else{
            CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
        }
    }

    /**
     * TODO: need to look at site notes, site log etc... not closing properly
     * @param position
     * @param bundle
     */
    public void switchFragment(final int position, final int animation, final Bundle bundle) {

        //Stop auto call back for new site on HomeController
        if (FRAGMENT_INDEX == 0 && position != 0){
            HomeFragment.getInstance().getController().stopCallBacks();
        }

        //Get mFragment by view
        mFragment = setFragmentSetting(position);

        //Replace the main stack with new view
        FragmentHelper.switchFragment(mFragmentManager, mFragment, mFragmentTag, animation, R.id.fragmentContainer, bundle);

        //Set the mFragment index after switching
        FRAGMENT_INDEX = position;

    }

    /**
     * TODO: need to look at site notes, site log etc... not closing properly
     * @param position
     * @param bundle
     */
    public void swapFragment(final int position, final int animation, final Bundle bundle) {

        //Stop auto call back for new site on HomeController
        if (FRAGMENT_INDEX == 0 && position != 0){
            HomeFragment.getInstance().getController().stopCallBacks();
        }

        //Get mFragment by view
        mFragment = setFragmentSetting(position);

        //Replace the main stack with new view
        FragmentHelper.swapFragment(mFragmentManager, mFragment, mFragmentTag, animation, R.id.fragmentContainer, bundle);

        //Set the mFragment index after switching
        //FRAGMENT_INDEX = position;

    }

    /**
     * Display CreateHazard to begin the wizard for creating a new Hazard
     * @param pos
     */
    public void addFragment(int pos, int animationType, Bundle bundle) {

        mFragment = setFragmentSetting(pos);

        FragmentHelper.addFragment(
                mFragmentManager,
                mFragment,
                animationType,
                bundle,
                R.id.fragmentContainer,
                mFragmentTag);
    }

    public void popFragment() {

        //set fragment based on last view
        mFragment = setFragmentSetting(FRAGMENT_INDEX);

        //Dismiss mFragment and take user back to previous mFragment
        FragmentHelper.popToRootFragment(getSupportFragmentManager());

        //Call on resume again
        mFragment.onResume();
    }

    public void popToRootFragment() {

        //set fragment based on last view
        mFragment = setFragmentSetting(FRAGMENT_INDEX);

        //Dismiss mFragment and take user back to previous mFragment
        FragmentHelper.popToRootFragment(getSupportFragmentManager());

        //Call on resume again
        mFragment.onResume();

    }

    public Fragment setFragmentSetting(int position){

        switch (position) {
            case Constants.FRAGMENT_HOME:
                mFragmentTag = getResources().getString(R.string.tag_fragment_home);
                return HomeFragment.getInstance();
            case Constants.FRAGMENT_LOGIN:
                mFragmentTag = getResources().getString(R.string.tag_fragment_team_status);
                return MyTeamFragment.getInstance();
            case Constants.FRAGMENT_MENU:
                mFragmentTag = getResources().getString(R.string.tag_fragment_menu);
                return MenuFragment.getInstance();
            case Constants.FRAGMENT_PROFILE:
                mFragmentTag = getResources().getString(R.string.tag_fragment_user_profile);
                return ProfileFragment.getInstance();
            case Constants.FRAGMENT_SCHEDULE:
                mFragmentTag = getResources().getString(R.string.tag_fragment_schedule);
                return SheduleFragment.getInstance();
            case Constants.FRAGMENT_TEAM_STATUS:
                mFragmentTag = getResources().getString(R.string.tag_fragment_team_status);
                return MyTeamFragment.getInstance();
            case Constants.FRAGMENT_ACTION_NOTE_ADD:
                mFragmentTag = getResources().getString(R.string.tag_fragment_action_note_add);
                return ActionNoteAddFragment.getInstance();
            default:
                return HomeFragment.getInstance();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    IGetLocation iGetLocation = new IGetLocation() {
        @Override
        public void onLocationListener(Location location) {
            SBApplication.getUserModel().location = new LatLng(location.getLatitude(),location.getLongitude());
            CKPersonModel.getInstance().updateUserInfo(mContext);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() - called");
        //LocationHelper.getInstance(mContext).removeIGetLocationListener(iGetLocation);
        //LocationHelper.getInstance(mContext).onDestroyGetLocation();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1){
            super.onBackPressed();
            popFragment();
        }else{
            moveTaskToBack(true);
        }
    }
}