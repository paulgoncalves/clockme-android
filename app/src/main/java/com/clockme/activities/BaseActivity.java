package com.clockme.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.clockme.R;
import com.clockme.fragments.HomeFragment;
import com.clockme.helpers.FragmentHelper;
import com.clockme.utilities.Constants;

public class BaseActivity extends FragmentActivity{

	protected Fragment mFragment;
	protected FragmentManager mFragmentManager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// set the Behind View
		if (savedInstanceState == null) {
			mFragmentManager = this.getSupportFragmentManager();
			mFragment = new HomeFragment();
			FragmentHelper.addFragment(mFragmentManager, mFragment, Constants.ANIMATION_VIEW_TYPE_FADE, null, Constants.FRAGMENT_HOME, getResources().getString(R.string.tag_fragment_home));
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

}
