package com.clockme.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.controllers.LoginController;
import com.clockme.fragments.LoginFragment;
import com.clockme.helpers.FragmentHelper;
import com.clockme.utilities.Constants;
import com.clockme.views.LoginActivityView;

public class LoginActivity extends BaseActivity{

	private Context context;
	private LoginActivityView view;
	private LoginController controller;

	private FragmentManager fmManager;
	private FragmentTransaction fmTransaction;
	private Fragment fragment;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SBApplication.setLoginActivity(LoginActivity.this);

		context = LoginActivity.this;
		view = new LoginActivityView(context);
		SwitchToFragment();
		setContentView(view);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	public void SwitchToFragment(){

		//Swipe fragments but remain on registration controller
		fmManager = getSupportFragmentManager();
		fmTransaction = fmManager.beginTransaction();
		fragment = LoginFragment.getInstance();

		FragmentHelper.switchFragment(fmManager,fragment,getResources().getString(R.string.tag_fragment_login), Constants.ANIMATION_VIEW_TYPE_FADE,Constants.FRAGMENT_LOGIN,null);

	}

	@Override
	protected void onStart() {
		super.onStart();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		SBApplication.destroy();
		finish();
	}

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SBApplication.setLoginActivity(null);
    }
}
