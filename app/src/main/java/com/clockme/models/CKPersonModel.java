package com.clockme.models;

import android.content.Context;
import android.util.Log;

import com.clockme.SBApplication;
import com.clockme.controllers.ActionController;
import com.clockme.utilities.SharePref;
import com.clockme.utilities.Utilities;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.TimeZone;

public class CKPersonModel {

	private static CKPersonModel instance = null;
	public boolean session,leftGeofence;
	public int personId,accessLevel;
	public String token,email,name,mobile,timezone,deviceToken;
	public LatLng location;

	public int badge,badgeMessage,badgeInDanger;

	public static CKPersonModel getInstance() {
		if (instance == null) {
			instance = new CKPersonModel();
		}
		return instance;
	}

	public CKPersonModel() { }

	/*
	* Build session
	*/
	public void initSession(Context context) {

		if (SBApplication.getUserModel() == null){
			SBApplication.gotoLoginPage();
		}else{

			try {
				//Load user model from phone memory
				SBApplication.setUserModel(getUserModelFromJson(new JSONObject(SharePref.getUserInfo(context))));
				Log.e("Loaded: ", " finished loading user model from memory");
			}catch (JSONException exx){
				Log.e("Error: ", " error getting memory object");
			}

			try {
				//Load entity model from phone memory
				SBApplication.setEntityModel(CKCompanyModel.getInstance().getEntityModelFromJson(new JSONObject(SharePref.getEntityInfo(context))));
				Log.e("Loaded: ", " finished loading entity model from memory");
			}catch (JSONException exx){
				if (SBApplication.getEntityModel() == null){
					SBApplication.setEntityModel(new CKCompanyModel());
				}
				Log.e("Error: ", " error getting memory object");
			}

			try {
				//Load site model from phone memory
				SBApplication.setSiteModel(CKPlaceModel.getInstance().getSiteModelFromJson(new JSONObject(SharePref.getSiteInfo(context))));
				Log.e("Loaded: ", " finished loading site model from memory");
			}catch (JSONException exx){

				if (SBApplication.getSiteModel() == null){
					SBApplication.setSiteModel(new CKPlaceModel());
				}
				Log.e("Error: ", " error getting memory object");
			}
		}
	}


	public CKPersonModel getUserModelFromJson(JSONObject userInfo) {

		this.session = Utilities.getBooleanValue(userInfo, "session");
		this.personId = Utilities.getIntValue(userInfo, "personId");
		this.name = Utilities.getStringValue(userInfo, "companyName");
		this.token = Utilities.getStringValue(userInfo,"token");
		this.email = Utilities.getStringValue(userInfo,"email");
		this.mobile = Utilities.getStringValue(userInfo,"mobile");
		this.accessLevel = Utilities.getIntValue(userInfo, "accessLevel");
		this.timezone = TimeZone.getDefault().getID();

		Log.e("User model object: ", " =>" + this);
		//SBApplication.setUserModel(this);
		return this;
	}

	public void updateUserInfo(Context context) {
		try {

			JSONObject jsonData = new JSONObject();
			jsonData.put("session", Utilities.getBooleanString(SBApplication.getUserModel().session));
			jsonData.put("personId", SBApplication.getUserModel().personId);
			jsonData.put("companyName", SBApplication.getUserModel().name);
			jsonData.put("token", SBApplication.getUserModel().token);
			jsonData.put("email", SBApplication.getUserModel().email);
			jsonData.put("mobile", SBApplication.getUserModel().mobile);
			jsonData.put("accessLevel", SBApplication.getUserModel().accessLevel);
			jsonData.put("timezone", SBApplication.getUserModel().timezone);

			Log.d("Updated user model: ", " =>" + jsonData.toString());
			SharePref.setUserInfo(context,jsonData.toString());

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	public void logOffUserWithAccessDenied(Context context){

		ActionController.getInstance().UserCheckoutSuccess(context);

		SBApplication.getUserModel().session = false;
		SBApplication.getUserModel().token = "";
		CKPersonModel.getInstance().updateUserInfo(context);

		//Take user back to main screen
		SBApplication.gotoLoginPage();
	}

}
