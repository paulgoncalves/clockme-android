package com.clockme.models;

import com.clockme.utilities.Utilities;
import com.google.android.gms.maps.model.LatLng;
import org.json.JSONObject;

/**
 * Created by AppDev on 25/08/15.
 * this model represents SiteNotes
 */
public class NotesModel {

    public int actionId,personId,placeId,companyId,actionType;
    public String timezone,placeName;
    public LatLng location;
    public double time;
    public boolean onRadius;

    //Action note
    public int actionNoteId;
    public String note,noteTimezone;
    public double notetime;

    public NotesModel() {}

    public NotesModel getActionNoteModelFromJson(JSONObject noteObj){

        this.actionId = Utilities.getIntValue(noteObj, "actionId");
        this.personId = Utilities.getIntValue(noteObj, "personId");
        this.placeId = Utilities.getIntValue(noteObj, "placeId");
        this.companyId = Utilities.getIntValue(noteObj, "companyId");
        this.actionType = Utilities.getIntValue(noteObj, "actionType");
        this.placeName = Utilities.getStringValue(noteObj, "placeName");
        this.timezone = Utilities.getStringValue(noteObj, "timezone");
        this.time = Utilities.getDoubleValue(noteObj, "time");
        this.onRadius = Utilities.getBooleanValue(noteObj, "onRadius");
        this.location = new LatLng(Utilities.getDoubleValue(noteObj,"lat"),Utilities.getDoubleValue(noteObj,"lng"));

        //Action note
        this.actionNoteId = Utilities.getIntValue(noteObj, "actionId");
        this.note = Utilities.getStringValue(noteObj, "note");
        this.noteTimezone = Utilities.getStringValue(noteObj, "noteTimezone");
        this.notetime = Utilities.getDoubleValue(noteObj, "noteTime");

        return this;

    }


}
