package com.clockme.models;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AddressModel {

	public String street_number = "";
	public String route = "";
	public String sublocality = "";
	public String locality = "";
	public String administrative_area_level_2 = "";
	public String administrative_area_level_1 = "";
	public String postal_code = "";
	public String country = "";
	public LatLng coordinate;

	public AddressModel() {

	}


	public AddressModel getAddressModelFromJson(JSONArray arrayAddress){

		for (int i = 0; i < arrayAddress.length(); i++) {

			try {

				JSONObject addressObj = arrayAddress.getJSONObject(i);

				if (addressObj.optString("types").toString().contains("street_number")) {
					this.street_number = addressObj.optString("long_name");
				} else if (addressObj.optString("types").toString().contains("route")) {
					this.route = addressObj.optString("long_name");
				} else if (addressObj.optString("types").toString().contains("sublocality")) {
					this.sublocality = addressObj.optString("long_name");
				} else if (addressObj.optString("types").toString().contains("locality")) {
					this.locality = addressObj.optString("long_name");
				} else if (addressObj.optString("types").toString().contains("administrative_area_level_2")) {
					this.administrative_area_level_2 = addressObj.optString("long_name");
				} else if (addressObj.optString("types").toString().contains("administrative_area_level_1")) {
					this.administrative_area_level_1 = addressObj.optString("long_name");
				} else if (addressObj.optString("types").toString().contains("postal_code")) {
					this.postal_code = addressObj.optString("long_name");
				} else if (addressObj.optString("types").toString().contains("country")) {
					this.country = addressObj.optString("long_name");
				}

			}catch (JSONException ex){
				ex.printStackTrace();
			}

		}

		return this;

	}




}
