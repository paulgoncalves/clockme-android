package com.clockme.models;

import android.content.Context;
import android.util.Log;

import com.clockme.SBApplication;
import com.clockme.utilities.SharePref;
import com.clockme.utilities.Utilities;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.Serializable;

public class CKPlaceModel implements Serializable{

	private static CKPlaceModel instance = null;
	public String name,stName,stNumber,suburb,state;
	public int actionId,placeId,radius;
	public double usedTime,distance;
	public LatLng siteLocation;
	public boolean isOffsite,isClockin;

	public CKPlaceModel(){}

	public static CKPlaceModel getInstance() {
		if (instance == null) {
			instance = new CKPlaceModel();
		}
		return instance;
	}

	public CKPlaceModel getSiteModelFromJson(JSONObject siteInfo){

		this.placeId = Utilities.getIntValue(siteInfo, "placeId");
		this.actionId = Utilities.getIntValue(siteInfo, "actionId");
		this.radius = Utilities.getIntValue(siteInfo, "radius");
		this.name = Utilities.getStringValue(siteInfo, "placeName");
		this.stName = Utilities.getStringValue(siteInfo, "stName");
		this.stNumber = Utilities.getStringValue(siteInfo, "stNumber");
		this.suburb = Utilities.getStringValue(siteInfo, "suburb");
		this.state = Utilities.getStringValue(siteInfo, "state");

		this.usedTime = Utilities.getDoubleValue(siteInfo, "usedTime");
		this.distance= Utilities.getDoubleValue(siteInfo, "distance");

		this.siteLocation = new LatLng(Utilities.getDoubleValue(siteInfo, "lat"),Utilities.getDoubleValue(siteInfo,"lng"));

		this.isOffsite = Utilities.getBooleanValue(siteInfo,"isOffsite");
		this.isClockin = Utilities.getBooleanValue(siteInfo, "isClockin");

		//Log.e("Site model object: ", " =>" + this);
		//SBApplication.setSiteModel(this);
		return this;
	}

	public void updateSiteInfo(Context context) {
		try {

			JSONObject jsonData = new JSONObject();
			jsonData.put("placeId", Utilities.validateInt(SBApplication.getSiteModel().placeId));
			jsonData.put("actionId", Utilities.validateInt(SBApplication.getSiteModel().actionId));
			jsonData.put("radius", Utilities.validateInt(SBApplication.getSiteModel().radius));

			jsonData.put("placeName", Utilities.validateString(SBApplication.getSiteModel().name));
			jsonData.put("stName", Utilities.validateString(SBApplication.getSiteModel().stName));
			jsonData.put("stNumber", Utilities.validateString(SBApplication.getSiteModel().stNumber));
			jsonData.put("suburb", Utilities.validateString(SBApplication.getSiteModel().suburb));
			jsonData.put("state", Utilities.validateString(SBApplication.getSiteModel().state));


			jsonData.put("usedTime", Utilities.validateDouble(SBApplication.getSiteModel().usedTime));
			jsonData.put("distance", Utilities.validateDouble(SBApplication.getSiteModel().distance));
			jsonData.put("lat", Utilities.validateDouble(SBApplication.getSiteModel().siteLocation.latitude));
			jsonData.put("lng", Utilities.validateDouble(SBApplication.getSiteModel().siteLocation.longitude));

			jsonData.put("isOffsite", Utilities.getBooleanString(SBApplication.getSiteModel().isOffsite));
			jsonData.put("isClockin", Utilities.getBooleanString(SBApplication.getSiteModel().isClockin));

			Log.d("Updated site model: ", " =>" + jsonData.toString());
			//missing: mobileEmergency, session, leftGeofence, location
			SharePref.setSiteInfo(context, jsonData.toString());

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

}
