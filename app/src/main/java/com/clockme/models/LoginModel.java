package com.clockme.models;

import android.content.Context;
import android.text.TextUtils;

import com.clockme.SBApplication;
import com.clockme.interfaces.ILoading;
import com.clockme.interfaces.IWebservice;
import com.clockme.networking.WebservicesHelper;
import com.clockme.utilities.Constants;
import com.clockme.utilities.DateTimeUtils;
import com.clockme.utilities.SharePref;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by clockme on 12/10/15
 */
public class LoginModel {

    private static LoginModel instance = null;
    private static Context mContext = null;
    private GoogleCloudMessaging gcm;
    protected LoginModel() {}

    public static LoginModel getInstance(Context context) {
        if (instance == null) {
            instance = new LoginModel();
        }
        mContext = context;
        return instance;
    }

    /**
     * Check for the new token and update both local and server if changed
     */
    public void checkPushToken() {

        String newToken = getTokenPush();
        String token_push = SharePref.getTokenPush(mContext);

        if (TextUtils.isEmpty(token_push) && !token_push.equals(newToken)) {

            //Save the new updated token
            SharePref.setTokenPush(mContext, newToken);

            //Save the token to webservice
            requestUpdateDevice(newToken);
        }
    }

    /**
     * Get the new token push
     * @return
     */
    public String getTokenPush(){

        String regid = "";

        if (gcm == null) {
            gcm = GoogleCloudMessaging.getInstance(mContext);

            try {
                return gcm.register(Constants.PROJECT_NUMBER);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return regid;
    }

    private void requestUpdateDevice(String newToken) {

        String body = String.format(WebservicesHelper.PARAMS_REGISTRATION_UPDATE_DEVICE, SBApplication.getUserModel().personId, SBApplication.getUserModel().token,
                newToken, DateTimeUtils.getCurrentTimeStamp(),SBApplication.getUserModel().timezone);
        WebservicesHelper.getInstance().webserviceRequest(mContext, iLoading, IWebserviceRequest, WebservicesHelper.ROUTE_USER_UPDATE_DEVICE, body);

    }

    IWebservice IWebserviceRequest = new IWebservice() {

        @Override
        public void onComplete(JSONObject WSResponse, String WSMessage, int WSCode) {

            if (WSResponse.optInt("status") == 1) {

                if (WSCode == WebservicesHelper.WSCODE_USER_TOKEN_UPDATE_SUCCESS) {

                    //Do nothing, update will not require call back
                }
            }
        }

        @Override
        public void onError(String error) {
        }

        @Override
        public void onAccessDenied(String WSMessage) {
            CKPersonModel.getInstance().logOffUserWithAccessDenied(mContext);
        }
    };

    ILoading iLoading = new ILoading() {

        @Override
        public void showLoading() {
        }
    };

}
