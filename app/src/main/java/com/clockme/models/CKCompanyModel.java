package com.clockme.models;

import android.content.Context;
import android.util.Log;
import com.clockme.SBApplication;
import com.clockme.utilities.SharePref;
import com.clockme.utilities.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

public class CKCompanyModel {

	private static CKCompanyModel instance = null;
	public int companyId,adminId;
	public String companyName;
	public double normalStartTime,normalEndTime,afterHourRate;

	public static CKCompanyModel getInstance() {
		if (instance == null) {
			instance = new CKCompanyModel();
		}
		return instance;
	}

	public CKCompanyModel(){};

	public CKCompanyModel getEntityModelFromJson(JSONObject entityInfo){

		this.companyId = Utilities.getIntValue(entityInfo, "companyId");
		this.adminId = Utilities.getIntValue(entityInfo, "adminId");
		this.companyName = Utilities.getStringValue(entityInfo, "companyName");
		this.normalStartTime = Utilities.getDoubleValue(entityInfo, "nomalStartTime");
		this.normalEndTime = Utilities.getDoubleValue(entityInfo, "nomalEndTime");
		this.afterHourRate = Utilities.getDoubleValue(entityInfo, "afterHourRate");

		return this;

	}

	public static void updateEntityInfo(Context context) {
		try {

			JSONObject jsonData = new JSONObject();
			jsonData.put("companyId", SBApplication.getEntityModel().companyId);
			jsonData.put("adminId", SBApplication.getEntityModel().adminId);
			jsonData.put("companyName", SBApplication.getEntityModel().companyName);
			jsonData.put("normalStartTime", SBApplication.getEntityModel().normalStartTime);
			jsonData.put("normalEndTime", SBApplication.getEntityModel().normalEndTime);
			jsonData.put("afterHourRate", SBApplication.getEntityModel().afterHourRate);

			//Log.e("jsonData: ", " =>" + jsonData.toString());
			Log.d("Updated entity model: ", " =>" + jsonData.toString());
			SharePref.setEntityInfo(context, jsonData.toString());

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

}
