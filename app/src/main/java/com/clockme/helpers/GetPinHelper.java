package com.clockme.helpers;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;

import com.clockme.R;

public class GetPinHelper {

	private static GetPinHelper instance = null;
	private static Context ctx;
	private Drawable dPin;
	private Resources res;
	private int listIcons;
	private TypedArray arrIcons;
	private SparseArray<TypedArray> hmIcons = new SparseArray<TypedArray>();

	private static final int STATUS_GREY = 0;
	private static final int STATUS_GREEN = 1;
	private static final int STATUS_RED = 2;
	private static final int STATUS_YELLOW = 3;

	protected GetPinHelper() {
		initData();
	}

	public static GetPinHelper getInstance(Context context) {
		if (instance == null) {
			ctx = context;
			instance = new GetPinHelper();
		}
		return instance;
	}

	private void initData() {
		res = ctx.getResources();
		listIcons = R.array.green_status_icons;
		arrIcons = res.obtainTypedArray(listIcons);
		hmIcons.put(STATUS_GREEN, arrIcons);

		listIcons = R.array.red_status_icons;
		arrIcons = res.obtainTypedArray(listIcons);
		hmIcons.put(STATUS_RED, arrIcons);

		listIcons = R.array.yellow_status_icons;
		arrIcons = res.obtainTypedArray(listIcons);
		hmIcons.put(STATUS_YELLOW, arrIcons);
	}

	public Drawable getPin(int checkinTypeId, int status) {

		switch (checkinTypeId) {
		case 1:
			dPin = res.getDrawable(R.mipmap.pin_checkin_green);
			break;
		case 2:
			dPin = res.getDrawable(R.mipmap.pin_reportin_green);
			break;
		case 3:
		case 8:
			dPin = res.getDrawable(R.mipmap.pin_inactive_grey);
			break;
		case 4:
		case 5:
		case 6:
		case 7:
		case 9:
			dPin = res.getDrawable(R.mipmap.pin_pulse_yellow);
			break;
		case 10:
		case 11:
			dPin = res.getDrawable(R.mipmap.pin_pulse_green);
			break;
		default:
			dPin = res.getDrawable(R.mipmap.pin_inactive_grey);
			break;
		}
		return dPin;
	}

	public int getPinResourceId(int checkinTypeId, int status) {
		int result = 0;
		if (status == STATUS_GREY) {
			result = R.mipmap.pin_inactive_grey;
		} else {
			result = R.mipmap.pin_inactive_grey;
		}
		return result;
	}

}
