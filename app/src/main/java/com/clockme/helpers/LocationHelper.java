package com.clockme.helpers;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.clockme.SBApplication;
import com.clockme.interfaces.IGetLocation;
import com.clockme.utilities.Constants;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class LocationHelper {

	private Context context;
	public static LocationHelper instance;
	private LocationManager locationManager;
	private GetLocationListener gllGPS, gllNetwork;
	private boolean gps_enabled = false, network_enabled = false;
	private ArrayList<IGetLocation> iGetLocationArr = new ArrayList<IGetLocation>();

	public static LocationHelper getInstance(Context ctx) {
		if (instance == null) {
			instance = new LocationHelper(ctx);
		}
		return instance;
	}
	
	public LocationHelper(Context ctx) {

		context = ctx;
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

		gllGPS = new GetLocationListener();
		gllNetwork = new GetLocationListener();

		initLocation();
		requestGetLocation();
	}

	private void initLocation() {
		try {
			gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		if (!gps_enabled && !network_enabled) {
			SBApplication.getUserModel().location = new LatLng(Constants.DEFAULT_LAT, Constants.DEFAULT_LNG);
		} else {
			Location lastLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			if (lastLocation != null) {
				SBApplication.getUserModel().location = new LatLng(lastLocation.getLatitude(),lastLocation.getLongitude());
			} else {
				lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				if (lastLocation != null) {
					SBApplication.getUserModel().location = new LatLng(lastLocation.getLatitude(),lastLocation.getLongitude());
				} else {
					SBApplication.getUserModel().location = new LatLng(Constants.DEFAULT_LAT, Constants.DEFAULT_LNG);
				}
			}
		}
	}

	public void addIGetLocationListener(IGetLocation iGetLocation) {
		iGetLocationArr.add(iGetLocation);
	}

	public void removeIGetLocationListener(IGetLocation iGetLocation) {
		iGetLocationArr.remove(iGetLocation);
	}

	public void requestGetLocation() {
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,Constants.SECONDS_IN_A_MINUTE * 5 * 1000, 0, gllGPS);
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, Constants.SECONDS_IN_A_MINUTE * 5 * 1000, 0, gllNetwork);
	}

	public class GetLocationListener implements LocationListener {

		public void onLocationChanged(final Location loc) {
			if (loc != null) {
				Log.e("GET LOCATION", "=> Lat: " + loc.getLatitude() + " --- Lng: " + loc.getLongitude() + " --- from: " + loc.getProvider());

				SBApplication.getUserModel().location = new LatLng(loc.getLatitude(),loc.getLongitude());

				//Update usermodel location
				SBApplication.getUserModel().updateUserInfo(context);

				for (IGetLocation iGetLocation : iGetLocationArr) {
					iGetLocation.onLocationListener(loc);
				}
			} else {
				Log.e("GET LOCATION", "=> NULL");
			}
		}

		public void onProviderDisabled(String provider) {

		}

		public void onProviderEnabled(String provider) {

		}

		public void onStatusChanged(String provider, int status, Bundle extras) {

		}

	}

	public void onDestroyGetLocation() {
		locationManager.removeUpdates(gllGPS);
		locationManager.removeUpdates(gllNetwork);
	}

}
