package com.clockme.helpers;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.clockme.R;
import com.clockme.utilities.Constants;

/**
 * Created by AppDev on 4/10/15.
 */
public class FragmentHelper {

    private static final String TAG = FragmentHelper.class.getSimpleName();

    private static int mEnterAnimation;
    private static int mExitAnimation;
    private static int mPopEnterAnimation;
    private static int mPopExitAnimation;

    public FragmentHelper() {

    }

    public static void addFragment(FragmentManager fragmentManager, Fragment fragment, int animationType,Bundle args, int containerId, String tag) {

        // check for bundle
        if(args != null) {
            fragment.setArguments(args);
        }

        setFragmentAnimation(animationType);

        fragmentManager.beginTransaction()
                .setCustomAnimations(mEnterAnimation, mExitAnimation, mPopEnterAnimation, mPopExitAnimation)
                .add(containerId, fragment, tag)
                .addToBackStack(tag)
                .commit();
    }

    /**
     * this clears the back stack
     * @param fragmentManager
     * @param fragment
     * @param tag
     * @param animationType
     * @param containerId
     * @param args
     */
    public static void switchFragment(FragmentManager fragmentManager, Fragment fragment, String tag, int animationType, int containerId, Bundle args) {

        // call helper method to set the animation for fragment transaction
        /** must call this before we use set the animation with fragment manager **/
        setFragmentAnimation(animationType);

        clearBackstack(fragmentManager);

        //Add bundle
        if (args != null){
            fragment.setArguments(args);
        }

        // begin fragment transaction, replace current fragment with new fragment,
        // add fragment tag to back stack, set custom animation and commit transaction
        fragmentManager.beginTransaction()
                .setCustomAnimations(mEnterAnimation, mExitAnimation, mPopEnterAnimation, mPopExitAnimation)
                .replace(containerId, fragment)
                .addToBackStack(tag)
                .commit();

        Log.d(TAG, "swapFragment() - " + tag + " added to back stack");
    }

    public static void swapFragment(
            FragmentManager fragmentManager,
            Fragment fragment,
            String tag,
            int animationType,
            int containerId,
            Bundle args) {

        // call helper method to set the animation for fragment transaction
        /** must call this before we use set the animation with fragment manager **/
        setFragmentAnimation(animationType);

        //Add bundle
        if (args != null){
            fragment.setArguments(args);
        }

        // begin fragment transaction, replace current fragment with new fragment,
        // add fragment tag to back stack, set custom animation and commit transaction
        fragmentManager.beginTransaction()
                .setCustomAnimations(mEnterAnimation, mExitAnimation, mPopEnterAnimation, mPopExitAnimation)
                .replace(containerId, fragment)
                .addToBackStack(tag)
                .commit();

        Log.d(TAG, "swapFragment() - " + tag + " added to back stack");
    }

    /**
     * set the animation for the fragment
     * @param type
     */
    private static void setFragmentAnimation(int type) {

        switch(type) {
            case Constants.ANIMATION_VIEW_TYPE_ADD:
                mEnterAnimation = R.anim.animation_enter;
                mExitAnimation = R.anim.animation_leave;
                mPopEnterAnimation = R.anim.push_left_in;
                mPopExitAnimation = R.anim.push_left_out;
                break;
            case Constants.ANIMATION_VIEW_TYPE_SLIDE:
                mEnterAnimation = R.anim.push_left_in;
                mExitAnimation = R.anim.push_left_out;
                mPopEnterAnimation = R.anim.animation_enter;
                mPopExitAnimation = R.anim.animation_leave;
                break;
            case Constants.ANIMATION_VIEW_TYPE_FADE:
                mEnterAnimation = R.anim.abc_fade_in;
                mExitAnimation = R.anim.abc_fade_out;
                mPopEnterAnimation = R.anim.abc_fade_in;
                mPopExitAnimation = R.anim.abc_fade_out;
                break;
            case Constants.ANIMATION_VIEW_TYPE_DISMISS:
                mEnterAnimation = R.anim.animation_enter;
                mExitAnimation = R.anim.animation_leave;
                mPopEnterAnimation = R.anim.push_left_in;
                mPopExitAnimation = R.anim.push_left_out;
                break;
            case Constants.ANIMATION_VIEW_TYPE_PRESENT:
                mEnterAnimation = R.anim.abc_slide_in_bottom;
                mExitAnimation = R.anim.abc_slide_in_top;
                mPopEnterAnimation = R.anim.push_left_in;
                mPopExitAnimation = R.anim.push_left_out;
                break;
            default:

                break;
        }

    }


    /**
     * this pop back to the root - removes all fragments
     * @param fragmentManager
     */
    public static void popToRootFragment(FragmentManager fragmentManager) {

        int count = fragmentManager.getBackStackEntryCount();
        if (count > 0) {
            for (int i = 1; i < count; i++) {
                fragmentManager.popBackStack(i, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }
    }

    /**
     * this pops a single fragment
     * @param fragmentManager
     */
    public static void popFragment(FragmentManager fragmentManager) {

        //Set Animation type
        setFragmentAnimation(Constants.ANIMATION_VIEW_TYPE_FADE);

        //Start transaction to pop current fragment
        FragmentTransaction ft = fragmentManager.beginTransaction();
        fragmentManager.popBackStackImmediate();
        ft.setCustomAnimations(mEnterAnimation, mExitAnimation, mPopEnterAnimation, mPopExitAnimation);
        ft.commit();

    }

    public static void clearBackstack(FragmentManager mFragmentManager) {

               int count = mFragmentManager.getBackStackEntryCount();
        if (count > 0) {
            for (int i = 0; i < count; i++) {
                mFragmentManager.popBackStack(i, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }
    }

}
