package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.clockme.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;

import java.util.ArrayList;

public class HomeView extends AbstractRelativeLayout {

	public NavigationView navView;
    public TopPopupView topPopupView;
	public CheckInView viewCheckIn;
	public SiteCheckInView viewSiteCheckIn;
	public SafeSiteView viewSafeSite;
	public GoogleMap googleMap;
	public MapView map;
	public ProgressBar pbGetLocation;
	public ImageButton btnCurrentLocation;
	public Button btnCheckIn, btnSendAPulse, btnCreateSite, btnJoinSite;
	public ImageView blurCheckIn, blurBottom;
	public LinearLayout llTaskBar;
	public HomeHelpView helpView;
	public ListCategoryView listCategoryView;
	public ArrayList<View> targetsHelpNotCheckIn;
	public ArrayList<View> targetsHelpCheckIn;

	public HomeView(Context context) {
		super(context);
	}

	public HomeView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public int i = 0;

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (RelativeLayout) inflate(context, R.layout.fragment_home,this);
		llTaskBar = (LinearLayout) findViewById(R.id.llTaskBar);
		navView = (NavigationView) findViewById(R.id.navView);
		navView.btnLeft.setVisibility(VISIBLE);
		navView.btnRight.setText(getContext().getString(R.string.btn_help_title));
		navView.btnRight.setVisibility(VISIBLE);
		navView.tvTitle.setText(context.getResources().getString(R.string.tv_home_title));
		navView.tvTitle.setVisibility(VISIBLE);

        topPopupView = (TopPopupView) findViewById(R.id.popupTop);

		viewCheckIn = (CheckInView) findViewById(R.id.viewCheckIn);
		viewSiteCheckIn = (SiteCheckInView) findViewById(R.id.viewSiteCheckIn);
		viewSafeSite = (SafeSiteView) findViewById(R.id.viewSafeSite);
		map = (MapView) findViewById(R.id.mapView);

		pbGetLocation = (ProgressBar) findViewById(R.id.pbGetLocation);
		btnCurrentLocation = (ImageButton) findViewById(R.id.btnCurrentLocation);
		btnCheckIn = (Button) findViewById(R.id.btnCheckIn);
		btnSendAPulse = (Button) findViewById(R.id.btnSendAPulse);
		btnCreateSite = (Button) findViewById(R.id.btnCreateSite);
		btnJoinSite = (Button) findViewById(R.id.btnJoinSite);
		blurCheckIn = (ImageView) findViewById(R.id.imgBlur);
		blurBottom = (ImageView) findViewById(R.id.blurBottom);

		btnCheckIn.setEnabled(false);
		blurCheckIn.setVisibility(VISIBLE);

		helpView = (HomeHelpView)findViewById(R.id.helpView);
		helpView.setVisibility(GONE);


		targetsHelpNotCheckIn = new ArrayList<>();
		targetsHelpNotCheckIn.add(btnCheckIn);
		targetsHelpNotCheckIn.add(btnSendAPulse);
		targetsHelpNotCheckIn.add(btnCreateSite);
		targetsHelpNotCheckIn.add(btnJoinSite);
		targetsHelpNotCheckIn.add(btnCurrentLocation);
		targetsHelpNotCheckIn.add(navView.btnLeft);

//		helpView.helpTarget0.setVisibility(View.VISIBLE);
//		helpView.helpTarget1.setVisibility(View.GONE);
//		helpView.helpTarget2.setVisibility(View.GONE);
//		helpView.helpTarget3.setVisibility(View.GONE);
//		helpView.helpTarget4.setVisibility(View.GONE);
//		helpView.helpTarget5.setVisibility(View.GONE);

		viewSafeSite.tvTimeToReport.measure(0, 0);
		viewSafeSite.btnCheckOut.measure(0, 0);
		viewSafeSite.btnReportIn.measure(0, 0);

		targetsHelpCheckIn = new ArrayList<>();
		targetsHelpCheckIn.add(viewSafeSite.tvTimeToReport);
		targetsHelpCheckIn.add(viewSafeSite.btnReportIn);
		targetsHelpCheckIn.add(viewSafeSite.btnCheckOut);
		targetsHelpCheckIn.add(viewSafeSite.btnEmergency);
		targetsHelpCheckIn.add(viewSafeSite.btnSiteOptions);

		listCategoryView = (ListCategoryView)findViewById(R.id.listCategoryView);

		map.onCreate(null);
		map.onResume();
	}
	
	public void createMapHolder() {
		MapsInitializer.initialize(context);
		googleMap = map.getMap();
		googleMap.getUiSettings().setZoomControlsEnabled(false);
		googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
	}

}