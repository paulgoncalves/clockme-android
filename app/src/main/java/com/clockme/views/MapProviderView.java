package com.clockme.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

import com.clockme.R;
import com.clockme.SBApplication;
import com.clockme.interfaces.IRefresh;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class MapProviderView {
	private Activity mActivity;
	IRefresh refreshInterface = null;
	int ZOOM_LEVEL = 12;
	int currenZoom = ZOOM_LEVEL;
	boolean mMapIsTouched;

	public void setRefreshListener(IRefresh refreshInterface) {
		this.refreshInterface = refreshInterface;
	}

	Timer t;

	private static final ScheduledExecutorService worker = Executors
			.newSingleThreadScheduledExecutor();

	public OnCameraChangeListener getCameraChangeListener() {
		return new OnCameraChangeListener() {

			@Override
			public void onCameraChange(final CameraPosition position) {
				if (!mMapIsTouched) {
					try {

						float tempZoom = position.zoom;

						if (position.target != null) {
//							double lastLat = SharePref.getLatitude(mActivity);
//							double lastLng = SharePref.getLongitude(mActivity);
//							double currentLat = position.target.latitude;
//							double currentLng = position.target.longitude;
//							Log.d("on map change to", position.target.latitude
//									+ "/" + position.target.longitude);
//							SharePref.setLatitude(mActivity,
//									position.target.latitude);
//							SharePref.setLongitude(mActivity,
//									position.target.longitude);

						} else {
							currenZoom = (int) tempZoom;
						}
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		};
	}

	public void getCurrentLocation(Activity mActivity, GoogleMap mMap,
			IRefresh iRefresh) {
		try {
			this.mActivity = mActivity;
			this.refreshInterface = iRefresh;
			mMap.setMyLocationEnabled(true);
			Location myLocation = mMap.getMyLocation();
			if (myLocation != null) {
				SBApplication.getUserModel().location = new LatLng(myLocation.getLatitude(),myLocation.getLongitude());
			}
			mMap.setOnMyLocationChangeListener(myLocationChangeListener);
			mMap.getUiSettings().setMyLocationButtonEnabled(false);
			mMap.getUiSettings().setZoomControlsEnabled(false);
			mMap.setOnCameraChangeListener(getCameraChangeListener());
			mMap.animateCamera(CameraUpdateFactory.zoomTo(ZOOM_LEVEL), 2000,
					null);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void clearAllEvent(GoogleMap mMap) {
		if (mMap != null) {
			mMap.setOnMyLocationChangeListener(null);
			mMap.setOnCameraChangeListener(null);
			Log.d("remove map listener", "remove map listener");
		}
	}

	private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
		@Override
		public void onMyLocationChange(Location myLocation) {
			Log.d("change my location", myLocation.getLatitude() + " --- "
					+ myLocation.getLongitude());
			try {
				if (myLocation != null) {
//					SharePref.setLatitude(mActivity, myLocation.getLatitude());
//					SharePref
//							.setLongitude(mActivity, myLocation.getLongitude());
//					LatLng latlng = new LatLng(
//							SharePref.getLatitude(mActivity),
//							SharePref.getLongitude(mActivity));
					// mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
					// latlng, ZOOM_LEVEL));
					// mMap.setOnMyLocationChangeListener(null);
					if (refreshInterface != null) {
						refreshInterface.onRefresh();
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	};

	public MapTouchInterface callBackMapListener;

	public interface MapTouchInterface {
		public void onMapTouchListener(boolean isTouched);
	}

	public void setCallBackMapListener(MapTouchInterface callBackMapListener) {
		this.callBackMapListener = callBackMapListener;
	}

	// 1. some variables:
	private static final double EARTH_RADIUS = 6378100.0;
	private int offset;

	// 2. convert meters to pixels between 2 points in current zoom:
	private int convertMetersToPixels(GoogleMap map, double d, double e,
			double radiusInMeters) {
		double lat1 = radiusInMeters / EARTH_RADIUS;
		double lng1 = radiusInMeters
				/ (EARTH_RADIUS * Math.cos((Math.PI * d / 180)));
		double lat2 = d + lat1 * 180 / Math.PI;
		double lng2 = e + lng1 * 180 / Math.PI;
		Point p1 = map.getProjection().toScreenLocation(new LatLng(d, e));
		Point p2 = map.getProjection().toScreenLocation(new LatLng(lat2, lng2));
		return Math.abs(p1.x - p2.x);
	}

	// 3. bitmap creation:
	private Bitmap getBitmap(GoogleMap map, Context ctx) {
		// fill color
		Paint paint1 = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint1.setColor(0x110000FF);
		paint1.setStyle(Style.FILL);
		// stroke color
		Paint paint2 = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint2.setColor(0xFF0000FF);
		paint2.setStyle(Style.STROKE);
		// icon
		Bitmap icon = BitmapFactory.decodeResource(ctx.getResources(),
				R.mipmap.pin_black);

		// circle radius - 200 meters
		int radius = offset = convertMetersToPixels(map,SBApplication.getSiteModel().siteLocation.latitude, SBApplication.getSiteModel().siteLocation.longitude, 200);

		// if zoom too small
		if (radius < icon.getWidth() / 2) {
			radius = icon.getWidth() / 2;
		}

		// create empty bitmap
		Bitmap b = Bitmap
				.createBitmap(radius * 2, radius * 2, Config.ARGB_8888);
		Canvas c = new Canvas(b);

		// draw blue area if area > icon size
		if (radius != icon.getWidth() / 2) {
			c.drawCircle(radius, radius, radius, paint1);
			c.drawCircle(radius, radius, radius, paint2);
		}

		// draw icon
		c.drawBitmap(icon, radius - icon.getWidth() / 2,
				radius - icon.getHeight() / 2, new Paint());

		return b;
	}

	// 4. calculate image offset:

	private LatLng getCoords(GoogleMap map, double lat, double lng) {

		LatLng latLng = new LatLng(lat, lng);

		Projection proj = map.getProjection();
		Point p = proj.toScreenLocation(latLng);
		p.set(p.x, p.y + offset);

		return proj.fromScreenLocation(p);
	}

	// 5. draw:
	public void drawCircle(GoogleMap map, Context ctx, double radius) {
		// MarkerOptions options = new MarkerOptions();
		// options.position(getCoords(map,lat, lng));
		// options.icon(BitmapDescriptorFactory.fromBitmap(getBitmap(map,ctx)));

		// marker = map.addMarker(options);
		map.addCircle(new CircleOptions()
				.center(new LatLng(SBApplication.getSiteModel().siteLocation.latitude, SBApplication.getSiteModel().siteLocation.longitude)).radius(radius).strokeWidth(0f)
				.fillColor(ctx.getResources().getColor(R.color.red)));
	}

	/**
	 * Calculator distance between 2 points
	 * */
	private static final int earthRadius = 6371;

	public static float calculateDistance(float lat1, float lon1, float lat2,
			float lon2) {
		float dLat = (float) Math.toRadians(lat2 - lat1);
		float dLon = (float) Math.toRadians(lon2 - lon1);
		float a = (float) (Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math
				.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2))
				* Math.sin(dLon / 2) * Math.sin(dLon / 2));
		float c = (float) (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
		float d = earthRadius * c * 1000;
		return d;
	}

	public String[] postalCode(Context ctx) {
		final Geocoder gcd = new Geocoder(ctx);
		List<Address> addresses;
		String[] str = new String[] { "", "" };
		try {
			addresses = gcd.getFromLocation(SBApplication.getSiteModel().siteLocation.latitude,SBApplication.getSiteModel().siteLocation.longitude, 1);
			for (Address address : addresses) {
				if (address.getLocality() != null) {
					if (address.getPostalCode() != null) {
						str[0] = address.getPostalCode();
					}
					if (address.getCountryName() != null) {
						str[1] = address.getCountryName();
					}
					return str;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	public String getAddress(Context ctx, double lat, double lng) {
		final Geocoder gcd = new Geocoder(ctx);
		List<Address> addresses;

		try {
			addresses = gcd.getFromLocation(lat, lng, 1);
			for (Address address : addresses) {
				if (address.getThoroughfare()!= null) {
					return address.getThoroughfare();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
}
