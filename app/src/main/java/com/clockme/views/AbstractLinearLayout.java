package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.clockme.interfaces.IView;

public class AbstractLinearLayout extends LinearLayout implements IView {
	public Context context;
	public LinearLayout layout;

	public AbstractLinearLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		findViewById();

	}

	public AbstractLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		findViewById();

	}

	public AbstractLinearLayout(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		findViewById();

	}

	@Override
	public void findViewById() {
		// TODO Auto-generated method stub

	}

}