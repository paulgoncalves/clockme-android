package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.clockme.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;

public class MyTeamView extends AbstractRelativeLayout {

	public NavigationView navView;
	public GoogleMap googleMap;
	public MapView map;
	public EditText etSearchMembers;
	public ProgressBar pbLoading;
	public ImageButton btnCurrentLocation;

	public MyTeamView(Context context) {
		super(context);
	}

	public MyTeamView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (RelativeLayout) View.inflate(context,
				R.layout.fragment_my_team, this);

		navView = (NavigationView) findViewById(R.id.navView);
		navView.btnLeft.setVisibility(View.VISIBLE);

		navView.tvTitle.setText(context.getResources().getString(
				R.string.tv_my_team_title));
		navView.tvTitle.setVisibility(View.VISIBLE);

		navView.btnRight.setText(context.getResources().getString(
				R.string.btn_list_title));
		navView.btnRight.setVisibility(View.VISIBLE);
		navView.btnRight.setEnabled(false);

		map = (MapView) findViewById(R.id.mapView);

		map.onCreate(null);
		map.onResume();
		MapsInitializer.initialize(context);
		googleMap = map.getMap();
		googleMap.getUiSettings().setZoomControlsEnabled(false);
		googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

		
		etSearchMembers = (EditText) findViewById(R.id.etSearchMembers);
		pbLoading = (ProgressBar) findViewById(R.id.pbLoading);
		btnCurrentLocation = (ImageButton) findViewById(R.id.btnCurrentLocation);
	}

}
