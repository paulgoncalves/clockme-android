package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.clockme.R;

/**
 * Created by antkingmacmini1 on 5/16/15.
 */
public class SearchView extends AbstractRelativeLayout {

    public EditText etSearch;

    public SearchView(Context context) {
        super(context);
    }

    public SearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void findViewById() {
        super.findViewById();

        layout = (RelativeLayout) View.inflate(context, R.layout.layout_search_view, this);

        etSearch = (EditText) findViewById(R.id.etSearch);
    }
}
