package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.clockme.R;

public class NavigationView extends AbstractRelativeLayout {

	public Button btnMenu;
	
	public NavigationView(Context context) {
		super(context);
	}

	public NavigationView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (RelativeLayout) View.inflate(context, R.layout.layout_navigation, this);
		btnMenu = (Button) findViewById(R.id.btnLeft);
	}

	@Override
	public void setEnabled(boolean enabled) {
	}
}
