package com.clockme.views;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.clockme.R;

public class ChangePasswordView extends AbstractLinearLayout {

	public EditText etConfirmPassword, etPassword, etCurrentPassword;
	public AlertDialog.Builder builder;
	public AlertDialog alertDialog;

	public ChangePasswordView(Context context) {
		super(context);
	}

	public ChangePasswordView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ChangePasswordView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (LinearLayout) View.inflate(context,R.layout.layout_change_password, this);

		etCurrentPassword = (EditText) findViewById(R.id.etCurrentPassword);
		etPassword = (EditText) findViewById(R.id.etPassword);
		etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);

		etCurrentPassword.setTypeface(Typeface.SANS_SERIF);
		etPassword.setTypeface(Typeface.SANS_SERIF);
		etConfirmPassword.setTypeface(Typeface.SANS_SERIF);

		builder = new AlertDialog.Builder(new ContextThemeWrapper(context,R.style.Theme_DialogCustom));

		CharSequence title = createString(context.getResources().getString(
				R.string.tv_password_change_title));
		title = TextUtils.concat(
				title,
				"\n"
						+ context.getResources().getString(
								R.string.tv_inform_password_change_title));
		builder.setTitle(title).setView(layout);
	}

	private SpannableString createString(String str) {
		SpannableString result;
		result = new SpannableString(str);
		result.setSpan(new StyleSpan(Typeface.BOLD), 0, str.length(),
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		result.setSpan(new RelativeSizeSpan(1.2f), 0, str.length(),
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		return result;
	}
}
