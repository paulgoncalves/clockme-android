package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.clockme.interfaces.IView;

public class AbstractRelativeLayout extends RelativeLayout implements
		IView {
	public Context context;
	public RelativeLayout layout;

	public AbstractRelativeLayout(Context context) {
		super(context);
		this.context = context;
		findViewById();
	}

	public AbstractRelativeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		findViewById();

	}

	public AbstractRelativeLayout(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		findViewById();

	}

	@Override
	public void findViewById() {
		// TODO Auto-generated method stub

	}

}