package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clockme.R;

public class UserItemView extends AbstractLinearLayout {

	public ImageView ivStatus;
	public TextView tvUser, tvSite, tvTime;

	public UserItemView(Context context) {
		super(context);
	}

	public UserItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public UserItemView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (LinearLayout) View.inflate(context,
				R.layout.layout_user_item, this);

		ivStatus = (ImageView) findViewById(R.id.ivStatus);
		tvUser = (TextView) findViewById(R.id.tvUser);
		tvSite = (TextView) findViewById(R.id.tvSite);
		tvTime = (TextView) findViewById(R.id.tvTime);
	}
}
