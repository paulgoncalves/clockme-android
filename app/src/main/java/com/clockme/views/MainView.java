package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.clockme.R;

public class MainView extends AbstractRelativeLayout {

	public MainView(Context context) {
		super(context);
	}

	public MainView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (RelativeLayout) View.inflate(context, R.layout.activity_main,
				this);
	}

}
