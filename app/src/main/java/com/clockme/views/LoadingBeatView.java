package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.clockme.R;

public class LoadingBeatView extends AbstractRelativeLayout {

	public ImageView imgLogo;

	public LoadingBeatView(Context context) {
		super(context);
	}

	public LoadingBeatView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public LoadingBeatView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (RelativeLayout) View.inflate(context,
				R.layout.loading_beat, this);
		imgLogo=(ImageView)findViewById(R.id.imgLogo);
	}
}
