package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.clockme.R;

public class UsersListView extends AbstractLinearLayout {

	public NavigationView navView;
	public EditText etSearchMembers;
	public ListView lvUsers;
	
	public UsersListView(Context context) {
		super(context);
	}

	public UsersListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public UsersListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (LinearLayout) View.inflate(context,R.layout.fragment_users_list, this);
		
		navView = (NavigationView) findViewById(R.id.navView);
		navView.btnLeft.setVisibility(View.VISIBLE);

		navView.tvTitle.setText(context.getResources().getString(
				R.string.tv_my_team_title));
		navView.tvTitle.setVisibility(View.VISIBLE);

		navView.btnRight.setText(context.getResources().getString(
				R.string.btn_map_title));
		navView.btnRight.setVisibility(View.VISIBLE);
		
		etSearchMembers = (EditText) findViewById(R.id.etSearchMembers);
		lvUsers = (ListView) findViewById(R.id.lvUsers);
	}
}
