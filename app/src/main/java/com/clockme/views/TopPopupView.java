package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clockme.R;

/**
 * Created by antkingmacmini1 on 4/26/15.
 */
public class TopPopupView extends AbstractRelativeLayout {
    public static final byte STATE_BANNER_CONSTANT = 0;
    public static final byte STATE_BANNER_DISMISS = 1;
    public static final byte STATE_BANNER_FLASH = 2;

    public TextView tvTitle;
    public TopPopupView(Context context) {
        super(context);
    }
    public TopPopupView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void findViewById() {
        super.findViewById();
        layout = (RelativeLayout) View.inflate(context,R.layout.layout_top_popup, this);
        tvTitle = (TextView) findViewById(R.id.tvPopupTitle);
    }

    public void showBanner(int state, String message) {

        Animation animShowPopup = AnimationUtils.loadAnimation(context,R.anim.in_from_top);

        switch (state) {
            case STATE_BANNER_CONSTANT:

                tvTitle.setText(message);
                if (layout.getVisibility() == View.GONE) {
                    layout.startAnimation(animShowPopup);
                    layout.setVisibility(View.VISIBLE);
                }

                break;
            case STATE_BANNER_DISMISS:

                tvTitle.setText(message);
                if (layout.getVisibility() == View.VISIBLE) {
                    layout.startAnimation(animShowPopup);
                    layout.setVisibility(View.VISIBLE);
                }

                //Dismiss after 2 seconds
                hideBanner(message);

                break;
            case STATE_BANNER_FLASH:

                //User in danger flash view in read
                tvTitle.setText(message);
                if (layout.getVisibility() == View.VISIBLE) {
                    layout.startAnimation(animShowPopup);
                    layout.setVisibility(View.VISIBLE);
                }

                break;
            default:
                //Do nothing
        }
    }

    public void hideBanner(String message){

        Animation animHidePopup = AnimationUtils.loadAnimation(context,R.anim.out_to_top);
        tvTitle.setText(message);

        if (layout.getVisibility() == View.VISIBLE) {
            layout.setVisibility(View.GONE);
            layout.startAnimation(animHidePopup);
        }

    }

}
