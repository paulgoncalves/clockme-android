package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clockme.R;

public class MarkerView extends AbstractRelativeLayout {

	public TextView tvSiteName, tvDistance, tvCurrentLocation;
	
	public MarkerView(Context context) {
		super(context);
	}

	public MarkerView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (RelativeLayout) View.inflate(context,
				R.layout.my_marker, this);
		
		tvSiteName=(TextView)findViewById(R.id.tvSiteName);
		tvDistance=(TextView)findViewById(R.id.tvDistance);
		tvCurrentLocation=(TextView)findViewById(R.id.tvCurrentLocation);
	}

}
