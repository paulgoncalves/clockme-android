package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clockme.R;

public class ProfileView extends AbstractRelativeLayout {

	public RelativeLayout rlRootView;
	public LinearLayout scrollView;
	public NavigationView navView;
	public ImageView ivAvatar;
	public TextView tvEditPhoto;
	public EditText etFirstName, etLastName, etMobile, etEmail, etEmergencyNumber;
	public ListView lvLog;
	public Button btnMyLeaders, btnChangePassword;
	
	public ProfileView(Context context) {
		super(context);
	}

	public ProfileView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (RelativeLayout) View.inflate(context,R.layout.fragment_profile, this);
		
		rlRootView = (RelativeLayout) findViewById(R.id.rlRootView);
		scrollView = (LinearLayout) findViewById(R.id.scrollView);
		
		navView = (NavigationView) findViewById(R.id.navView);
		navView.btnLeft.setVisibility(View.VISIBLE);
		
		navView.tvTitle.setText(context.getResources().getString(
				R.string.tv_my_profile_title));
		navView.tvTitle.setVisibility(View.VISIBLE);
		
		navView.btnRight.setText(context.getResources().getString(
				R.string.btn_edit_title));
		navView.btnRight.setVisibility(View.VISIBLE);
		
		ivAvatar = (ImageView) findViewById(R.id.ivAvatar);
		//ivAvatar.setEnabled(false);
		tvEditPhoto = (TextView) findViewById(R.id.tvEditPhoto);
		
		etFirstName = (EditText) findViewById(R.id.etFirstName);
		etLastName = (EditText) findViewById(R.id.etLastName);
		etMobile = (EditText) findViewById(R.id.etMobile);
		etEmail = (EditText) findViewById(R.id.etEmail);
		etEmergencyNumber = (EditText) findViewById(R.id.etEmergencyNumber);

		btnMyLeaders = (Button) findViewById(R.id.btnMyLeaders);
		btnChangePassword = (Button) findViewById(R.id.btnChangePassword);
		
		lvLog = (ListView) findViewById(R.id.lvLog);
//		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
//		params.height = 600;
//		params.leftMargin = 60;
//		params.rightMargin = 60;
//		lvLog.setLayoutParams(params);
	}

}
