package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clockme.R;

/**
 * Created by AppDev on 29/09/15.
 */
public class ActionCreateView extends AbstractRelativeLayout {

    public NavigationView navView;

    public EditText etActionTitle;
    public EditText etPersonToComplete;
    public EditText etDescription;
    public TextView tvCompleteActionBy;


    public ImageButton calendarImgBtn;
    public Button addActionBtn;

    public ActionCreateView(Context context) {
        super(context);
    }

    public ActionCreateView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void findViewById() {
        super.findViewById();

        layout = (RelativeLayout) inflate(context, R.layout.fragment_wizard_action_create, this);

        navView = (NavigationView)findViewById(R.id.navView);
        navView.tvTitle.setVisibility(VISIBLE);
        navView.tvTitle.setText(getResources().getString(R.string.tv_hazard_create_action_title));
        navView.btnLeft.setVisibility(VISIBLE);
        navView.btnLeft.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selector_btn_close, 0, 0,
                0);

        etActionTitle = (EditText)findViewById(R.id.actionTitleEditText);
        etPersonToComplete = (EditText)findViewById(R.id.personToCompleteEditText);
        tvCompleteActionBy = (TextView)findViewById(R.id.dateTextView);
        etDescription = (EditText)findViewById(R.id.actionDescEditText);

        calendarImgBtn = (ImageButton)findViewById(R.id.calendarImageButton);

        addActionBtn = (Button)findViewById(R.id.addActionButton);
    }
}
