package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.clockme.R;

public class LoginActivityView extends AbstractRelativeLayout{

	public RelativeLayout rlRootView;
	public LinearLayout scrollView;
	public EditText etEmail, etPassword;
	public Button btnLogin, btnRegister, btnResetPassword;

	public LoginActivityView(Context context) {
		super(context);
	}

	public LoginActivityView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public LoginActivityView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (RelativeLayout) View.inflate(context,
				R.layout.activity_login, this);
		
		rlRootView = (RelativeLayout) findViewById(R.id.rlRootView);
		scrollView = (LinearLayout) findViewById(R.id.scrollView);
		etEmail = (EditText) findViewById(R.id.etEmail);
		etPassword = (EditText) findViewById(R.id.etPassword);
		//etPassword.setTypeface(Typeface.SANS_SERIF);
		btnLogin = (Button) findViewById(R.id.btnLogin);
		btnRegister = (Button) findViewById(R.id.btnRegister);
		btnResetPassword = (Button) findViewById(R.id.btnResetPassword);
	}
	
}
