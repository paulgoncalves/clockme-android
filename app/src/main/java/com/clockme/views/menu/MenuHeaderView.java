package com.clockme.views.menu;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clockme.R;
import com.clockme.views.AbstractRelativeLayout;

public class MenuHeaderView extends AbstractRelativeLayout {

	public TextView tvHeaderTitle;
	
	public MenuHeaderView(Context context) {
		super(context);
	}

	public MenuHeaderView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (RelativeLayout) View.inflate(context,
				R.layout.layout_menu_header, this);

		tvHeaderTitle = (TextView) findViewById(R.id.tvHeaderTitle);
	}

}
