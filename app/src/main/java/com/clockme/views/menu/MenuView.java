package com.clockme.views.menu;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.clockme.R;
import com.clockme.views.AbstractRelativeLayout;

public class MenuView extends AbstractRelativeLayout {

	public MenuHeaderView vMenuHeader;
	public ListView lvMenu;
	public Button btnLogOff;
	
	public MenuView(Context context) {
		super(context);
	}

	public MenuView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (RelativeLayout) View.inflate(context,
				R.layout.fragment_menu, this);
		
		vMenuHeader = new MenuHeaderView(context);
		lvMenu = (ListView) findViewById(R.id.lvMenu);
		btnLogOff = (Button) findViewById(R.id.btnLogOff);


		
		lvMenu.addHeaderView(vMenuHeader);
	}

}
