package com.clockme.views.menu;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clockme.R;
import com.clockme.views.AbstractRelativeLayout;

public class MenuItemView extends AbstractRelativeLayout {

	public ImageView ivItemIcon;
	public TextView tvItemTitle, tvItemCount;
	
	public MenuItemView(Context context) {
		super(context);
	}

	public MenuItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (RelativeLayout) View.inflate(context,
				R.layout.layout_menu_item, this);
		
		ivItemIcon = (ImageView) findViewById(R.id.ivItemIcon);
		tvItemTitle = (TextView) findViewById(R.id.tvItemTitle);
		tvItemCount = (TextView) findViewById(R.id.tvCount);

		tvItemCount.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (Integer.valueOf(s.toString()) > 0) {
					tvItemCount.setVisibility(VISIBLE);
				} else {
					tvItemCount.setVisibility(GONE);
				}
			}
		});
	}

}
