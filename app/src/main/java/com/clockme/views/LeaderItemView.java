package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clockme.R;

public class LeaderItemView extends AbstractRelativeLayout {
	
	public ImageView ivAvatar;
	public TextView tvLeaderName;
	public Button btnChoice;
	
	public LeaderItemView(Context context) {
		super(context);
	}

	public LeaderItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (RelativeLayout) View.inflate(context,
				R.layout.row_leader_item, this);
		
		ivAvatar = (ImageView) findViewById(R.id.ivAvatar);
		tvLeaderName = (TextView) findViewById(R.id.tvLeaderName);
		btnChoice = (Button) findViewById(R.id.btnChoice);
	}

}
