package com.clockme.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.clockme.R;

public class TeamLeadersView extends AbstractRelativeLayout {

	public RelativeLayout rlRootView;
	public NavigationView navView;
	public ListView lvLeaders;
	public Button btnTryAgain;
	public EditText etSearchLeaders;
	
	public TeamLeadersView(Context context) {
		super(context);
	}

	public TeamLeadersView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (RelativeLayout) View.inflate(context,
				R.layout.activity_team_leaders, this);
		
		rlRootView = (RelativeLayout) findViewById(R.id.rlRootView);
		
		navView = (NavigationView) findViewById(R.id.navView);
		navView.btnLeft.setVisibility(View.VISIBLE);
		
		navView.tvTitle.setText(context.getResources().getString(
				R.string.tv_team_leaders_title));
		navView.tvTitle.setVisibility(View.VISIBLE);
		
		btnTryAgain = (Button) findViewById(R.id.btnTryAgain);
		lvLeaders = (ListView) findViewById(R.id.lvLeaders);
		etSearchLeaders = (EditText) findViewById(R.id.etSearchLeaders);
		
	}

}
