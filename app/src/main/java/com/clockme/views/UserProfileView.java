package com.clockme.views;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clockme.R;

public class UserProfileView extends AbstractRelativeLayout{

	public NavigationView navView;
	public ImageView ivAvatar , ivConnStatusLeader,ivConnStatusFollower, ivIsConnection;
	public TextView tvUsername, tvEmail, tvPhone, tvConnecionStatusLeader,tvConnecionStatusFollower, tvIsConnection;
	public ListView lvLogs;
	public LinearLayout lnIsConnected, lnConnectionStatusLeader,lnConnectionStatusFollower;
	
	public UserProfileView(Context context) {
		super(context);
	}

	@Override
	public void findViewById() {
		super.findViewById();
		layout = (RelativeLayout) View.inflate(context,R.layout.fragment_user_profile, this);
		
		navView = (NavigationView) findViewById(R.id.navView);
		navView.btnLeft.setVisibility(View.VISIBLE);

		navView.tvTitle.setText(context.getResources().getString(
				R.string.tv_user_profile_title));
		navView.tvTitle.setVisibility(View.VISIBLE);

		navView.btnRight.setText(context.getResources().getString(
				R.string.btn_done_title));
		navView.btnRight.setVisibility(View.VISIBLE);
		
		ivAvatar = (ImageView) findViewById(R.id.ivAvatar);
		tvUsername = (TextView) findViewById(R.id.tvUsername);
		tvEmail = (TextView) findViewById(R.id.tvEmail);
		tvPhone = (TextView) findViewById(R.id.tvPhone);
		lvLogs = (ListView) findViewById(R.id.lvLogs);

		lnIsConnected = (LinearLayout) findViewById(R.id.lnIsConnected);

		lnConnectionStatusLeader = (LinearLayout) findViewById(R.id.lnConnectionStatusLeader);
		tvConnecionStatusLeader = (TextView) findViewById(R.id.tvConnStatusLeader);
		ivConnStatusLeader = (ImageView) findViewById(R.id.ivConnStatusLeader);

		lnConnectionStatusFollower = (LinearLayout) findViewById(R.id.lnConnectionStatusFollower);
		tvConnecionStatusFollower = (TextView) findViewById(R.id.tvConnStatusFollower);
		ivConnStatusFollower = (ImageView) findViewById(R.id.ivConnStatusFollower);

		tvIsConnection = (TextView) findViewById(R.id.tvIsConnection);
		ivIsConnection = (ImageView) findViewById(R.id.ivIsConnection);
		
	}
	
}
