package com.clockme;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.clockme.utilities.Constants;
import com.clockme.utilities.SharePref;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by lexuanvu on 2/13/15.
 */
public class GcmIntentService extends IntentService {
    private long NOTIFICATION_ID = 0l;
    private NotificationManager mNotificationManager;
    PendingIntent contentIntent;//, contentIntentSplash;

    public GcmIntentService() {
        super(Constants.PROJECT_NUMBER);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle

            String userInfo = SharePref.getUserInfo(this);
            if (!TextUtils.isEmpty(userInfo)) {
                if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                    // Post notification of received message.
                    int total = SharePref.getTotalPush(this);
                    SharePref.setTotalPush(this, total + 1);
                    Set<String> keys = extras.keySet();
                    Iterator<String> it = keys.iterator();

                    if (extras != null) {
                        String msg = "";
                        int type = 0;
                        int userId = 0;
                        while (it.hasNext()) {
                            String key = it.next();

                            Log.e("PUSHMESSAGE", "[" + key + "=" + extras.get(key) + "]");
                            if (key.equals("message")) {
                                msg = (String) extras.get(key);
                                //break;
                            }

                            if (key.equals("personId")) {
                                userId = extras.getInt(key);
                                //break;
                            }

                            if (key.equals("type")) {
                                type = extras.getInt(key);
                                //break;
                            }

                        }
                        //String localUserId = SharePref.getUserId(this) +"";
                        //Log.e("PUSHMESSAGE","UserId =" + localUserId);
                        if(!TextUtils.isEmpty(msg)){
                            if(SBApplication.getUserModel().personId != 0 && SBApplication.getUserModel().personId == userId && type == 1){
                                showNotification(msg, true);
                            }
                            else{
                                showNotification(msg, false);
                            }

                        }
                    }

//                    showNotification(extras.toString());
                }
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void showNotification(String msg, Boolean isRequireAction) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                        .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                        .setAutoCancel(true)
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);

        NOTIFICATION_ID = System.currentTimeMillis();
        mNotificationManager.notify((int)NOTIFICATION_ID, mBuilder.build());

    }

}
