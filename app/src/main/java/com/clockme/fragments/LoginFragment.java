package com.clockme.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clockme.controllers.LoginController;
import com.clockme.views.LoginView;

/**
 * Created by clockme on 18/09/15.
 */
public class LoginFragment extends Fragment {

    static LoginFragment instance;
    private Context mContext;
    private LoginView view;
    private LoginController controller;

    public static LoginFragment getInstance() {
        if (instance == null) {
            instance = new LoginFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = new LoginView(mContext);
        controller = new LoginController(mContext, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(controller != null) {
            controller.initData();
            controller.setListener();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}


