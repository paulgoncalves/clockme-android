package com.clockme.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clockme.controllers.SiteNotesController;

/**
 * A simple {@link Fragment} subclass.
 */
public class ActionNoteAddFragment extends Fragment {

    private static ActionNoteAddFragment mInstance;
    private Context mContext;
    private SiteNotesView mView;
    private SiteNotesController mController;

    public static ActionNoteAddFragment getInstance() {
        if(mInstance == null) {
            mInstance = new ActionNoteAddFragment();
        }

        return mInstance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = new SiteNotesView(mContext);
        mController = new SiteNotesController(mContext, mView);
        mController.initData();
        mController.setListener();

        return mView;
    }


}
