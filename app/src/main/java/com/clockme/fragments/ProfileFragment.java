package com.clockme.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clockme.R;
import com.clockme.controllers.ProfileController;
import com.clockme.views.ProfileView;

public class ProfileFragment extends Fragment {

	static ProfileFragment mInstance;
	private Context mContext;
	private ProfileView mView;
	private ProfileController mController;

	public static ProfileFragment getInstance() {
		if (mInstance == null) {
			mInstance = new ProfileFragment();
		}
		return mInstance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, Bundle savedInstanceState) {
		mView = new ProfileView(mContext);
		mController = new ProfileController(mContext, mView);
		return mView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		super.onStart();
		new CountDownTimer(700, 100) {

			@Override
			public void onTick(long millisUntilFinished) {

			}

			@Override
			public void onFinish() {
				if (!mView.navView.btnRight.getText().equals(mContext.getResources()
						.getString(R.string.btn_save_title))) {
					mController.initData();
					mController.setListener();
				}
			}
		}.start();
	}

	@Override
	public void onResume() {
		super.onResume();
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}

}
