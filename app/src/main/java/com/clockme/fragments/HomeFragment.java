package com.clockme.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clockme.controllers.HomeController;
import com.clockme.views.HomeView;

public class HomeFragment extends Fragment {

	static HomeFragment instance;
	private Context mContext;
	private HomeView view;
	private static HomeController controller;

	public static HomeFragment getInstance() {
		if (instance == null) {
			instance = new HomeFragment();
		}
		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = new HomeView(mContext);
		controller = new HomeController(mContext, view);
		controller.setListener();
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
		controller.initData();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		//controller.destroy();
		//controller.stopCallBacks();
	}

	public HomeController getController(){
		return controller;
	}
}
