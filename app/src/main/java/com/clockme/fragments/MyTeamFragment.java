package com.clockme.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clockme.controllers.MyTeamController;
import com.clockme.views.MyTeamView;

public class MyTeamFragment extends Fragment {

	static MyTeamFragment instance;
	private Context context;
	private MyTeamView view;
	private MyTeamController controller;

	public static MyTeamFragment getInstance() {
		if (instance == null) {
			instance = new MyTeamFragment();
		}
		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		view = new MyTeamView(context);
		controller = new MyTeamController(context, view);
		return view;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		controller.setListener();
	}

	@Override
	public void onResume() {
		super.onResume();
		controller.initData();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

}
