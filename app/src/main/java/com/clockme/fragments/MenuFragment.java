package com.clockme.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clockme.controllers.menu.MenuController;
import com.clockme.views.menu.MenuView;

public class MenuFragment extends Fragment {

	private static final String TAG = MenuFragment.class.getSimpleName();
	static MenuFragment instance;
	private Context context;
	private MenuView view;
	private MenuController controller;

	public static MenuFragment getInstance() {
		if (instance == null) {
			instance = new MenuFragment();
		}
		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
		view = new MenuView(context);
		controller = new MenuController(context, view);
		return view;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.e(TAG, "onResume() - MENU");
		new CountDownTimer(500, 100) {
			
			@Override
			public void onTick(long millisUntilFinished) {
				
			}
			
			@Override
			public void onFinish() {
				controller.initData();
				controller.setListener();
			}
		}.start();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}

}
